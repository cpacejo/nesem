#ifndef MAPPER_H
#define MAPPER_H

#include <stdbool.h>
#include "cart.h"
#include "cpu.h"
#include "ppu.h"
#include "mapper_nrom.h"
#include "mapper_mmc1.h"
#include "mapper_uxrom.h"
#include "mapper_cnrom.h"
#include "mapper_mmc3.h"

struct mapper
{
  enum mapper_type type;
  union
  {
    struct nrom nrom;
    struct mmc1 mmc1;
    struct uxrom uxrom;
    struct cnrom cnrom;
    struct mmc3 mmc3;
  };
};


int mapper_config(struct mapper *, const struct cart *)
  __attribute__ ((cold));

void mapper_init(struct mapper *, struct cpu *, struct ppu *)
  __attribute__ ((cold));

void mapper_write(struct mapper *, struct cpu *, struct ppu *,
  int addr, int value);

void mapper_scanline(struct mapper *);

inline bool mapper_need_prg_ram_sync(const struct mapper *const mapper)
{
  switch (mapper->type)
  {
  case MAPPER_TYPE_MMC3: return mapper->mmc3.need_prg_ram_sync;
  default: return false;
  }
}

void mapper_notify_prg_ram_sync(struct mapper *)
  __attribute__ ((cold));

inline bool mapper_poll_irq(const struct mapper *const mapper)
{
  switch (mapper->type)
  {
  case MAPPER_TYPE_MMC3: return mmc3_poll_irq(&mapper->mmc3);
  default: return false;
  }
}

#endif
