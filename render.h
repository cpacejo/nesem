#ifndef RENDER_H
#define RENDER_H

#include <stdint.h>
#include "ppu.h"

// See filter_gen.c for notes on rendering modes.

void render_ntsc_scanline_rar_1_1(
  const struct scanline *restrict, uint8_t [restrict 2][640][3]);
void render_ntsc_scanline_rar_2_1(
  const struct scanline *restrict, uint8_t [restrict 1][640][3]);

void render_ntsc_scanline_rar_1_1_linear(
  const struct scanline *restrict, float [restrict 2][640][3]);
void render_ntsc_scanline_rar_2_1_linear(
  const struct scanline *restrict, float [restrict 1][640][3]);

void render_ntsc_frame_rar_1_1(
  const struct frame *restrict, uint8_t [restrict 480][640][3]);
void render_ntsc_frame_rar_2_1(
  const struct frame *restrict, uint8_t [restrict 240][640][3]);

void render_ntsc_frame_rar_1_1_linear(
  const struct frame *restrict, float [restrict 480][640][3]);
void render_ntsc_frame_rar_2_1_linear(
  const struct frame *restrict, float [restrict 240][640][3]);

#endif
