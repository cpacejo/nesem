#include <stdbool.h>
#include <stdlib.h>
#include "cpu.h"
#include "ppu.h"
#include "mapper_nrom.h"
#include "mapper_mmc1.h"
#include "mapper_uxrom.h"
#include "mapper_cnrom.h"
#include "mapper_mmc3.h"
#include "mapper.h"

int mapper_config(struct mapper *const mapper, const struct cart *const cart)
{
  mapper->type = cart->mapper_type;
  switch (mapper->type)
  {
  case MAPPER_TYPE_NROM: return nrom_config(&mapper->nrom, cart);
  case MAPPER_TYPE_MMC1: return mmc1_config(&mapper->mmc1, cart);
  case MAPPER_TYPE_UXROM: return uxrom_config(&mapper->uxrom, cart);
  case MAPPER_TYPE_CNROM: return cnrom_config(&mapper->cnrom, cart);
  case MAPPER_TYPE_MMC3: return mmc3_config(&mapper->mmc3, cart);
  default: abort();
  }
}

void mapper_init(struct mapper *const mapper,
  struct cpu *const cpu, struct ppu *const ppu)
{
  switch (mapper->type)
  {
  case MAPPER_TYPE_NROM: nrom_init(&mapper->nrom, cpu, ppu); break;
  case MAPPER_TYPE_MMC1: mmc1_init(&mapper->mmc1, cpu, ppu); break;
  case MAPPER_TYPE_UXROM: uxrom_init(&mapper->uxrom, cpu, ppu); break;
  case MAPPER_TYPE_CNROM: cnrom_init(&mapper->cnrom, cpu, ppu); break;
  case MAPPER_TYPE_MMC3: mmc3_init(&mapper->mmc3, cpu, ppu); break;
  default: abort();
  }
}

void mapper_write(struct mapper *const mapper,
  struct cpu *const cpu, struct ppu *const ppu,
  const int addr, const int value)
{
  switch (mapper->type)
  {
  case MAPPER_TYPE_NROM: break;
  case MAPPER_TYPE_MMC1: mmc1_write(&mapper->mmc1, cpu, ppu, addr, value); break;
  case MAPPER_TYPE_UXROM: uxrom_write(&mapper->uxrom, cpu, ppu, addr, value); break;
  case MAPPER_TYPE_CNROM: cnrom_write(&mapper->cnrom, cpu, ppu, addr, value); break;
  case MAPPER_TYPE_MMC3: mmc3_write(&mapper->mmc3, cpu, ppu, addr, value); break;
  default: abort();
  }
}

void mapper_scanline(struct mapper *const mapper)
{
  switch (mapper->type)
  {
  case MAPPER_TYPE_NROM:
  case MAPPER_TYPE_MMC1:
  case MAPPER_TYPE_UXROM:
  case MAPPER_TYPE_CNROM: break;
  case MAPPER_TYPE_MMC3: mmc3_scanline(&mapper->mmc3); break;
  default: abort();
  }
}

bool mapper_need_prg_ram_sync(const struct mapper *);

void mapper_notify_prg_ram_sync(struct mapper *const mapper)
{
  switch (mapper->type)
  {
  case MAPPER_TYPE_MMC3:
    mapper->mmc3.need_prg_ram_sync = false;
    break;

  default: break;
  }
}

bool mapper_poll_irq(const struct mapper *);
