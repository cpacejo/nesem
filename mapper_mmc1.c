#include <assert.h>
#include <stdint.h>
#include <stdlib.h>
#include "cart.h"
#include "cpu.h"
#include "misc.h"
#include "ppu.h"
#include "mapper_mmc1.h"


int mmc1_config(struct mmc1 *const mmc1, const struct cart *const cart)
{
  assert(cart->mapper_type == MAPPER_TYPE_MMC1);

  if (!is_power_of_2(cart->prg_rom_size) ||
    cart->prg_rom_size < 16 << 10 || cart->prg_rom_size > 256 << 10)
    return -1;  // TODO: 512 KiB

  if (cart->chr_rom_size != 0 && (!is_power_of_2(cart->chr_rom_size) ||
    cart->chr_rom_size < 4 << 10 || cart->chr_rom_size > 128 << 10))
    return -1;

  if (cart->prg_ram_size != 0 && cart->prg_ram_size != 8192)
    return -1;  // TODO: 16/32 KiB

  if (cart->chr_ram_size != 0 && cart->chr_ram_size != 8192)
    return -1;

  if (cart->chr_rom_size != 0 && cart->chr_ram_size != 0)
    return -1;

  for (int i = 0; i < 16; i++)
    mmc1->memory.prg_bank[i] =
      (char *) cart->prg_rom + ((i << 14) & (cart->prg_rom_size - 1));

  if (cart->chr_rom_size == 0)
  {
    for (int i = 0; i < 32; i++)
      mmc1->memory.chr_bank[i] =
        (char *) cart->chr_ram + ((i & 1) << 12);
  }
  else
  {
    for (int i = 0; i < 32; i++)
      mmc1->memory.chr_bank[i] =
        (char *) cart->chr_rom + ((i << 12) & (cart->chr_rom_size - 1));
  }

  mmc1->memory.has_prg_ram = cart->prg_ram_size != 0;
  mmc1->memory.prg_ram = cart->prg_ram;

  return 0;
}


//
// Helpers
//

static void update_mirroring(struct ppu *const ppu, const int mode)
{
  ppu->memory.name_attribute_table[0] =
    &ppu->ram->name_attribute_table[(int) (mode == 1)];

  ppu->memory.name_attribute_table[1] =
    &ppu->ram->name_attribute_table[(mode ^ mode >> 1) & 1];

  ppu->memory.name_attribute_table[2] =
    &ppu->ram->name_attribute_table[mode & 1];

  ppu->memory.name_attribute_table[3] =
    &ppu->ram->name_attribute_table[(int) (mode != 0)];
}

static void update_prg_16k_bank_helper(struct mmc1_memory *const memory,
  struct cpu *const cpu, const int which, const int bank)
{
  for (int i = 0; i < 64; i++)
    cpu->memory.pages[i + ((which + 2) << 6)] =
      (uint8_t *) memory->prg_bank[bank] + (i << 8);
}

static void update_chr_4k_bank_helper(struct mmc1_memory *const memory,
  struct ppu *const ppu, const int which, const int bank)
{
  for (int i = 0; i < 4; i++)
    ppu->memory.pattern_table_1k[(which * 4) + i] =
      (struct pattern_table_1k *) memory->chr_bank[bank] + i;
}

static void update_prg_bank(struct mmc1 *const mmc1, struct cpu *const cpu)
{
  switch (mmc1->regs.prg_bank_mode)
  {
  case MMC1_PRG_BANK_32_KB:
    update_prg_16k_bank_helper(&mmc1->memory, cpu, 0, mmc1->regs.prg_bank & ~1);
    update_prg_16k_bank_helper(&mmc1->memory, cpu, 1, mmc1->regs.prg_bank | 1);
    break;

  case MMC1_PRG_BANK_16_KB_UPPER:
    update_prg_16k_bank_helper(&mmc1->memory, cpu, 0, 0);
    update_prg_16k_bank_helper(&mmc1->memory, cpu, 1, mmc1->regs.prg_bank);
    break;

  case MMC1_PRG_BANK_16_KB_LOWER:
    update_prg_16k_bank_helper(&mmc1->memory, cpu, 0, mmc1->regs.prg_bank);
    update_prg_16k_bank_helper(&mmc1->memory, cpu, 1, 15);
    break;
  }
}

static void update_chr_bank_0(struct mmc1 *const mmc1, struct ppu *const ppu)
{
  switch (mmc1->regs.chr_bank_mode)
  {
  case MMC1_CHR_BANK_8_KB:
    update_chr_4k_bank_helper(&mmc1->memory, ppu, 0, mmc1->regs.chr_bank_0 & ~1);
    update_chr_4k_bank_helper(&mmc1->memory, ppu, 1, mmc1->regs.chr_bank_0 | 1);
    break;

  case MMC1_CHR_BANK_4_KB:
    update_chr_4k_bank_helper(&mmc1->memory, ppu, 0, mmc1->regs.chr_bank_0);
    break;
  }
}

static void update_chr_bank_1(struct mmc1 *const mmc1, struct ppu *const ppu)
{
  switch (mmc1->regs.chr_bank_mode)
  {
  case MMC1_CHR_BANK_8_KB:
    break;

  case MMC1_CHR_BANK_4_KB:
    update_chr_4k_bank_helper(&mmc1->memory, ppu, 1, mmc1->regs.chr_bank_1);
    break;
  }
}


//
// I/O
//

void mmc1_init(struct mmc1 *const mmc1,
  struct cpu *const cpu, struct ppu *const ppu)
{
  mmc1->shift = 0x10;

  mmc1->regs.prg_bank_mode = MMC1_PRG_BANK_16_KB_LOWER;
  mmc1->regs.chr_bank_mode = MMC1_CHR_BANK_8_KB;

  mmc1->regs.prg_bank = 0;
  mmc1->regs.chr_bank_0 = 0;
  mmc1->regs.chr_bank_1 = 0;

  update_mirroring(ppu, 0);
  update_prg_bank(mmc1, cpu);
  update_chr_bank_0(mmc1, ppu);
  update_chr_bank_1(mmc1, ppu);

  if (mmc1->memory.has_prg_ram)
  {
    cpu->memory.io_write[1] &= UINT64_C(0x00000000FFFFFFFF);
    cpu->memory.io_read[1] &= UINT64_C(0x00000000FFFFFFFF);
    for (int i = 0; i < 32; i++)
      cpu->memory.pages[0x60 + i] =
        (uint8_t *) mmc1->memory.prg_ram + (i << 8);
  }
}

static void reset(struct mmc1 *const mmc1, struct cpu *const cpu)
{
  mmc1->regs.prg_bank_mode = MMC1_PRG_BANK_16_KB_LOWER;
  update_prg_bank(mmc1, cpu);
}

static void write_control(struct mmc1 *const mmc1,
  struct cpu *const cpu, struct ppu *const ppu,
  const int value)
{
  mmc1->regs.prg_bank_mode =
    ((value >> 3) & 1) == 0 ? MMC1_PRG_BANK_32_KB :
    ((value >> 2) & 1) == 0 ? MMC1_PRG_BANK_16_KB_UPPER :
    MMC1_PRG_BANK_16_KB_LOWER;

  mmc1->regs.chr_bank_mode =
    ((value >> 4) & 1) == 0 ? MMC1_CHR_BANK_8_KB : MMC1_CHR_BANK_4_KB;

  update_mirroring(ppu, value & 3);
  update_prg_bank(mmc1, cpu);
  update_chr_bank_0(mmc1, ppu);
  update_chr_bank_1(mmc1, ppu);
}

static void write_chr_bank_0(struct mmc1 *const mmc1,
  struct ppu *const ppu, const int bank)
{
  mmc1->regs.chr_bank_0 = bank;
  update_chr_bank_0(mmc1, ppu);
}

static void write_chr_bank_1(struct mmc1 *const mmc1,
  struct ppu *const ppu, const int bank)
{
  mmc1->regs.chr_bank_1 = bank;
  update_chr_bank_1(mmc1, ppu);
}

static void write_prg_bank(struct mmc1 *const mmc1,
  struct cpu *const cpu, const int bank)
{
  mmc1->regs.prg_bank = bank;
  update_prg_bank(mmc1, cpu);
}


void mmc1_write(struct mmc1 *const mmc1,
  struct cpu *const cpu, struct ppu *const ppu,
  const int addr, const int value)
{
  if ((value & 0x80) != 0)
  {
    mmc1->shift = 0x10;
    reset(mmc1, cpu);
  }
  else
  {
    const int sr = (mmc1->shift >> 1) | ((value & 1) << 4);

    if ((mmc1->shift & 1) == 0)
      mmc1->shift = sr;
    else
    {
      mmc1->shift = 0x10;

      switch ((addr >> 13) & 3)
      {
      case 0:
        write_control(mmc1, cpu, ppu, sr);
        break;

      case 1:
        write_chr_bank_0(mmc1, ppu, sr);
        break;

      case 2:
        write_chr_bank_1(mmc1, ppu, sr);
        break;

      case 3:
        write_prg_bank(mmc1, cpu, sr);
        break;
      }
    }
  }
}
