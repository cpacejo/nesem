#include <assert.h>
#include <math.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include "cpu.h"
#include "filter_gen.h"
#include "misc.h"
#include "apu.h"

#define LOG2_CPU_CYCLES_PER_SAMPLE 4

// main logic

static void advance_envelope(struct apu_envelope *const envelope)
{
  if (envelope->start)
  {
    envelope->decay_level = 15;
    envelope->divider = envelope->volume;
    envelope->start = false;
  }
  else
  {
    if (envelope->divider > 0)
      envelope->divider--;
    else
    {
      envelope->divider = envelope->volume;
      if (envelope->decay_level > 0)
        envelope->decay_level--;
      else
        envelope->decay_level = envelope->halt ? 15 : 0;
    }
  }
}

static void advance_linear_counter(struct apu_triangle *const triangle)
{
  if (triangle->linear_counter_reload)
    triangle->linear_counter = triangle->linear_counter_reload_value;
  else if (triangle->linear_counter > 0)
    triangle->linear_counter--;
}

static void do_quarter_frame(struct apu *const apu)
{
  advance_envelope(&apu->regs.envelope[CHANNEL_PULSE_1]);
  advance_envelope(&apu->regs.envelope[CHANNEL_PULSE_2]);
  advance_envelope(&apu->regs.envelope[CHANNEL_NOISE]);

  advance_linear_counter(&apu->regs.triangle);
  if (!apu->regs.envelope[CHANNEL_TRIANGLE].halt)
    apu->regs.triangle.linear_counter_reload = false;
}

static void advance_length_counter(struct apu_envelope *const envelope)
{
  if (!envelope->halt && envelope->length_counter > 0)
    envelope->length_counter--;
}

static int sweep_target_period(const struct apu_pulse *const pulse)
{
  const int delta = pulse->timer >> pulse->sweep.shift;
  // ignore channel 2 one's-complement bug
  return pulse->sweep.negate ?
    pulse->timer - delta : pulse->timer + delta;
}

static void sweep_update_period(struct apu_pulse *const pulse)
{
  const int target_period = sweep_target_period(pulse);
  if (pulse->timer >= 8 && target_period <= 0x7FF)
    pulse->timer = target_period;
}

static void advance_sweep(struct apu_pulse *const pulse)
{
  const int old_divider = pulse->sweep.divider;

  if (pulse->sweep.reload)
    pulse->sweep.divider = pulse->sweep.period;
  else if (pulse->sweep.divider > 0)
    pulse->sweep.divider--;

  if (old_divider <= 0 && pulse->sweep.enabled)
  {
    pulse->sweep.divider = pulse->sweep.period;
    sweep_update_period(pulse);
  }

  pulse->sweep.reload = false;
}

static void do_half_frame(struct apu *const apu)
{
  for (int channel = 0; channel < 4; channel++)
    advance_length_counter(&apu->regs.envelope[channel]);

  for (int which = 0; which < 2; which++)
    advance_sweep(&apu->regs.pulse[which]);
}


// output

static int envelope_volume(const struct apu *const apu,
  const enum apu_channel channel)
{
  assert(channel != CHANNEL_TRIANGLE);
  return apu->regs.envelope[channel].constant_volume ?
    apu->regs.envelope[channel].volume :
    apu->regs.envelope[channel].decay_level;
}

static bool pulse_active(const struct apu *const apu, const int which)
{
  return apu->regs.pulse[which].timer >= 8 &&
    apu->regs.envelope[CHANNEL_PULSE_1 + which].length_counter > 0 &&
    sweep_target_period(&apu->regs.pulse[which]) <= 0x7FF;
}

static bool triangle_active(const struct apu *const apu)
{
  return apu->regs.envelope[CHANNEL_TRIANGLE].length_counter > 0 &&
    apu->regs.triangle.linear_counter > 0;
}

static bool noise_active(const struct apu *const apu)
{
  return apu->regs.envelope[CHANNEL_NOISE].length_counter > 0;
}

static bool dmc_active(const struct apu *const apu)
{
  return apu->regs.control.enable[CHANNEL_DMC] &&
    apu->regs.dmc.remaining > 0;
}


// registers

static void write_envelope_envelope(struct apu *const apu,
  const enum apu_channel which, const int val)
{
  apu->regs.envelope[which].halt = (bool) ((val >> 5) & 1);
  apu->regs.envelope[which].constant_volume = (bool) ((val >> 4) & 1);
  apu->regs.envelope[which].volume = val & 0xF;
  apu->regs.envelope[which].start = true;
}

static void write_envelope_length(struct apu *const apu,
  const enum apu_channel which, const int val)
{
  static const uint8_t table[32] =
    { 10, 254, 20, 2, 40, 4, 80, 6, 160, 8, 60, 10, 14, 12, 26, 14,
      12, 16, 24, 18, 48, 20, 96, 22, 192, 24, 72, 26, 16, 28, 32, 30 };
  if (apu->regs.control.enable[which])
    apu->regs.envelope[which].length_counter = (int) table[val >> 3];
}

static void apu_write_pulse_envelope(struct apu *const apu,
  const int which, const int val)
{
  apu->regs.pulse[which].duty = val >> 6;
  write_envelope_envelope(apu, CHANNEL_PULSE_1 + which, val);
}

static void apu_write_pulse_sweep(struct apu *const apu,
  const int which, const int val)
{
  apu->regs.pulse[which].sweep.enabled = (bool) (val >> 7);
  apu->regs.pulse[which].sweep.period = (val >> 4) & 7;
  apu->regs.pulse[which].sweep.negate = (bool) ((val >> 3) & 1);
  apu->regs.pulse[which].sweep.shift = val & 7;
  apu->regs.pulse[which].sweep.reload = true;
}

static void apu_write_pulse_timer_low(struct apu *const apu,
  const int which, const int val)
{
  apu->regs.pulse[which].timer =
    (apu->regs.pulse[which].timer & ~0xFF) | val;
}

static void apu_write_pulse_timer_high(struct apu *const apu,
  const int which, const int val)
{
  const int new_timer =
    (apu->regs.pulse[which].timer & 0xFF) | ((val & 7) << 8);
  apu->regs.pulse[which].timer = new_timer;
  apu->regs.pulse[which].phase_high = 0;
  apu->regs.pulse[which].phase_low = new_timer;
  write_envelope_length(apu, CHANNEL_PULSE_1 + which, val);
}

void apu_write_pulse_1_envelope(struct apu *const apu, const int val)
{
  apu_write_pulse_envelope(apu, 0, val);
}

void apu_write_pulse_1_sweep(struct apu *const apu, const int val)
{
  apu_write_pulse_sweep(apu, 0, val);
}

void apu_write_pulse_1_timer_low(struct apu *const apu, const int val)
{
  apu_write_pulse_timer_low(apu, 0, val);
}

void apu_write_pulse_1_timer_high(struct apu *const apu, const int val)
{
  apu_write_pulse_timer_high(apu, 0, val);
}

void apu_write_pulse_2_envelope(struct apu *const apu, const int val)
{
  apu_write_pulse_envelope(apu, 1, val);
}

void apu_write_pulse_2_sweep(struct apu *const apu, const int val)
{
  apu_write_pulse_sweep(apu, 1, val);
}

void apu_write_pulse_2_timer_low(struct apu *const apu, const int val)
{
  apu_write_pulse_timer_low(apu, 1, val);
}

void apu_write_pulse_2_timer_high(struct apu *const apu, const int val)
{
  apu_write_pulse_timer_high(apu, 1, val);
}

void apu_write_triangle_linear(struct apu *const apu, const int val)
{
  apu->regs.envelope[CHANNEL_TRIANGLE].halt = (bool) (val >> 7);
  apu->regs.triangle.linear_counter_reload_value = val & 0x7F;
}

void apu_write_triangle_timer_low(struct apu *const apu, const int val)
{
  apu->regs.triangle.timer = (apu->regs.triangle.timer & ~0xFF) | val;
}

void apu_write_triangle_timer_high(struct apu *const apu, const int val)
{
  apu->regs.triangle.timer = (apu->regs.triangle.timer & 0xFF) | ((val & 7) << 8);
  apu->regs.triangle.linear_counter_reload = true;
  write_envelope_length(apu, CHANNEL_TRIANGLE, val);
}

void apu_write_noise_envelope(struct apu *const apu, const int val)
{
  write_envelope_envelope(apu, CHANNEL_NOISE, val);
}

void apu_write_noise_timer(struct apu *const apu, const int val)
{
  static const uint16_t table[16] =
    { 2, 4, 8, 16, 32, 48, 64, 80,
      101, 127, 190, 254, 381, 508, 1017, 2034 };
  apu->regs.noise.fast = (bool) (val >> 7);
  apu->regs.noise.timer = (int) table[val & 0xF];
}

void apu_write_noise_length(struct apu *const apu, const int val)
{
  if (apu->regs.control.enable[CHANNEL_NOISE])
    write_envelope_length(apu, CHANNEL_NOISE, val);
}

void apu_write_dmc_rate(struct apu *const apu, const int val)
{
  static uint8_t table[16] =
    { 214, 190, 170, 160, 143, 127, 113, 107,
      95, 80, 71, 64, 53, 42, 36, 27 };
  apu->regs.dmc.irq_enable = (bool) (val >> 7);
  if ((val >> 7) == 0) apu->regs.dmc.irq = false;
  apu->regs.dmc.loop = (bool) ((val >> 6) & 1);
  apu->regs.dmc.timer = (int) table[val & 0xF];
}

void apu_write_dmc_direct(struct apu *const apu, const int val)
{
  apu->regs.dmc.level = val & 0x7F;
}

void apu_write_dmc_address(struct apu *const apu, const int val)
{
  apu->regs.dmc.address = 0xC000 | (val << 6);
}

void apu_write_dmc_length(struct apu *const apu, const int val)
{
  const int length = (val << 4) + 1;
  apu->regs.dmc.length = length;
  apu->regs.dmc.remaining = length << 3;
}

void apu_write_control(struct apu *const apu, const int val)
{
  apu->regs.control.enable[CHANNEL_PULSE_1] = (bool) (val & 1);
  apu->regs.control.enable[CHANNEL_PULSE_2] = (bool) (val & 2);
  apu->regs.control.enable[CHANNEL_TRIANGLE] = (bool) (val & 4);
  apu->regs.control.enable[CHANNEL_NOISE] = (bool) (val & 8);
  apu->regs.control.enable[CHANNEL_DMC] = (bool) (val & 0x10);

  if ((val & 1) == 0)
    apu->regs.envelope[CHANNEL_PULSE_1].length_counter = 0;
  if ((val & 2) == 0)
    apu->regs.envelope[CHANNEL_PULSE_2].length_counter = 0;
  if ((val & 4) == 0)
    apu->regs.envelope[CHANNEL_TRIANGLE].length_counter = 0;
  if ((val & 8) == 0)
    apu->regs.envelope[CHANNEL_NOISE].length_counter = 0;
}

int apu_read_status(struct apu *const apu)
{
  const int ret =
    ((int) apu->regs.dmc.irq << 7) |
    ((int) apu->regs.frame_counter.irq << 6) |
    ((int) (apu->regs.dmc.remaining > 0) << 4) |
    ((int) (apu->regs.envelope[CHANNEL_NOISE].length_counter > 0) << 3) |
    ((int) (apu->regs.envelope[CHANNEL_TRIANGLE].length_counter > 0) << 2) |
    ((int) (apu->regs.envelope[CHANNEL_PULSE_2].length_counter > 0) << 1) |
    ((int) (apu->regs.envelope[CHANNEL_PULSE_1].length_counter > 0));

  apu->regs.frame_counter.irq = false;

  return ret;
}

void apu_write_frame_counter(struct apu *const apu, const int val)
{
  apu->regs.frame_counter.stutter_mode = (bool) (val >> 7);
  apu->regs.frame_counter.irq_inhibit = (bool) ((val >> 6) & 1);

  if (((val >> 6) & 1) != 0)
    apu->regs.frame_counter.irq = false;

  // FIXME: handle stutter mode correctly
  apu->state.step = (val >> 7) == 0 ? APU_STEP_1 : APU_STEP_4;
}

bool apu_poll_irq(const struct apu *);


// step logic

static void advance_frame_counter(struct apu *const apu)
{
  switch (apu->state.step)
  {
  case APU_STEP_1:
    do_quarter_frame(apu);
    apu->state.step = APU_STEP_2;
    break;

  case APU_STEP_2:
    do_quarter_frame(apu);
    do_half_frame(apu);
    apu->state.step = APU_STEP_3;
    break;

  case APU_STEP_3:
    do_quarter_frame(apu);
    apu->state.step = apu->regs.frame_counter.stutter_mode ?
      APU_STEP_STUTTER : APU_STEP_4;
    break;

  case APU_STEP_4:
    do_quarter_frame(apu);
    do_half_frame(apu);
    if (!apu->regs.frame_counter.stutter_mode &&
        !apu->regs.frame_counter.irq_inhibit)
      apu->regs.frame_counter.irq = true;
    apu->state.step = APU_STEP_1;
    break;

  case APU_STEP_STUTTER:
    assert(apu->regs.frame_counter.stutter_mode);
    apu->state.step = APU_STEP_1;
    break;
  }
}


// sequencer rendering

static int pulse_sequence(const enum pulse_duty duty)
{
  switch (duty)
  {
  case PULSE_DUTY_1_8: return 0x0202;
  case PULSE_DUTY_1_4: return 0x0606;
  case PULSE_DUTY_1_2: return 0x1E1E;
  case PULSE_DUTY_3_4: return 0xF9F9;
  default: PROMISE(false);
  }
}

static void render_pulse(const struct apu *const apu, const int which,
  uint8_t levels[restrict], uint16_t durations[restrict], const int cpu_cycles)
{
  if (pulse_active(apu, which))
  {
    const int timer = apu->regs.pulse[which].timer;
    const int sequence = pulse_sequence(apu->regs.pulse[which].duty);
    const int volume = envelope_volume(apu, CHANNEL_PULSE_1 + which);
    int phase_high = apu->regs.pulse[which].phase_high;
    const int phase_low = apu->regs.pulse[which].phase_low;

    levels[0] = ((sequence >> phase_high) & 1) * volume;
    durations[0] = phase_low << 1;

    // TODO: optimize
    int j = 1;
    for (int i = phase_low << 1; i < cpu_cycles; i += (timer + 1) << 1)
    {
      phase_high = (phase_high + 1) & 7; 
      levels[j] = ((sequence >> phase_high) & 1) * volume;
      durations[j] = (timer + 1) << 1;
      j++;
    }
  }
  else
  {
    levels[0] = 0;
    durations[0] = cpu_cycles;
  }
}

static int triangle_value(const int phase)
{
  return phase ^ (31 * (phase >> 4));
}

static void render_triangle(const struct apu *const apu,
  uint8_t levels[restrict], uint16_t durations[restrict], const int cpu_cycles)
{
  int phase_high = apu->regs.triangle.phase_high;

  if (triangle_active(apu))
  {
    const int timer = apu->regs.triangle.timer;
    const int phase_low = apu->regs.triangle.phase_low;

    levels[0] = triangle_value(phase_high);
    durations[0] = phase_low;

    int j = 1;
    for (int i = phase_low; i < cpu_cycles; i += timer + 1)
    {
      phase_high = (phase_high + 1) & 31;
      levels[j] = triangle_value(phase_high);
      durations[j] = timer + 1;
      j++;
    }
  }
  else
  {
    levels[0] = triangle_value(phase_high);
    durations[0] = cpu_cycles;
  }
}

static int lfsr_slow_next(const int lfsr)
{
  return (lfsr >> 1) | (((lfsr ^ (lfsr >> 1)) & 1) << 14);
}

static int lfsr_slow_next8(const int lfsr)
{
  return (lfsr >> 8) | (((lfsr ^ (lfsr >> 1)) & 0xFF) << 6);
}

static int lfsr_fast_next(const int lfsr)
{
  return (lfsr >> 1) | (((lfsr ^ (lfsr >> 6)) & 1) << 14);
}

static int lfsr_fast_next8(const int lfsr)
{
  return (lfsr >> 8) | (((lfsr << 6) ^ lfsr) & 0x3FC0);
}

static void render_noise(const struct apu *const apu,
  uint8_t levels[restrict], uint16_t durations[restrict], const int cpu_cycles)
{
  if (noise_active(apu))
  {
    const int timer = apu->regs.noise.timer;
    const int volume = envelope_volume(apu, CHANNEL_NOISE);
    int lfsr = apu->regs.noise.lfsr;
    const int phase_low = apu->regs.noise.phase_low;

    levels[0] = (lfsr & 1) * volume;
    durations[0] = phase_low << 1;

    int j = 1;
    for (int i = phase_low << 1; i < cpu_cycles; i += (timer + 1) << 1)
    {
      lfsr = apu->regs.noise.fast ? lfsr_fast_next(lfsr) : lfsr_slow_next(lfsr);
      levels[j] = (lfsr & 1) * volume;
      durations[j] = (timer + 1) << 1;
      j++;
    }
  }
  else
  {
    levels[0] = 0;
    durations[0] = cpu_cycles;
  }
}

#if 0
static uint64_t decode_dmc_byte(const int level, const int byte)
{
  if (LIKELY(level >= 16 && level <= 111))
    return 0x70727476787A7C7Eull + (explode_bits(byte) << 2);
  else
  {
    uint64_t ret = 0;
    int cur_level = level;
    for (int i = 0; i < 8; i++)
    {
      const int new_level = cur_level + (((byte >> i) & 1) << 2) - 2;
      if (new_level >= 0 && new_level <= 127)
        cur_level = new_level;
      ret |= cur_level << (i * 8);
    }
    return ret;
  }
}
#endif

static void render_dmc(struct apu *const apu,
  const struct cpu_memory *const cpu_memory,
  uint8_t levels[restrict], uint16_t durations[restrict],
  int *const restrict new_level, const int cpu_cycles)
{
  if (dmc_active(apu))
  {
    const int timer = apu->regs.dmc.timer;
    int level = apu->regs.dmc.level;
    int remaining = apu->regs.dmc.remaining;
    const int phase_low = apu->regs.dmc.phase_low;

    assert(remaining > 0);

    levels[0] = level;
    durations[0] = phase_low << 1;

    int current_addr =
      0x8000 + ((apu->regs.dmc.address + apu->regs.dmc.length -
        (remaining >> 3) - 1) & 0x7FFF);
    int current_byte =
      cpu_memory->pages[current_addr >> 8][current_addr & 0xFF];

    int j = 1;
    for (int i = phase_low << 1; i < cpu_cycles; i += (timer + 1) << 1)
    {
      remaining--;

      const int new_level = level +
        (int) (((current_byte << (remaining & 7)) & 0x80) >> 5) - 2;
      if (new_level >= 0 && new_level <= 127)
        level = new_level;
      levels[j] = level;

      if (UNLIKELY((remaining & 7) == 0))
      {
        if (UNLIKELY(remaining <= 0))
        {
          if (apu->regs.dmc.loop)
            remaining = apu->regs.dmc.length << 3;
          else
          {
            durations[j] = cpu_cycles;
            break;
          }
        }

        current_addr = 0x8000 + ((current_addr + 1) & 0x7FFF);
        current_byte = cpu_memory->pages[current_addr >> 8][current_addr & 0xFF];
      }

      durations[j] = (timer + 1) << 1;
      j++;
    }

    *new_level = level;
  }
  else
  {
    levels[0] = apu->regs.dmc.level;
    durations[0] = cpu_cycles;
    *new_level = apu->regs.dmc.level;
  }
}


// mixdown

static float mix(const int pulse1, const int pulse2,
  const int triangle, const int noise, const int dmc)
{
  PROMISE(pulse1 >= 0);
  PROMISE(pulse2 >= 0);
  PROMISE(triangle >= 0);
  PROMISE(noise >= 0);
  PROMISE(dmc >= 0);

  // Note: we could scale pulse to 3/8 and tnd to 5/8,
  // then scale by output voltage 1.17 V, and divide
  // by consumer peak-to-peak reference voltage 0.894 V,
  // then scale by -18 dBFS digital reference level,
  // but this ends up being very quiet for some reason.
  // So just scale to 100%.

  const float pulse_out =
    pulse1 + pulse2 == 0 ? 0.0f :
    0.9588f / (81.28f / (float) (pulse1 + pulse2) + 1.0f);
  const float tnd_sum =
    triangle * (1.0f/82.27f) +
    noise * (1.0f/122.41f) +
    dmc * (1.0f/226.38f);
  const float tnd_out =
    UNLIKELY(triangle + noise + dmc == 0) ? 0.0f :
    1.5979f / (1.0f / tnd_sum + 1.0f);
  return pulse_out + tnd_out;
}

static float step[1 << LOG2_CPU_CYCLES_PER_SAMPLE][STEP_FILTER_WIDTH];

__attribute__ ((constructor))
static void init_step(void)
{
  for (int phase = 0; phase < 1 << LOG2_CPU_CYCLES_PER_SAMPLE; phase++)
    for (int i = 0; i < STEP_FILTER_WIDTH; i++)
      step[phase][i] = (float)
        step_coeff(i + STEP_FILTER_MIN_X,
          ldexp((double) phase, -LOG2_CPU_CYCLES_PER_SAMPLE));
}

static void mix_samples(
  const uint8_t *restrict levels[5], const uint16_t *restrict durations[5],
  float buffer[restrict], const int samples)
{
  int level[5];
  int next[5];
  int pos[5] = { };
  for (int i = 0; i < 5; i++)
  {
    level[i] = levels[i][0];
    next[i] = durations[i][0];
  }

  float cur_level =
    mix(level[CHANNEL_PULSE_1], level[CHANNEL_PULSE_2],
      level[CHANNEL_TRIANGLE], level[CHANNEL_NOISE], level[CHANNEL_DMC]);
  int cur_next = min(min(next[0], next[1]), min(next[2], min(next[3], next[4])));
  int sample = 0;

  while (cur_next >> LOG2_CPU_CYCLES_PER_SAMPLE < samples)
  {
    assert(cur_next >= sample << LOG2_CPU_CYCLES_PER_SAMPLE);

    // Render current level up to and including the next sample
    // which contains a transition.  This complements the decorations,
    // which include the delta in the first sample (regardless of phase).
    for (; sample <= cur_next >> LOG2_CPU_CYCLES_PER_SAMPLE; sample++)
      buffer[sample + DELAY_SAMPLES] += cur_level;

    // Now, decorate sample - 1.
    const int next_cpu_cycle = sample << LOG2_CPU_CYCLES_PER_SAMPLE;
    while (cur_next < next_cpu_cycle)
    {
      for (int i = 0; i < 5; i++)
      {
        assert(next[i] >= cur_next);
        if (next[i] <= cur_next)
        {
          pos[i]++;
          level[i] = levels[i][pos[i]];
          next[i] += durations[i][pos[i]];
        }
      }

      const float next_level =
        mix(level[CHANNEL_PULSE_1], level[CHANNEL_PULSE_2],
          level[CHANNEL_TRIANGLE], level[CHANNEL_NOISE], level[CHANNEL_DMC]);

      const float delta = next_level - cur_level;
      const int phase = cur_next & ((1 << LOG2_CPU_CYCLES_PER_SAMPLE) - 1);

      for (int j = 0; j < STEP_FILTER_WIDTH; j++)
        buffer[sample + (DELAY_SAMPLES + STEP_FILTER_MIN_X - 1) + j] +=
          delta * step[phase][j];

      cur_level = next_level;
      cur_next = min(min(next[0], next[1]), min(next[2], min(next[3], next[4])));
    }
  }

  for (; sample < samples; sample++)
    buffer[sample + DELAY_SAMPLES] += cur_level;
}


// sequencer advance

static void advance_pulse(struct apu *const apu, const int which, const int cpu_cycles)
{
  assert(cpu_cycles % 2 == 0);

  struct apu_pulse *const pulse = &apu->regs.pulse[CHANNEL_PULSE_1 + which];
  const int timer = pulse->timer;
  const int new_phase_low_less_1 = timer + (cpu_cycles >> 1) - pulse->phase_low;

  pulse->phase_low = timer - new_phase_low_less_1 % (timer + 1);
  pulse->phase_high = (pulse->phase_high + new_phase_low_less_1 / (timer + 1)) & 7;
}

static void advance_triangle(struct apu *const apu, const int cpu_cycles)
{
  if (!triangle_active(apu)) return;

  struct apu_triangle *const triangle = &apu->regs.triangle;
  const int timer = triangle->timer;
  const int new_phase_low_less_1 = timer + cpu_cycles - triangle->phase_low;

  triangle->phase_low = timer - new_phase_low_less_1 % (timer + 1);
  triangle->phase_high = (triangle->phase_high + new_phase_low_less_1 / (timer + 1)) & 31;
}

static void advance_noise(struct apu *const apu, const int cpu_cycles)
{
  assert(cpu_cycles % 2 == 0);

  struct apu_noise *const noise = &apu->regs.noise;
  const int timer = noise->timer;
  const int new_phase_low_less_1 = timer + (cpu_cycles >> 1) - noise->phase_low;
  const int lfsr_steps = new_phase_low_less_1 / (timer + 1);

  int lfsr = noise->lfsr;
  if (noise->fast)
  {
    for (int i = 0; i < lfsr_steps >> 3; i++)
      lfsr = lfsr_fast_next8(lfsr);
    for (int i = 0; i < (lfsr_steps & 7); i++)
      lfsr = lfsr_fast_next(lfsr);
  }
  else
  {
    for (int i = 0; i < lfsr_steps >> 3; i++)
      lfsr = lfsr_slow_next8(lfsr);
    for (int i = 0; i < (lfsr_steps & 7); i++)
      lfsr = lfsr_slow_next(lfsr);
  }

  noise->phase_low = timer - new_phase_low_less_1 % (timer + 1);
  noise->lfsr = lfsr;
}

static void advance_dmc(struct apu *const apu, const int new_level, const int cpu_cycles)
{
  assert(cpu_cycles % 2 == 0);

  // we don't bother simulating the 8-bit buffer itself
  if (dmc_active(apu))
  {
    struct apu_dmc *const dmc = &apu->regs.dmc;
    const int timer = dmc->timer;
    const int new_phase_low_less_1 = timer + (cpu_cycles >> 1) - dmc->phase_low;
    dmc->phase_low = timer - new_phase_low_less_1 % (timer + 1);
    dmc->level = new_level;
    const int elapsed = new_phase_low_less_1 / (timer + 1);
    if (!dmc->loop && elapsed > dmc->remaining)
    {
      dmc->remaining = 0;
      if (dmc->irq_enable)
        dmc->irq = true;
    }
    else
    {
      const int length = dmc->length << 3;
      dmc->remaining = length - (length - dmc->remaining + elapsed) % length;
    }
  }
}


// mixdown

static struct filter { float a1, b0, b1; }
  highpass_90, highpass_440, lowpass_14000;

__attribute__ ((constructor))
void init_filters(void)
{
  double a1, b0, b1;
  highpass_iir(90.0 / SAMPLE_RATE, &a1, &b0, &b1);
  highpass_90 = (struct filter) {
    .a1 = (float) a1, .b0 = (float) b0, .b1 = (float) b1 };
  highpass_iir(440.0 / SAMPLE_RATE, &a1, &b0, &b1);
  highpass_440 = (struct filter) {
    .a1 = (float) a1, .b0 = (float) b0, .b1 = (float) b1 };
  lowpass_iir(14000.0 / SAMPLE_RATE, &a1, &b0, &b1);
  lowpass_14000 = (struct filter) {
    .a1 = (float) a1, .b0 = (float) b0, .b1 = (float) b1 };
}

static void apply_filter(
  const struct filter *const filter,
  float *const carry,
  float buffer[], const size_t samples)
{
  const float a1 = filter->a1;
  const float b0 = filter->b0;
  const float b1 = filter->b1;

  float z1 = *carry;

  for (int i = 0; i < samples; i++)
  {
    const float z0 = buffer[i] - a1 * z1;
    buffer[i] = b0 * z0 + b1 * z1;
    z1 = z0;
  }

  *carry = z1;
}


void apu_init(struct apu *const apu)
{
  apu->regs.noise.lfsr = 1;
}

#define MAX_TRANSITIONS (SAMPLE_BUFFER_SIZE << LOG2_CPU_CYCLES_PER_SAMPLE)

void apu_frame(struct apu *const apu,
  const struct cpu_memory *const cpu,
  float buffer[restrict RAW_SAMPLE_BUFFER_SIZE],
  size_t *const samples_out)
{
  const size_t samples = SAMPLE_BUFFER_SIZE;  // for now

  // render each channel
  uint8_t levels[5][MAX_TRANSITIONS];
  uint16_t durations[5][MAX_TRANSITIONS];
  const int cpu_cycles = samples << LOG2_CPU_CYCLES_PER_SAMPLE;
  render_pulse(apu, 0, levels[CHANNEL_PULSE_1], durations[CHANNEL_PULSE_1], cpu_cycles);
  render_pulse(apu, 1, levels[CHANNEL_PULSE_2], durations[CHANNEL_PULSE_2], cpu_cycles);
  render_triangle(apu, levels[CHANNEL_TRIANGLE], durations[CHANNEL_TRIANGLE], cpu_cycles);
  render_noise(apu, levels[CHANNEL_NOISE], durations[CHANNEL_NOISE], cpu_cycles);
  int dmc_level;
  render_dmc(apu, cpu, levels[CHANNEL_DMC], durations[CHANNEL_DMC], &dmc_level, cpu_cycles);

  // carry over delay and overlap samples from last frame
  memcpy(&buffer[0], &apu->state.delay_overlap_buffer[0],
    (DELAY_SAMPLES + OVERLAP_SAMPLES) * sizeof buffer[0]);

  // clear rest of buffer
  memset(&buffer[DELAY_SAMPLES + OVERLAP_SAMPLES], 0, samples * sizeof buffer[0]);

  // mix this frame
  mix_samples(
    (const uint8_t *[5]) { levels[0], levels[1], levels[2], levels[3], levels[4] },
    (const uint16_t *[5]) { durations[0], durations[1], durations[2], durations[3], durations[4] },
    buffer, samples);

  // store delay and overlap samples from this frame
  memcpy(&apu->state.delay_overlap_buffer[0], &buffer[samples],
    (DELAY_SAMPLES + OVERLAP_SAMPLES) * sizeof buffer[0]);

  // filter
  apply_filter(&highpass_90, &apu->state.highpass_90_carry, buffer, samples);
  apply_filter(&highpass_440, &apu->state.highpass_440_carry, buffer, samples);
  apply_filter(&lowpass_14000, &apu->state.lowpass_14000_carry, buffer, samples);

  // advance sequencers
  advance_pulse(apu, 0, cpu_cycles);
  advance_pulse(apu, 1, cpu_cycles);
  advance_triangle(apu, cpu_cycles);
  advance_noise(apu, cpu_cycles);
  advance_dmc(apu, dmc_level, cpu_cycles);

  // advance frame, *after* rendering, so we correctly account for stutter
  // mode timings (even though this adds a frame of latency)
  advance_frame_counter(apu);

  *samples_out = samples;
}
