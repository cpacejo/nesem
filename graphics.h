#ifndef GRAPHICS_H
#define GRAPHICS_H

#define GLFW_INCLUDE_GLCOREARB
#include <GLFW/glfw3.h>

struct graphics
{
  GLuint scaling_program;
  GLuint vertex_buffer, vertex_array;
  GLuint texture;
};

void graphics_init(struct graphics *)
  __attribute__ ((cold));
void graphics_draw(struct graphics *, const float framebuffer[240][640][3]);
void graphics_destroy(struct graphics *)
  __attribute__ ((cold));

#endif
