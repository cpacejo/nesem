#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <stdbool.h>
#include "input.h"

struct controller
{
  bool strobe;
  bool pressed[2][8];
  bool latched[2][8];
  int shift[2];
};

void controller_write_strobe(struct controller *, int value);
int controller_read_0(struct controller *);
int controller_read_1(struct controller *);

void controller_set_state(struct controller *,
  int which, const bool pressed[8]);

#endif
