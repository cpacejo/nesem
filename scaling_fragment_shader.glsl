#version 410 core

// This is just a simple Lanczos-2 upsampler.

#define M_PI 3.1415926535897932384626433832795

#define L2(x) ( \
  2.0 * sin(M_PI*(x)) * sin(0.5*(M_PI*(x))) / \
  ((M_PI*(x)) * (M_PI*(x))))

uniform sampler2DRect tex;

in vec2 coord;
out vec4 color;

void main()
{
  ivec2 icoord = ivec2(floor(coord));
  vec2 fcoord = fract(coord);

  vec4 L2x =
    fcoord.x < 0.0001 ? vec4(0.0, 1.0, 0.0, 0.0) :
    fcoord.x > 0.9999 ? vec4(0.0, 0.0, 1.0, 0.0) :
    L2(fcoord.x + vec4(1.0, 0.0, -1.0, -2.0));

  vec4 L2y =
    fcoord.y < 0.0001 ? vec4(0.0, 1.0, 0.0, 0.0) :
    fcoord.y > 0.9999 ? vec4(0.0, 0.0, 1.0, 0.0) :
    L2(fcoord.y + vec4(1.0, 0.0, -1.0, -2.0));

  mat4 L2k = outerProduct(L2y, L2x);

#define conv(i, j) \
  (L2k[i][j] * texelFetchOffset(tex, icoord, ivec2((i) - 1, (j) - 1)))

  color =
    conv(0, 0) + conv(1, 0) + conv(2, 0) + conv(3, 0) +
    conv(0, 1) + conv(1, 1) + conv(2, 1) + conv(3, 1) +
    conv(0, 2) + conv(1, 2) + conv(2, 2) + conv(3, 2) +
    conv(0, 3) + conv(1, 3) + conv(2, 3) + conv(3, 3);
}
