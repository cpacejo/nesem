#ifndef PPU_H
#define PPU_H

#include <stdbool.h>
#include "cpu.h"

#define FRAME_US 16639.27

struct ppu;

void ppu_write_control(struct ppu *, int val);
void ppu_write_mask(struct ppu *, int val);
int ppu_read_status(struct ppu *);
void ppu_write_oam_addr(struct ppu *, int val);
void ppu_write_oam_data(struct ppu *, int val);
void ppu_write_scroll(struct ppu *, int val);
void ppu_write_addr(struct ppu *, int val);
void ppu_write_data(struct ppu *, int val);
int ppu_read_data(struct ppu *);
void ppu_write_oam_dma(struct ppu *, struct cpu *cpu, int val);
inline bool ppu_poll_nmi(const struct ppu *);

struct scanline
{
  bool greyscale, emph_r, emph_g, emph_b;
  uint8_t pixel[280];
};

struct frame
{
  struct scanline scanline[240];
};

void ppu_step(struct ppu *, struct frame [2], int *valid_frame);


// private

#include <stdint.h>

enum state
{
  PPU_PRE_RENDER,
  PPU_VISIBLE,
  PPU_POST_RENDER,
  PPU_VBLANK
};

struct ppu_state
{
  bool odd_frame;
  enum state state;
  int scanline;  // will be 240 in POST_RENDER and VBLANK; never less than 0
};

struct ppu_regs
{
  struct
  {
    bool data_vert_incr;
    int sprite_pattern_table, background_pattern_table;
    bool large_sprites, vblank_nmi;
  } control;

  struct
  {
    bool greyscale;
    bool background_leftmost, sprites_leftmost;
    bool background, sprites;
    bool emph_r, emph_g, emph_b;
  } mask;

  struct
  {
    bool sprite_0_hit, vblank;
  } status;

  int oam_addr;

  struct
  {
    bool w;
    int v, t, x;
  } scroll;

  int data;
};

// laid out in a pseudo-physical manner
// to greatly simplify read/write from CPU
// (at the expense of slightly more complicated
// internal accesses)

struct pattern
{
  uint8_t bits[2][8];
} __attribute__ ((packed));

struct pattern_table
{
  struct pattern pattern[256];
} __attribute__ ((packed));

struct pattern_table_1k
{
  struct pattern pattern[64];
} __attribute__ ((packed));

struct name_table
{
  uint8_t tile[30][32];
} __attribute__ ((packed));

struct attribute_table
{
  uint8_t square[8][8];
} __attribute__ ((packed));

struct name_attribute_table
{
  struct name_table name_table;
  struct attribute_table attribute_table;
} __attribute__ ((packed));

struct palette
{
  uint8_t color[4][4];
} __attribute__ ((packed));

struct sprite
{
  uint8_t y, index, attr, x;
} __attribute__ ((packed));

struct oam
{
  struct sprite sprite[64];
} __attribute__ ((packed));

struct ppu_ram
{
  struct name_attribute_table name_attribute_table[2];
};

struct ppu_memory
{
  struct pattern_table_1k *pattern_table_1k[8];
  struct name_attribute_table *name_attribute_table[4];
  struct palette background_palette;
  struct palette sprite_palette;
  struct oam oam;
};

struct ppu
{
  long long clock;
  struct ppu_state state;
  struct ppu_regs regs;
  struct ppu_memory memory;
  struct ppu_ram *ram;
};


inline bool ppu_poll_nmi(const struct ppu *const ppu)
{
  return ppu->regs.status.vblank && ppu->regs.control.vblank_nmi;
}

#endif
