CFLAGS += -std=gnu11 -Os -g -Wall -Werror -march=native -ffast-math \
  $(shell pkg-config --cflags glfw3) $(shell pkg-config --cflags portaudio-2.0)
LDFLAGS += -Wall -Werror -march=native -lm \
  $(shell pkg-config --static --libs glfw3) $(shell pkg-config --libs portaudio-2.0)

MAPPERS = nrom mmc1 uxrom cnrom mmc3
OBJS = main.o cart.o cpu.o ppu.o mapper.o system.o $(MAPPERS:%=mapper_%.o) \
  render.o pnm.o filter_gen.o vertex_shader.o scaling_fragment_shader.o \
  graphics.o input.o controller.o apu.o audio.o misc.o

nesem: $(OBJS)
	$(CC) $(LDFLAGS) -o $@ $(OBJS)

filter_gen: filter_gen.o
	$(CC) -std=gnu11 -Wall -Werror -lm -o $@ $^

joystick_test: joystick_test.o
	$(CC) $(LDFLAGS) -o $@ $^

audio.o: audio.h misc.h filter_gen.h
cart.o: cart.h
cpu.o: cpu.h misc.h
apu.o: apu.h cpu.h filter_gen.h misc.h
ppu.o: ppu.h cpu.h
controller.o: controller.h input.h
graphics_thread.o: graphics_thread.h graphics.h system.h ppu.h render.h
main.o: cart.h system.h apu.h cpu.h ppu.h mapper.h render.h graphics.h misc.h graphics_thread.h
mapper_mmc1.o: misc.h
mapper_uxrom.o: misc.h
mapper_cnrom.o: misc.h
mapper_mmc3.o: misc.h
$(foreach mapper,$(MAPPERS),$(eval mapper_$(mapper).o: mapper_$(mapper).h cart.h cpu.h ppu.h))
mapper.o: mapper.h $(MAPPERS:%=mapper_%.h) cart.h cpu.h ppu.h
system.o: system.h cart.h controller.h apu.h cpu.h mapper.h ppu.h $(MAPPERS:%=mapper_%.h)
render.o: render.h ppu.h filter_gen.h misc.h
pnm.o: pnm.h ppu.h render.h
filter_gen.o: filter_gen.h
graphics.o: graphics.h

%.o: %.glsl
	echo "\\t.section .rodata\\n\\t.global $*_source\\n$*_source:\\n\\t.incbin \"$<\"\\n\\t.byte 0" | \
	gcc -x assembler -c - -c -o $@
