#version 410 core

#define MULT_LENGTH 7

uniform vec3 palette[512];
uniform vec4 coeff[8 * MULT_LENGTH];

uniform samplerBuffer emph;
uniform sampler2DRect pixels;

layout(pixel_center_integer) in vec4 gl_FragCoord;

out vec4 color;

void main()
{
  int coeff_base = (gl_FragCoord.x & 7) * MULT_LENGTH;
  int palette_base = texelFetch(emph, gl_FragCoord.y) << 6;
  ivec2 tex_base = ivec2(((int(gl_FragCoord.x) + 1) * 7) / 8, gl_FragCoord.y);
  vec3 c;
  for (int i = 0; i < MULT_LENGTH; i++)
    c += coeff[coeff_base + i] *
      palette[e + texelFetchOffset(pixels, tex_base, ivec2(i - ((MULT_LENGTH - 1) / 2), 0))];
  color = vec4(c, 1.0);
}
