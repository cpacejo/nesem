#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "ppu.h"
#include "render.h"
#include "pnm.h"

static int write_pnm_header(const int width, const int height, const int max, FILE *const file)
{
  return fprintf(file, "P6\n%i %i\n%i\n", width, height, max);
}

int export_frame_pnm_rar_1_1(const struct frame *const frame, FILE *const file)
{
  setvbuf(file, NULL, _IOFBF, 0);

  if (write_pnm_header(640, 480, 255, file) < 0)
    return -1;

  uint8_t out[2][640][3];
  for (int i = 0; i < 240; i++)
  {
    render_ntsc_scanline_rar_1_1(&frame->scanline[i], out);
    if (fwrite(out, sizeof out, 1, file) < 1)
      return -1;
  }

  return 0;
}

int export_frame_pnm_rar_2_1(const struct frame *const frame, FILE *const file)
{
  setvbuf(file, NULL, _IOFBF, 0);

  if (write_pnm_header(640, 240, 255, file) < 0)
    return -1;

  uint8_t out[1][640][3];
  for (int i = 0; i < 240; i++)
  {
    render_ntsc_scanline_rar_2_1(&frame->scanline[i], out);
    if (fwrite(out, sizeof out, 1, file) < 1)
      return -1;
  }

  return 0;
}

int export_pattern_tables_pnm(struct pattern_table_1k *pattern_table_1k[8], FILE *const file)
{
  setvbuf(file, NULL, _IOFBF, 0);

  if (write_pnm_header(256, 128, 1, file) < 0)
    return -1;

  for (int y = 0; y < 128; y++)
    for (int x = 0; x < 256; x++)
    {
      const int table = ((x >> 5) & 4) | (y >> 5);
      const int pattern = ((y & 0x18) << 1) | ((x >> 3) & 15);
      const int row = y & 7;
      const int col = 7 - (x & 7);
      if (putc((pattern_table_1k[table]->pattern[pattern].bits[0][row] >> col) & 1, file) < 0)
        return -1;
      if (putc(0, file) < 0)
        return -1;
      if (putc((pattern_table_1k[table]->pattern[pattern].bits[1][row] >> col) & 1, file) < 0)
        return -1;
    }

  return 0;
}
