#include <errno.h>
#include <stdatomic.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include "cart.h"
#include "misc.h"
#include "system.h"

#define MAX_FRAMESKIP 2

static void usage(const char *const prog_name)
{
  printf("Usage: %s <cart.nes>\n", prog_name);
  exit(1);
}

#define GLFW_INCLUDE_GLCOREARB
#include <GLFW/glfw3.h>
#include <portaudio.h>
#include <assert.h>
#include <string.h>
#include "audio.h"
#include "apu.h"
#include "input.h"
#include "graphics.h"
#include "render.h"

static int glfw_error_code;
static const char *glfw_error_description;

__attribute__ ((cold))
static void glfw_error_callback(const int code, const char *const description)
{
  glfw_error_code = code;
  glfw_error_description = description;
}

#define OUT_SAMPLE_BUFFER_SIZE 200
#define OUT_BUFFERS 12
#define OUT_SAMPLE_RATE (OUT_SAMPLE_BUFFER_SIZE * 240.0)

#define AUDIO_BUF_SIZE (OUT_SAMPLE_BUFFER_SIZE * OUT_BUFFERS)

struct audio_buf
{
  volatile atomic_int prod, cons;
  float buf[AUDIO_BUF_SIZE];  // fast & loose w/ no volatile here
};

static int pa_audio_callback(
  const void *const input, void *const output,
  const unsigned long frameCount,
  const PaStreamCallbackTimeInfo *const timeInfo,
  const PaStreamCallbackFlags statusFlags,
  void *const userData)
{
  struct audio_buf *const audio_buf = userData;

  const int prod = atomic_load_explicit(&audio_buf->prod, memory_order_acquire);
  const int cons = atomic_load_explicit(&audio_buf->cons, memory_order_relaxed);

  const int amt =
    min(frameCount, (prod + 2 * AUDIO_BUF_SIZE - cons) % (2 * AUDIO_BUF_SIZE));
  assert(amt <= AUDIO_BUF_SIZE);
  if (amt < frameCount) printf("UNDER %i < %i\n", amt, (int) frameCount);
  for (int i = 0; i < amt; i++)
   ((float *) output)[i] = audio_buf->buf[(cons + i) % AUDIO_BUF_SIZE];
  memset(&((float *) output)[amt], 0, (frameCount - amt) * sizeof (float));

  atomic_store_explicit(&audio_buf->cons,
    (cons + amt) % (2 * AUDIO_BUF_SIZE), memory_order_release);

  return paContinue;
}

static bool needs_audio(struct audio_buf *const audio_buf)
{
  const int prod = atomic_load_explicit(&audio_buf->prod, memory_order_relaxed);
  const int cons = atomic_load_explicit(&audio_buf->cons, memory_order_acquire);
  const int amt = (prod + 2 * AUDIO_BUF_SIZE - cons) % (2 * AUDIO_BUF_SIZE);
  assert(amt <= AUDIO_BUF_SIZE);
  return amt < AUDIO_BUF_SIZE - OUT_SAMPLE_BUFFER_SIZE;
}

static float *cur_audio_buf(struct audio_buf *const audio_buf)
{
  const int prod = atomic_load_explicit(&audio_buf->prod, memory_order_relaxed);
  assert((prod % OUT_SAMPLE_BUFFER_SIZE) == 0);
  return &audio_buf->buf[prod % AUDIO_BUF_SIZE];
}

static void poke_audio(struct audio_buf *const audio_buf)
{
  const int prod = atomic_load_explicit(&audio_buf->prod, memory_order_relaxed);
  atomic_store_explicit(&audio_buf->prod,
    (prod + OUT_SAMPLE_BUFFER_SIZE) % (2 * AUDIO_BUF_SIZE), memory_order_release);
}

int main(int argc, char *argv[])
{
  if (argc < 2) usage(argv[0]);

  // load cart
  const char *const cart_path = argv[1];
  FILE *const cart_file = fopen(cart_path, "rb");
  if (cart_file == NULL)
  {
    printf("Can't open %s!\n", cart_path);
    return 1;
  }
  enum cart_error cart_error;
  struct cart *const cart = load_cart(cart_file, cart_path, &cart_error);
  if (cart == NULL)
    switch (cart_error)
    {
    case CART_ERROR_BAD_HEADER:
      printf("Cart has bad header!\n");
      return 1;
    case CART_ERROR_UNSUPPORTED_FEATURE:
      printf("Cart has unsupported feature!\n");
      return 2;
    case CART_ERROR_UNSUPPORTED_FORMAT:
      printf("Cart has unsupported format!\n");
      return 2;
    case CART_ERROR_UNKNOWN_MAPPER:
      printf("Cart has unknown mapper!\n");
      return 2;
    case CART_ERROR_BAD_DATA:
      printf("Cart has bad data!\n");
      return 1;
    default: abort();
    }
  fclose(cart_file);

  // config system
  struct system system;
  if (system_config(&system, cart) < 0)
  {
    printf("Cart has invalid mapper configuration!\n");
    return 1;
  }

  // init system
  system_init(&system);

  // init graphics
  glfwSetErrorCallback(glfw_error_callback);

  if (glfwInit() == GLFW_FALSE)
  {
    fprintf(stderr, "glfwInit: %s\n", glfw_error_description);
    abort();
  }

  // setup window & context
  glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
  glfwWindowHint(GLFW_VISIBLE, GLFW_TRUE);
  glfwWindowHint(GLFW_FOCUSED, GLFW_TRUE);
  glfwWindowHint(GLFW_SRGB_CAPABLE, GLFW_TRUE);
  glfwWindowHint(GLFW_DOUBLEBUFFER, GLFW_TRUE);
  glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  GLFWwindow *const window =
    glfwCreateWindow(960, 720, "nesem", NULL, NULL);
  if (window == NULL)
  {
    fprintf(stderr, "glfwCreateWindow: %s\n", glfw_error_description);
    abort();
  }
  // 320x240 is actually too small for RAR 1:1 on a non-high-DPI screen,
  // since we have only an upsampler, not a downsampler.  Caveat emptor.
  glfwSetWindowSizeLimits(window, 320, 240, GLFW_DONT_CARE, GLFW_DONT_CARE);
  glfwSetWindowAspectRatio(window, 4, 3);
  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
  glfwMakeContextCurrent(window);
  glfwSwapInterval(1);

  struct graphics graphics;
  graphics_init(&graphics);
  float (*const framebuffer)[240][640][3] = aligned_alloc(64, 640 * 240 * 3 * sizeof(float));

  glfwPollEvents();


  // init audio

  PaError pa_err;
  pa_err = Pa_Initialize();
  if (pa_err != paNoError)
  {
    fprintf(stderr, "Pa_Initialize: %s\n", Pa_GetErrorText(pa_err));
    abort();
  }

  struct audio_buf audio_buf = {
    .prod = ATOMIC_VAR_INIT(0),
    .cons = ATOMIC_VAR_INIT(0)
  };

  struct biquad filter_coeffs[DOWNSAMPLE_FILTER_ORDER / 2];
  compute_downsample_filter_coeffs(SAMPLE_RATE / OUT_SAMPLE_RATE, filter_coeffs);
  float filter_state[DOWNSAMPLE_FILTER_ORDER] = { };

  PaStream *stream;
  pa_err = Pa_OpenDefaultStream(&stream, 0, 1, paFloat32, OUT_SAMPLE_RATE,
    paFramesPerBufferUnspecified, pa_audio_callback, &audio_buf);
  if (pa_err != paNoError)
  {
    fprintf(stderr, "Pa_OpenDefaultStream: %s\n", Pa_GetErrorText(pa_err));
    stream = NULL;
  }

  if (stream != NULL)
  {
    pa_err = Pa_StartStream(stream);
    if (pa_err != paNoError)
    {
      fprintf(stderr, "Pa_StartStream: %s\n", Pa_GetErrorText(pa_err));
      abort();
    }
  }

  FILE *const audio_file = fopen("audio.raw", "wb");


  // go!

  struct timeval next;
  if (gettimeofday(&next, NULL) < 0)
  {
    perror("gettimeofday");
    abort();
  }

  const int step_us = (int) FRAME_US;

  const int pre_samples = downsample_pre_in_samples(SAMPLE_RATE / OUT_SAMPLE_RATE);
  float sample_buf[pre_samples + RAW_SAMPLE_BUFFER_SIZE];
  memset(sample_buf, 0, pre_samples * sizeof sample_buf[0]);

  for (;;)
  {
    struct timeval now;

    // handle UI stuff
    glfwPollEvents();
    if (glfwWindowShouldClose(window) == GLFW_TRUE)
      break;

    if (gettimeofday(&now, NULL) < 0)
    {
      perror("gettimeofday");
      abort();
    }

    // sleep and loop if not yet time to do stuff
    if (next.tv_sec > now.tv_sec ||
      (next.tv_sec == now.tv_sec && next.tv_usec > now.tv_usec))
    {
      struct timespec ts = {
        .tv_sec = next.tv_sec - now.tv_sec - (int) (next.tv_usec < now.tv_usec),
        .tv_nsec = 1000ll * ((next.tv_usec < now.tv_usec ? 1000000 : 0) +
          next.tv_usec - now.tv_usec)
      };
      if (nanosleep(&ts, NULL) < 0 && errno != EINTR)
      {
        perror("nanosleep");
        abort();
      }
      continue;
    }

    for (int skipped = 0; ; skipped++)
    {
      // read joystick
      bool buttons[2][8];
      input_read_controller(0, buttons[0]);
      input_read_controller(1, buttons[1]);
      system_set_controller_state(&system, 0, buttons[0]);
      system_set_controller_state(&system, 1, buttons[1]);

      // run CPU
      system_run_for_us(&system, step_us);

      if (gettimeofday(&now, NULL) < 0)
      {
        perror("gettimeofday");
        abort();
      }

      if (skipped >= MAX_FRAMESKIP)
        next = now;  // skipped too much; pretend we just caught up

      // move forward time-to-do-stuff clock
      next.tv_usec += step_us;
      if (next.tv_usec > 1000000)
      {
        next.tv_sec++;
        next.tv_usec -= 1000000;
      }

      // if we're caught up, break
      if (next.tv_sec > now.tv_sec ||
        (next.tv_sec == now.tv_sec && next.tv_usec > now.tv_usec))
        break;

      printf("SKIP %i\n", skipped + 1); fflush(stdout);
    }

    if (stream != NULL)
    {
      while (needs_audio(&audio_buf))
      {
        float *const buffer = cur_audio_buf(&audio_buf);
        size_t samples;
        apu_frame(&system.apu, &system.cpu.memory, &sample_buf[pre_samples], &samples);
        assert(samples == SAMPLE_BUFFER_SIZE);
        downsample(SAMPLE_BUFFER_SIZE, OUT_SAMPLE_BUFFER_SIZE, sample_buf, buffer,
          filter_coeffs, filter_state);
        poke_audio(&audio_buf);
//        fwrite(&sample_buf[pre_samples], sizeof sample_buf[0], samples, audio_file);
//        fwrite(buffer, sizeof buffer[0], OUT_SAMPLE_BUFFER_SIZE, audio_file);
        memcpy(sample_buf, &sample_buf[samples], pre_samples * sizeof sample_buf[0]);
      }
    }
    else
    {
      for (int i = 0; i < 4; i++)
      {
        float *const buffer = cur_audio_buf(&audio_buf);
        size_t samples;
        apu_frame(&system.apu, &system.cpu.memory, &sample_buf[pre_samples], &samples);
        downsample(SAMPLE_BUFFER_SIZE, OUT_SAMPLE_BUFFER_SIZE, sample_buf, buffer,
          filter_coeffs, filter_state);
        memcpy(sample_buf, &sample_buf[samples], pre_samples * sizeof sample_buf[0]);
      }
    }

    // render
    render_ntsc_frame_rar_2_1_linear(system_current_frame(&system), *framebuffer);
    graphics_draw(&graphics, *framebuffer);
    glfwSwapBuffers(window);
  }

  // clean up
  fclose(audio_file);
  free(framebuffer);
  graphics_destroy(&graphics);

  if (stream != NULL)
  {
    Pa_StopStream(stream);
    Pa_CloseStream(stream);
  }
  Pa_Terminate();

  glfwDestroyWindow(window);
  glfwTerminate();

  system_destroy(&system);
  free_cart(cart);
  return 0;
}
