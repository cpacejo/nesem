#include <stdbool.h>
#include <stdint.h>
#include "cpu.h"
#include "misc.h"


//
// General helpers
//

#define PROMISE_U8(v) PROMISE((v) >= 0 && (v) < 0x100)

static int as_signed(const int x)
{
  return ((x + 0x80) & 0xFF) - 0x80;
}

static int flags_as_int(const bool p[6])
{
  return
    ((int) p[N] << 7) |
    ((int) p[V] << 6) |
    ((int) p[D] << 3) |
    ((int) p[I] << 2) |
    ((int) p[Z] << 1) |
    (int) p[C];
}

static void flags_from_int(bool p[6], const int flags)
{
  p[N] = (bool) (flags & 0x80);
  p[V] = (bool) (flags & 0x40);
  p[D] = (bool) (flags & 0x08);
  p[I] = (bool) (flags & 0x04);
  p[Z] = (bool) (flags & 0x02);
  p[C] = (bool) (flags & 0x01);
}

__attribute__ ((hot))
static int do_read(const struct cpu *const state, const int addr)
{
  const int page = addr >> 8;
  if (((state->memory.io_read[page >> 6] >> (page & 0x3F)) & 1) == 0)
    return (int) state->memory.pages[page][addr & 0xFF];
  else
    return state->memory.io_read_f(state->memory.io_ctx, addr);
}

static int do_read2(const struct cpu *const state, const int addr)
{
  return do_read(state, addr) + (do_read(state, (addr + 1) & 0xFFFF) << 8);
}

static int do_read2b(const struct cpu *const state, const int addr)
{
  // Note: includes 6502 page-wrapping bug.
  return do_read(state, addr) +
    (do_read(state, (addr & 0xFF00) | ((addr + 1) & 0xFF)) << 8);
}

static void do_write(struct cpu *const state, const int addr, const int val)
{
  const int page = addr >> 8;
  if (((state->memory.io_write[page >> 6] >> (page & 0x3F)) & 1) == 0)
    state->memory.pages[page][addr & 0xFF] = val;
  else
    return state->memory.io_write_f(state->memory.io_ctx, addr, val);
}

static int do_pc_read(struct cpu *const state)
{
  const int ret = do_read(state, state->regs.pc);
  state->regs.pc = (state->regs.pc + 1) & 0xFFFF;
  return ret;
}

static int do_pc_read2(struct cpu *const state)
{
  const int ret = do_read2(state, state->regs.pc);
  state->regs.pc = (state->regs.pc + 2) & 0xFFFF;
  return ret;
}

static void do_interrupt(struct cpu *const state, const int addr,
  const bool no_writes)
{
  PROMISE_U8(state->regs.sp);
  if (!no_writes)
  {
    do_write(state, 0x100 + state->regs.sp,
      (state->regs.pc >> 8) & 0xFF);
    do_write(state, 0x100 + ((state->regs.sp + 0xFF) & 0xFF),
      state->regs.pc & 0xFF);
    do_write(state, 0x100 + ((state->regs.sp + 0xFE) & 0xFF),
      flags_as_int(state->regs.p) | 0x20);
  }
  state->regs.sp = (state->regs.sp + 0xFD) & 0xFF;
  state->regs.pc = do_read2(state, addr);
  state->regs.p[I] = true;
  state->clock += 7;
}


//
// Addressing modes
//

static int addr_abs(struct cpu *const state, const int arg)
{
  state->clock += 2;
  return arg;
}

static int addr_zp(struct cpu *const state, const int arg)
{
  state->clock += 1;
  return arg;
}

static int addr_rel(struct cpu *const state, const int arg)
{
  state->clock += 1 + ((state->regs.pc & 0xFF) + arg >= 0x100);
  return (state->regs.pc + arg) & 0xFFFF;
}

static int addr_absx(struct cpu *const state, const int arg, const bool write)
{
  state->clock += 2 + (write || (arg & 0xFF) + state->regs.x >= 0x100);
  return (arg + state->regs.x) & 0xFFFF;
}

static int addr_absy(struct cpu *const state, const int arg, const bool write)
{
  state->clock += 2 + (write || (arg & 0xFF) + state->regs.y >= 0x100);
  return (arg + state->regs.y) & 0xFFFF;
}

static int addr_zpx(struct cpu *const state, const int arg)
{
  state->clock += 2;
  return (arg + state->regs.x) & 0xFF;
}

static int addr_zpy(struct cpu *const state, const int arg)
{
  state->clock += 2;
  return (arg + state->regs.y) & 0xFF;
}

static int addr_zpxi(struct cpu *const state, const int arg)
{
  state->clock += 4;
  return do_read2b(state, (arg + state->regs.x) & 0xFF);
}

static int addr_zpiy(struct cpu *const state, const int arg, const bool write)
{
  const int i = do_read2b(state, arg);
  state->clock += 3 + (write || i + state->regs.y >= 0x100);
  return (i + state->regs.y) & 0xFFFF;
}


//
// Opcode helpers
//

static void opcode_set_nz(struct cpu *const state, const int a)
{
  PROMISE_U8(a);
  state->regs.p[Z] = (a == 0);
  state->regs.p[N] = (a >= 0x80);
}

static void opcode_set_nza(struct cpu *const state, const int a)
{
  opcode_set_nz(state, a);
  state->regs.a = a;
}

static int opcode_do_lea(struct cpu *const state,
  const int mode, const bool write)
{
  switch (mode)
  {
  case 0:
    return addr_zpxi(state, do_pc_read(state));
  case 1:
    return addr_zp(state, do_pc_read(state));
  case 2:
    return 0;
  case 3:
    return addr_abs(state, do_pc_read2(state));
  case 4:
    return addr_zpiy(state, do_pc_read(state), write);
  case 5:
    return addr_zpx(state, do_pc_read(state));
  case 6:
    return addr_absy(state, do_pc_read2(state), write);
  case 7:
    return addr_absx(state, do_pc_read2(state), write);
  default: PROMISE(false);
  }
}

static int opcode_do_lea_x(struct cpu *const state,
  const int mode, const bool write)
{
  if ((mode & 1) == 0) return 0;

  switch (mode >> 1)
  {
  case 0:
    return addr_zp(state, do_pc_read(state));
  case 1:
    return addr_abs(state, do_pc_read2(state));
  case 2:
    return addr_zpx(state, do_pc_read(state));
  case 3:
    return addr_absx(state, do_pc_read2(state), write);
  default: PROMISE(false);
  }
}

static int opcode_do_lea_y(struct cpu *const state,
  const int mode, const bool write)
{
  if ((mode & 1) == 0) return 0;

  switch (mode >> 1)
  {
  case 0:
    return addr_zp(state, do_pc_read(state));
  case 1:
    return addr_abs(state, do_pc_read2(state));
  case 2:
    return addr_zpy(state, do_pc_read(state));
  case 3:
    return addr_absy(state, do_pc_read2(state), write);
  default: PROMISE(false);
  }
}


//
// The opcodes!
//

// type-A1 (read mem)

static void opcode_adc(struct cpu *const state, const int val)
{
  PROMISE_U8(state->regs.a);
  PROMISE_U8(val);
  const int usum = state->regs.a + state->regs.p[C] + val;
  const int ssum = as_signed(state->regs.a) + state->regs.p[C] + as_signed(val);
  state->regs.p[C] = usum > 0xFF;
  state->regs.p[V] = ssum > 0x7F || ssum < -0x80;
  opcode_set_nza(state, usum & 0xFF);
}

static void opcode_and(struct cpu *const state, const int val)
{
  opcode_set_nza(state, state->regs.a & val);
}

static void opcode_cmp(struct cpu *const state, const int val)
{
  PROMISE_U8(val);
  const int usum = state->regs.a + (0x100 - val);
  state->regs.p[C] = usum > 0xFF;
  opcode_set_nz(state, usum & 0xFF);
}

static void opcode_eor(struct cpu *const state, const int val)
{
  opcode_set_nza(state, state->regs.a ^ val);
}

static void opcode_sta(struct cpu *const state, const int addr)
{
  do_write(state, addr, state->regs.a);
}

static void opcode_lda(struct cpu *const state, const int val)
{
  opcode_set_nza(state, val);
}

static void opcode_ora(struct cpu *const state, const int val)
{
  opcode_set_nza(state, state->regs.a | val);
}

static void opcode_sbc(struct cpu *const state, const int val)
{
  PROMISE_U8(state->regs.a);
  PROMISE_U8(val);
  const int usum = state->regs.a + state->regs.p[C] + (0xFF - val);
  const int ssum = as_signed(state->regs.a) + state->regs.p[C] - 1 - as_signed(val);
  state->regs.p[C] = usum > 0xFF;
  state->regs.p[V] = ssum > 0x7F || ssum < -0x80;
  opcode_set_nza(state, usum & 0xFF);
}

static void opcode_type_a1(struct cpu *const state, const int opcode)
{
  PROMISE_U8(opcode);

  const int mode = (opcode >> 2) & 7;
  const int op = opcode >> 5;
  const int addr = opcode_do_lea(state, mode, op == 4);
  const int val = mode == 2 ? do_pc_read(state) :
    op == 4 ? 0 : do_read(state, addr);

  switch (op)
  {
  case 0: opcode_ora(state, val); break;
  case 1: opcode_and(state, val); break;
  case 2: opcode_eor(state, val); break;
  case 3: opcode_adc(state, val); break;
  case 4: opcode_sta(state, addr); break;
  case 5: opcode_lda(state, val); break;
  case 6: opcode_cmp(state, val); break;
  case 7: opcode_sbc(state, val); break;
  }
}

// type-M (read mem, write mem)

static int opcode_asl(struct cpu *const state, const int val)
{
  PROMISE_U8(val);
  const int res = (val << 1) & 0xFF;
  state->regs.p[C] = val >= 0x80;
  opcode_set_nz(state, res);
  return res;
}

static int opcode_rol(struct cpu *const state, const int val)
{
  PROMISE_U8(val);
  const int res = ((val << 1) & 0xFF) | (int) state->regs.p[C];
  state->regs.p[C] = val >= 0x80;
  opcode_set_nz(state, res);
  return res;
}

static int opcode_lsr(struct cpu *const state, const int val)
{
  PROMISE_U8(val);
  const int res = val >> 1;
  state->regs.p[C] = val & 1;
  opcode_set_nz(state, res);
  return res;
}

static int opcode_ror(struct cpu *const state, const int val)
{
  PROMISE_U8(val);
  const int res = (val >> 1) | ((int) state->regs.p[C] << 7);
  state->regs.p[C] = val & 1;
  opcode_set_nz(state, res);
  return res;
}

static int opcode_dec(struct cpu *const state, const int val)
{
  PROMISE_U8(val);
  const int res = (val + 0xFF) & 0xFF;
  opcode_set_nz(state, res);
  return res;
}

static int opcode_inc(struct cpu *const state, const int val)
{
  PROMISE_U8(val);
  const int res = (val + 1) & 0xFF;
  opcode_set_nz(state, res);
  return res;
}

static void opcode_type_m(struct cpu *const state, const int opcode)
{
  PROMISE_U8(opcode);

  const int mode = (opcode >> 2) & 7;
  const int addr = opcode_do_lea_x(state, mode, true);
  int val;
  if (mode == 2) val = state->regs.a;
  else
  {
    val = do_read(state, addr);
    state->clock += 2;
  }

  int res;
  switch (opcode >> 5)
  {
  case 0: res = opcode_asl(state, val); break;
  case 1: res = opcode_rol(state, val); break;
  case 2: res = opcode_lsr(state, val); break;
  case 3: res = opcode_ror(state, val); break;
  case 4 ... 5: PROMISE(false);  // opcode_type_x
  case 6: res = opcode_dec(state, val); break;
  case 7: res = opcode_inc(state, val); break;
  default: PROMISE(false);
  }

  if (mode == 2) state->regs.a = res;
  else do_write(state, addr, res);
}

// type-B (branch)

static void opcode_type_b(struct cpu *const state, const int opcode)
{
  PROMISE_U8(opcode);
  const int arg = ((do_pc_read(state) + 0x80) & 0xFF) - 0x80;
  if (state->regs.p[opcode >> 6] == (bool) (opcode & 0x20))
    state->regs.pc = addr_rel(state, arg);
}

// type-I1 (immediate)

static void opcode_php(struct cpu *const state)
{
  PROMISE_U8(state->regs.sp);
  do_write(state, 0x100 + state->regs.sp,
    flags_as_int(state->regs.p) | 0x30);
  state->regs.sp = (state->regs.sp + 0xFF) & 0xFF;
  state->clock += 1;
}

static void opcode_plp(struct cpu *const state)
{
  PROMISE_U8(state->regs.sp);
  const int new_sp = (state->regs.sp + 1) & 0xFF;
  flags_from_int(state->regs.p, do_read(state, 0x100 + new_sp));
  state->regs.sp = new_sp;
  state->clock += 2;
}

static void opcode_pha(struct cpu *const state)
{
  PROMISE_U8(state->regs.sp);
  do_write(state, 0x100 + state->regs.sp, state->regs.a);
  state->regs.sp = (state->regs.sp + 0xFF) & 0xFF;
  state->clock += 1;
}

static void opcode_pla(struct cpu *const state)
{
  PROMISE_U8(state->regs.sp);
  const int new_sp = (state->regs.sp + 1) & 0xFF;
  const int res = do_read(state, 0x100 + new_sp);
  opcode_set_nza(state, res);
  state->regs.sp = new_sp;
  state->clock += 2;
}

static void opcode_dey(struct cpu *const state)
{
  state->regs.y = opcode_dec(state, state->regs.y);
}

static void opcode_tay(struct cpu *const state)
{
  state->regs.y = state->regs.a;
  opcode_set_nz(state, state->regs.a);
}

static void opcode_iny(struct cpu *const state)
{
  state->regs.y = opcode_inc(state, state->regs.y);
}

static void opcode_inx(struct cpu *const state)
{
  state->regs.x = opcode_inc(state, state->regs.x);
}

static void opcode_clc(struct cpu *const state)
{
  state->regs.p[C] = false;
}

static void opcode_sec(struct cpu *const state)
{
  state->regs.p[C] = true;
}

static void opcode_cli(struct cpu *const state)
{
  state->regs.p[I] = false;
}

static void opcode_sei(struct cpu *const state)
{
  state->regs.p[I] = true;
}

static void opcode_tya(struct cpu *const state)
{
  opcode_set_nza(state, state->regs.y);
}

static void opcode_clv(struct cpu *const state)
{
  state->regs.p[V] = false;
}

static void opcode_cld(struct cpu *const state)
{
  state->regs.p[D] = false;
}

static void opcode_sed(struct cpu *const state)
{
  state->regs.p[D] = true;
}

static void opcode_type_i1(struct cpu *const state, const int opcode)
{
  PROMISE_U8(opcode);

  switch (opcode >> 4)
  {
  case 0: opcode_php(state); break;
  case 1: opcode_clc(state); break;
  case 2: opcode_plp(state); break;
  case 3: opcode_sec(state); break;
  case 4: opcode_pha(state); break;
  case 5: opcode_cli(state); break;
  case 6: opcode_pla(state); break;
  case 7: opcode_sei(state); break;
  case 8: opcode_dey(state); break;
  case 9: opcode_tya(state); break;
  case 10: opcode_tay(state); break;
  case 11: opcode_clv(state); break;
  case 12: opcode_iny(state); break;
  case 13: opcode_cld(state); break;
  case 14: opcode_inx(state); break;
  case 15: opcode_sed(state); break;
  default: PROMISE(false);
  }
}

// type-I2 (immediate)

static void opcode_txa(struct cpu *const state)
{
  opcode_set_nza(state, state->regs.x);
}

static void opcode_txs(struct cpu *const state)
{
  state->regs.sp = state->regs.x;
}

static void opcode_tax(struct cpu *const state)
{
  state->regs.x = state->regs.a;
  opcode_set_nz(state, state->regs.a);
}

static void opcode_tsx(struct cpu *const state)
{
  state->regs.x = state->regs.sp;
  opcode_set_nz(state, state->regs.sp);
}

static void opcode_dex(struct cpu *const state)
{
  state->regs.x = opcode_dec(state, state->regs.x);
}

static void opcode_nop(struct cpu *const state)
{
  // do nothing!
}

static void opcode_type_i2(struct cpu *const state, const int opcode)
{
  PROMISE_U8(opcode);

  switch ((opcode >> 4) - 8)
  {
  case 0: opcode_txa(state); break;
  case 1: opcode_txs(state); break;
  case 2: opcode_tax(state); break;
  case 3: opcode_tsx(state); break;
  case 4: opcode_dex(state); break;
  case 5 ... 7: opcode_nop(state); break;
  default: PROMISE(false);
  }
}

// type-X (STX/LDX)

static void opcode_stx(struct cpu *const state, const int addr)
{
  do_write(state, addr, state->regs.x);
}

static void opcode_ldx(struct cpu *const state, const int val)
{
  state->regs.x = val;
  opcode_set_nz(state, val);
}

static void opcode_type_x(struct cpu *const state, const int opcode)
{
  PROMISE_U8(opcode);

  const int mode = (opcode >> 2) & 7;
  const int op = opcode >> 5;
  int addr = opcode_do_lea_y(state, mode, op == 4);

  switch (op & 1)
  {
  case 0: opcode_stx(state, addr); break;
  case 1:
    opcode_ldx(state, mode == 0 ? do_pc_read(state) : do_read(state, addr));
    break;
  default: PROMISE(false);
  }
}

// type-A2 (read mem)

static void opcode_bit(struct cpu *const state, const int val)
{
  PROMISE_U8(val);
  PROMISE_U8(state->regs.a);
  state->regs.p[V] = (bool) (val & 0x40);
  state->regs.p[N] = val >= 0x80;
  state->regs.p[Z] = (state->regs.a & val) == 0;
}

static void opcode_sty(struct cpu *const state, const int addr)
{
  do_write(state, addr, state->regs.y);
}

static void opcode_ldy(struct cpu *const state, const int val)
{
  state->regs.y = val;
  opcode_set_nz(state, val);
}

static void opcode_cpy(struct cpu *const state, const int val)
{
  PROMISE_U8(val);
  const int usum = state->regs.y + (0x100 - val);
  state->regs.p[C] = usum > 0xFF;
  opcode_set_nz(state, usum & 0xFF);
}

static void opcode_cpx(struct cpu *const state, const int val)
{
  PROMISE_U8(val);
  const int usum = state->regs.x + (0x100 - val);
  state->regs.p[C] = usum > 0xFF;
  opcode_set_nz(state, usum & 0xFF);
}

static void opcode_type_a2(struct cpu *const state, const int opcode)
{
  PROMISE_U8(opcode);

  const int mode = (opcode >> 2) & 7;
  const int op = opcode >> 5;
  const int addr = opcode_do_lea_x(state, mode, op == 4);
  const int val = mode == 0 ? do_pc_read(state) :
    op == 4 ? 0 : do_read(state, addr);

  switch (op)
  {
  case 0: opcode_nop(state); break;
  case 1: opcode_bit(state, val); break;
  case 2:
  case 3: PROMISE(false);  // handled by opcode_jmp/jmpi
  case 4: opcode_sty(state, addr); break;
  case 5: opcode_ldy(state, val); break;
  case 6: opcode_cpy(state, val); break;
  case 7: opcode_cpx(state, val); break;
  default: PROMISE(false);
  }
}

// JMP

static void opcode_jmp(struct cpu *const state, const int arg)
{
  state->regs.pc = arg;
  state->clock += 1;
}

static void opcode_jmpi(struct cpu *const state, const int arg)
{
  state->regs.pc = do_read2b(state, arg);
  state->clock += 3;
}

// type-F (control flow)

static void opcode_brk(struct cpu *const state)
{
  PROMISE_U8(state->regs.sp);
  do_write(state, 0x100 + state->regs.sp,
    ((state->regs.pc + 1) >> 8) & 0xFF);
  do_write(state, 0x100 + ((state->regs.sp + 0xFF) & 0xFF),
    (state->regs.pc + 1) & 0xFF);
  do_write(state, 0x100 + ((state->regs.sp + 0xFE) & 0xFF),
    flags_as_int(state->regs.p) | 0x30);
  state->regs.sp = (state->regs.sp + 0xFD) & 0xFF;
  state->regs.pc = do_read2(state, 0xFFFE);
  state->regs.p[I] = true;
  state->clock += 4;
}

static void opcode_jsr(struct cpu *const state, const int arg)
{
  PROMISE_U8(state->regs.sp);
  do_write(state, 0x100 + state->regs.sp,
    ((state->regs.pc + 0xFFFF) >> 8) & 0xFF);
  do_write(state, 0x100 + ((state->regs.sp + 0xFF) & 0xFF),
    (state->regs.pc + 0xFFFF) & 0xFF);
  state->regs.sp = (state->regs.sp + 0xFE) & 0xFF;
  state->regs.pc = addr_abs(state, arg);
  state->clock += 4;
}

static void opcode_rts(struct cpu *const state)
{
  PROMISE_U8(state->regs.sp);
  const int low = do_read(state, 0x100 + ((state->regs.sp + 1) & 0xFF));
  const int high = do_read(state, 0x100 + ((state->regs.sp + 2) & 0xFF));
  state->regs.pc = (low + 1 + (high << 8)) & 0xFFFF;
  state->regs.sp = (state->regs.sp + 2) & 0xFF;
  state->clock += 4;
}

static void opcode_rti(struct cpu *const state)
{
  PROMISE_U8(state->regs.sp);
  flags_from_int(state->regs.p,
    do_read(state, 0x100 + ((state->regs.sp + 1) & 0xFF)));
  const int low = do_read(state, 0x100 + ((state->regs.sp + 2) & 0xFF));
  const int high = do_read(state, 0x100 + ((state->regs.sp + 3) & 0xFF));
  state->regs.pc = low | (high << 8);
  state->regs.sp = (state->regs.sp + 3) & 0xFF;
  state->clock += 4;
}

static void opcode_type_f_a2(struct cpu *const state, const int opcode)
{
  PROMISE_U8(opcode);

  int arg;
  switch (opcode >> 5)
  {
  case 0:
    opcode_brk(state);
    break;
  case 1:
    arg = do_pc_read2(state);
    opcode_jsr(state, arg);
    break;
  case 2:
    opcode_rti(state);
    break;
  case 3:
    opcode_rts(state);
    break;
  case 4:
    opcode_nop(state);
    break;
  case 5:
    arg = do_pc_read(state);
    opcode_ldy(state, arg);
    break;
  case 6:
    arg = do_pc_read(state);
    opcode_cpy(state, arg);
    break;
  case 7:
    arg = do_pc_read(state);
    opcode_cpx(state, arg);
    break;
  default:
    PROMISE(false);
  }
}

// wrap it up!

static void do_opcode(struct cpu *const state, const int opcode)
{
  PROMISE_U8(opcode);

  switch (opcode & 3)
  {
  case 0:
    if ((opcode & 4) != 0)
    {
      switch (opcode >> 5)
      {
      case 2:
        // [4-5][4C]
        opcode_jmp(state, do_pc_read2(state));
        break;
      case 3:
        // [6-7][4C]
        opcode_jmpi(state, do_pc_read2(state));
        break;
      default:
        // [0-38-F][4C]
        opcode_type_a2(state, opcode);
        break;
      }
    }
    else
      switch ((opcode >> 3) & 3)
      {
      case 0:
        // [02468ACE]0
        opcode_type_f_a2(state, opcode);
        break;
      case 1:
      case 3:
        // [0-F]8
        opcode_type_i1(state, opcode);
        break;
      case 2:
        // [13579BDF]0
        opcode_type_b(state, opcode);
        break;
      }
    break;

  case 1:
    // [0-F][159D]
    opcode_type_a1(state, opcode);
    break;

  case 2:
    if (opcode >= 0x80 && (opcode & 0xC) == 8)
      // [8-F]A
      opcode_type_i2(state, opcode);
    else if ((opcode >> 6) == 2)
      // [8-B][26E]
      opcode_type_x(state, opcode);
    else
      // [0-7][26AE] [C-F][26E]
      opcode_type_m(state, opcode);
    break;

  case 3:
    // [0-F][37BF]
    // garbage; treat as NOP
    opcode_nop(state);
    break;
  }
}

void cpu_step(struct cpu *const state)
{
  const bool nmi = !state->old_nmi && state->nmi;
  state->old_nmi = state->nmi;

  if (nmi)
    do_interrupt(state, 0xFFFA, false);
  else if (state->irq && !state->regs.p[I])
    do_interrupt(state, 0xFFFE, false);
  else
  {
    const int opcode = do_read(state, state->regs.pc);
    state->regs.pc++;
    state->clock += 2;
    do_opcode(state, opcode);
  }

}

void cpu_reset(struct cpu *const state)
{
  state->old_nmi = state->nmi;
  do_interrupt(state, 0xFFFC, true);
}


//
// serialization
//
#if 0
#include <stdio.h>
#include <string.h>
#include "sljson.h"

#define J(f, args...) do { const int ret = sljson_##f(args); if (ret < 0) return ret; } while (0)

int cpu_serialize(sljson_writer_t *const w, FILE *const f, const struct cpu *const cpu)
{
#define W(o, args...) J(write_##o, w, f, #args)
#define WN(name, o, args...) do { W(z_name, name); W(o, #args); } while (0)
  W(begin_object);
    WN("old_nmi", bool, cpu->old_nmi);
    WN("regs", begin_object);
      WN("a", integer, cpu->regs.a);
      WN("x", integer, cpu->regs.x);
      WN("y", integer, cpu->regs.y);
      WN("sp", integer, cpu->regs.sp);
      WN("pc", integer, cpu->regs.pc);
      WN("p", begin_object);
        WN("n", bool, cpu->regs.p[N]);
        WN("v", bool, cpu->regs.p[V]);
        WN("c", bool, cpu->regs.p[C]);
        WN("z", bool, cpu->regs.p[Z]);
        WN("d", bool, cpu->regs.p[D]);
        WN("i", bool, cpu->regs.p[I]);
      W(end_object);
    W(end_object);
  W(end_object);
#undef WN
#undef W

  return 0;
}

#define NAME_BUF_SIZE 256

static int deserialize_flags(sljson_reader_t *const r, FILE *const f, bool p[6])
{
#define P(o, d) J(peek_##o, r, f, &(d))
#define C(o) J(consume_##o, r, f)
#define R(o, args...) J(read_##o, r, f, #args)

  bool found[6] = { };

  enum sljson_type t;
  P(type, t);
  if (t != SLJSON_OBJECT) return -1;

  C(begin_object);

  for (;;)
  {
    bool is_end;
    P(end_object, is_end);
    if (is_end) break;

    char name[NAME_BUF_SIZE];
    R(name, name, sizeof name, false);

    // THINKING: need to check type?
    if (strcmp(name, "n") == 0)
    {
      R(bool, &p[N]);
      found[N] = true;
    }

    if (strcmp(name, "v") == 0)
    {
      R(bool, &p[V]);
      found[V] = true;
    }

    if (strcmp(name, "c") == 0)
    {
      R(bool, &p[C]);
      found[C] = true;
    }

    if (strcmp(name, "z") == 0)
    {
      R(bool, &p[Z]);
      found[Z] = true;
    }

    if (strcmp(name, "d") == 0)
    {
      R(bool, &p[D]);
      found[D] = true;
    }

    if (strcmp(name, "i") == 0)
    {
      R(bool, &p[I]);
      found[I] = true;
    }
  }

  C(end_object);

#undef P
#undef C
#undef R

  for (int i = 0; i < 6; i++)
    if (!found[i]) return -1;

  return 0;
}

int cpu_deserialize(sljson_reader_t *const r, FILE *const f, struct cpu *const cpu)
{
}
#undef J
#endif
