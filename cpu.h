#ifndef CPU_H
#define CPU_H

#include <stdbool.h>
#include <stdint.h>

enum cpu_flag
{
  // don't change order!
  N, V, C, Z, D, I
};

struct cpu_regs
{
  int a, x, y, sp, pc;
  bool p[6];
};

struct cpu_memory
{
  void *io_ctx;
  void (*io_write_f)(void *ctx, int addr, int val);
  int (*io_read_f)(void *ctx, int addr);
  uint64_t io_write[4];
  uint64_t io_read[4];
  uint8_t *pages[256];
};

struct cpu
{
  long long clock;
  bool old_nmi, nmi, irq;
  struct cpu_regs regs;
  struct cpu_memory memory;
};

void cpu_step(struct cpu *);
void cpu_reset(struct cpu *)
  __attribute__ ((cold));

/*
#include "sljson.h"
int cpu_serialize(sljson_writer_t *, FILE *, const struct cpu *)
  __attribute__ ((cold));
int cpu_deserialize(sljson_reader_t *, FILE *, struct cpu *)
  __attribute__ ((cold));
*/
#endif
