#include <assert.h>
#include <stddef.h>
#include <math.h>
#include "apu.h"
#include "misc.h"
#include "audio.h"

// We put the knee enough above 20 kHz not to affect audible freqs,
// and enough below 28 kHz so that aliasing about 24 kHz doesn't
// extend below 20 kHz.  We can get away with this *only* because
// we're the last output stage; if the audio were being post-processed
// we'd want to roll off completely before 24 kHz.
#define BUTTERWORTH_REL_F (22.0/48.0)
#define BUTTERWORTH_ORDER 6
#define LANCZOS_ORDER 2
#define FILTER_HALF_WIDTH ((float) LANCZOS_ORDER)

static float lanczos_approx_coeff;

__attribute__ ((constructor))
static void init_lanczos_approx_coeff(void)
{
  // derived from Taylor series
  lanczos_approx_coeff = -M_PI * M_PI * (1.0 + 1.0 / ((double) LANCZOS_ORDER * (double) LANCZOS_ORDER)) / 6.0;
}

__attribute__ ((hot
#ifndef __clang__  // clang lies and claims it's GCC
, optimize ("O3")  // `gcc -Os -ffast-math` replaces 1/(x*x) with pow()...
#endif
))
static float lanczos_kernel(const float x)
{
  if (UNLIKELY(fabsf(x) < (1.0f / 1024.0f)))
    // simple and very accurate approximation
    return 1.0f + lanczos_approx_coeff * (x * x);
  else
  {
    const float pi_x = (float) M_PI * x;
    if (LANCZOS_ORDER == 2)
    {
      // half-angle identity lets us use sincos + sqrt
      // which is faster than sin + sin
      const float s = sinf(pi_x), c = cosf(pi_x);
      return (2.0f * s * copysignf(sqrtf(0.5f * (1.0f - c)), pi_x)) / (pi_x * pi_x);
    }
    else
    {
      const float pi_x_w = (float) (M_PI / (double) LANCZOS_ORDER) * x;
      return (sinf(pi_x) * sinf(pi_x_w)) / (pi_x * pi_x_w);
    }
  }
}

static float base_in_x_0(const float in_per_out)
{
  // what (un-delayed) input x does output x 0 correspond to?
  // 0.665
  return 0.5f * in_per_out - 0.5f;
}

static float downsample_in_delay(const float in_per_out)
{
  // in: |   |   | 1 |   |   |
  // out: |     |  1  |     |
  // filt: -----+-------)
  // delay:          |==|
  // i.e., how much delay do we need to add
  // so we don't sample "future" input samples
  // (exclusive bound)
  // 0.335
  return fmaxf(0.0f, (FILTER_HALF_WIDTH - 1.0f) - base_in_x_0(in_per_out));
}

int downsample_pre_in_samples(const float in_per_out)
{
  // in:   |   |   | 0 |   |   |
  // out:   |     |  0  |     |
  // delay:          |==|
  // filt:   (-------+-------)
  // pre:      X   X
  // i.e., how many samples do we need
  // before the actual samples?
  // 1.0
  return (int) ceilf(2.0f * downsample_in_delay(in_per_out));
}

static float adjusted_in_x_0(const float in_per_out)
{
  // accounting for delay and pre-samples,
  // what input x does output x 0 correspond to?
  // 1.33 -> (0, 1, 2, 3)
  // 465 -> (464, 465, 466)
  return (float) downsample_pre_in_samples(in_per_out)
    + base_in_x_0(in_per_out)
    - downsample_in_delay(in_per_out);
}

void compute_downsample_filter_coeffs(const float in_per_out,
  struct biquad filter_coeffs[restrict DOWNSAMPLE_FILTER_ORDER / 2])
{
  for (int stage = 0; stage < DOWNSAMPLE_FILTER_ORDER / 2; stage++)
  {
    double a1, a2, b0, b1, b2;
    butterworth_lowpass_iir(BUTTERWORTH_REL_F / in_per_out,
      BUTTERWORTH_ORDER, stage, &a1, &a2, &b0, &b1, &b2);
    filter_coeffs[stage].a1 = (float) a1;
    filter_coeffs[stage].a2 = (float) a2;
    filter_coeffs[stage].b0 = (float) b0;
    filter_coeffs[stage].b1 = (float) b1;
    filter_coeffs[stage].b2 = (float) b2;
  }
}

static void apply_biquad(const struct biquad *const coeffs,
  const size_t samples, float buffer[], float state[2])
{
  const float b0 = coeffs->b0;
  const float a1 = coeffs->a1;
  const float b1 = coeffs->b1;
  const float a2 = coeffs->a2;
  const float b2 = coeffs->b2;

  float z1 = state[0], z2 = state[1];
  for (int i = 0; i < samples; i++)
  {
    const float z0 = buffer[i] - a1 * z1 - a2 * z2;
    buffer[i] = b0 * z0 + b1 * z1 + b2 * z2;
    z2 = z1;
    z1 = z0;
  }
  state[0] = z1;
  state[1] = z2;
}

void downsample(
  const size_t samples_in, const size_t samples_out,
  float in[restrict], float out[restrict],
  const struct biquad filter_coeffs[restrict DOWNSAMPLE_FILTER_ORDER / 2],
  float filter_state[restrict DOWNSAMPLE_FILTER_ORDER])
{
  const float in_per_out = (float) samples_in / (float) samples_out;
  const int pre_in_samples = downsample_pre_in_samples(in_per_out);
  const float in_x_0 = adjusted_in_x_0(in_per_out);

  for (int i = 0; i < DOWNSAMPLE_FILTER_ORDER / 2; i++)
    apply_biquad(&filter_coeffs[i], samples_in, &in[pre_in_samples], &filter_state[i * 2]);

  for (int i = 0; i < samples_out; i++)
  {
    float in_x_i;
    const float in_x_f = modff(in_x_0 + in_per_out * (float) i, &in_x_i);
    const int in_x = (int) in_x_i;

    float sum = 0.0f;
    // TODO: we could optimize this slightly,
    // since the sinc sin just flips sign
    // (among other optimizations)
    for (int j = 1 - LANCZOS_ORDER; j < LANCZOS_ORDER; j++)
      sum += in[in_x + j] * lanczos_kernel(in_x_f - (float) j);
    // Need to special-case this, b/c the very last sample
    // could be integer-aligned and hence past the end of input.
    if (LIKELY(in_x_f != 0.0f))
      sum += in[in_x + LANCZOS_ORDER] * lanczos_kernel(in_x_f - (float) LANCZOS_ORDER);
    out[i] = sum;
  }
}
