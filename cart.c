#define _GNU_SOURCE
#include <fcntl.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include "cart.h"

struct cart *load_cart(FILE *const file, const char *const name,
  enum cart_error *const error)
{
  // parse header

  uint8_t header[16];
  if (fread(header, sizeof header, 1, file) < 1)
  {
    if (error != NULL) *error = CART_ERROR_BAD_HEADER;
    return NULL;
  }

  if (header[0] != 'N' || header[1] != 'E' || header[2] != 'S' || header[3] != 0x1A)
  {
    if (error != NULL) *error = CART_ERROR_BAD_HEADER;
    return NULL;
  }

  const size_t prg_rom_size = header[4] * (size_t) 16384;
  const size_t chr_rom_size = header[5] * (size_t) 8192;
  const size_t chr_ram_size = chr_rom_size == 0 ? 8192 : 0;

  const enum mirroring mirroring =
    (header[6] & 1) == 0 ? MIRRORING_HORIZONTAL : MIRRORING_VERTICAL;

  const bool persistent_prg_ram = (header[6] & 2) != 0;

  if ((header[6] & 0xC) != 0)
  {
    if (error != NULL) *error = CART_ERROR_UNSUPPORTED_FEATURE;
    return NULL;
  }

  enum mapper_type mapper_type;
  switch ((header[7] & 0xF0) | (header[6] >> 4))
  {
  case 0: mapper_type = MAPPER_TYPE_NROM; break;
  case 1: mapper_type = MAPPER_TYPE_MMC1; break;
  case 2: mapper_type = MAPPER_TYPE_UXROM; break;
  case 3: mapper_type = MAPPER_TYPE_CNROM; break;
  case 4: mapper_type = MAPPER_TYPE_MMC3; break;
  case 5: mapper_type = MAPPER_TYPE_MMC5; break;
  default:
    if (error != NULL) *error = CART_ERROR_UNKNOWN_MAPPER;
    return NULL;
  }

  if ((header[7] & 0xC) != 0)
  {
    if (error != NULL) *error = CART_ERROR_UNSUPPORTED_FORMAT;
    return NULL;
  }

  const size_t prg_ram_size = (size_t) 8192 *
    (header[8] == 0 && (
      mapper_type == MAPPER_TYPE_MMC1 ||
      mapper_type == MAPPER_TYPE_MMC3 ||
      mapper_type == MAPPER_TYPE_MMC5
    ) ? 1 : header[8]);

  // read data  

  void *const prg_rom = malloc(prg_rom_size);
  void *const chr_rom = malloc(chr_rom_size);

  if (prg_rom_size > 0 && fread(prg_rom, prg_rom_size, 1, file) < 1)
  {
    if (error != NULL) *error = CART_ERROR_BAD_DATA;
    free(prg_rom);
    free(chr_rom);
    return NULL;
  }

  if (chr_rom_size > 0 && fread(chr_rom, chr_rom_size, 1, file) < 1)
  {
    if (error != NULL) *error = CART_ERROR_BAD_DATA;
    free(prg_rom);
    free(chr_rom);
    return NULL;
  }

  // allocate PRG RAM

  void *prg_ram;

  if (persistent_prg_ram)
  {
    char *filename;
    asprintf(&filename, "%s.sav", name);
    const int fd = open(filename, O_RDWR | O_CREAT,
      S_IRUSR | S_IWUSR | S_IWGRP | S_IRGRP | S_IROTH | S_IWOTH);
    free(filename);
    if (fd < 0)
    {
      *error = CART_ERROR_CANT_CREATE_SAVE_FILE;
      free(prg_rom);
      free(chr_rom);
      return NULL;
    }

    if (ftruncate(fd, prg_ram_size) < 0)
    {
      *error = CART_ERROR_CANT_CREATE_SAVE_FILE;
      close(fd);
      free(prg_rom);
      free(chr_rom);
      return NULL;
    }

    prg_ram = mmap(NULL, prg_ram_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    close(fd);
    if (prg_ram == NULL)
    {
      *error = CART_ERROR_CANT_CREATE_SAVE_FILE;
      free(prg_rom);
      free(chr_rom);
      return NULL;
    }
  }
  else
    prg_ram = calloc(1, prg_ram_size);

  struct cart *const cart = malloc(sizeof (struct cart));
  cart->mapper_type = mapper_type;
  cart->mirroring = mirroring;
  cart->persistent_prg_ram = persistent_prg_ram;
  cart->prg_rom = prg_rom;
  cart->prg_rom_size = prg_rom_size;
  cart->chr_rom = chr_rom;
  cart->chr_rom_size = chr_rom_size;
  cart->prg_ram = prg_ram;
  cart->prg_ram_size = prg_ram_size;
  cart->chr_ram = calloc(1, chr_ram_size);
  cart->chr_ram_size = chr_ram_size;
  return cart;
}

void cart_sync_prg_ram(struct cart *const cart)
{
  if (!cart->persistent_prg_ram) return;

  // Flush, but do not wait.
  // (While it would be nice to provide a user notice
  // after waiting, we don't.)
  if (msync(cart->prg_ram, cart->prg_ram_size, MS_ASYNC) < 0)
    abort();
}

void free_cart(struct cart *const cart)
{
  free(cart->prg_rom);
  free(cart->chr_rom);
  if (cart->persistent_prg_ram)
    munmap(cart->prg_ram, cart->prg_ram_size);
  else
    free(cart->prg_ram);
  free(cart->chr_ram);
  free(cart);
}
