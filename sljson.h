#ifndef SLJSON_H
#define SLJSON_H

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>
#if __STDC_VERSION__ >= 201112L && !defined(__clang__)
#include <uchar.h>
#else
typedef uint_fast16_t char16_t;
typedef uint_fast32_t char32_t;
#endif
#include <sys/types.h>

/* Design notes:
 *
 * Reads do not consume more than one byte beyond end of token,
 * which is pushed back into the stream.
 *
 * skip -> consume, discard  (only minimal format checking!)
 * consume -> check format, consume, discard
 * read -> check format, consume, store
 * peek -> (none of the above; only minimal format checking!)
 *
 * After an error, the writer/reader and stream is left in an undefined state.
 */

//
// writer
//

typedef struct sljson_writer sljson_writer_t;

// You must initialize a writer before using it.
// A writer may be re-used for multiple JSON terms; they will
// be separated with white space.
void sljson_init_writer(sljson_writer_t *writer);

enum sljson_encoding
{
  SLJSON_ASCII,  // encode all codepoints outside of the range 0x20-0x7F
  SLJSON_BMP_UTF8,  // encode all codepoints outside the range 0x20-0xFFFF
  SLJSON_UNICODE_UTF8  // only 0x00-0x1F are encoded
};

// `printable_only` escapes all Unicode "Default Ignorable" characters,
// all "Newline" characters, all "Invisible Placeholder" characters,
// and all "Noncharacters".  Why?  Because these (by definition) might not
// find their way through a display routine.
void sljson_set_writer_output_style(sljson_writer_t *writer,
  enum sljson_encoding encoding, bool printable_only);

void sljson_destroy_writer(sljson_writer_t *writer);

int sljson_write_null(sljson_writer_t *writer, FILE *file);

int sljson_write_bool(sljson_writer_t *writer, FILE *file, bool value);

// UTF-8 (or ASCII)
int sljson_write_z_string(sljson_writer_t *writer, FILE *file, const char *value);
int sljson_write_string(sljson_writer_t *writer, FILE *file, const char *value, size_t length);

int sljson_write_latin1_z_string(sljson_writer_t *writer, FILE *file, const unsigned char *value);
int sljson_write_latin1_string(sljson_writer_t *writer, FILE *file, const unsigned char *value, size_t length);
int sljson_write_bmp_z_string(sljson_writer_t *writer, FILE *file, const char16_t *value);
int sljson_write_bmp_string(sljson_writer_t *writer, FILE *file, const char16_t *value, size_t length);
int sljson_write_unicode_z_string(sljson_writer_t *writer, FILE *file, const char32_t *value);
int sljson_write_unicode_string(sljson_writer_t *writer, FILE *file, const char32_t *value, size_t length);

#if __STDC_VERSION__ >= 201112L
#define sljson_write_unicode_z_string(writer, file, value) _Generic(value, \
  unsigned char *: sljson_write_latin1_z_string, \
  char16_t *: sljson_write_bmp_z_string, \
  char32_t *: sljson_write_unicode_z_string)(writer, file, value)
#define sljson_write_unicode_string(writer, file, value, length) _Generic(value, \
  unsigned char *: sljson_write_latin1_string, \
  char16_t *: sljson_write_bmp_string, \
  char32_t *: sljson_write_unicode_string)(writer, file, value, length)
#endif

// will never have decimal point or exponent
int sljson_write_integer(sljson_writer_t *writer, FILE *file, intmax_t value);
int sljson_write_u_integer(sljson_writer_t *writer, FILE *file, uintmax_t value);
// enough digits to represent float exactly; will always have decimal point
int sljson_write_float(sljson_writer_t *writer, FILE *file, float value);
// enough digits to represent double exactly; will always have decimal point
int sljson_write_double(sljson_writer_t *writer, FILE *file, double value);
// may have decimal point or exponent even if exp == 0
int sljson_write_decimal(sljson_writer_t *writer, FILE *file, intmax_t sig, int exp);
int sljson_write_decimal2(sljson_writer_t *writer, FILE *file,
  int sign, uintmax_t sig, uintmax_t frac, int frac_digits, int exp);
// packed BCD, big-endian, optional sign in rightmost nibble (D for negative, C for positive)
int sljson_write_bcd(sljson_writer_t *writer, FILE *file,
  const unsigned char *sig, size_t length, int exp);

int sljson_write_begin_array(sljson_writer_t *writer, FILE *file);
int sljson_write_end_array(sljson_writer_t *writer, FILE *file);

int sljson_write_begin_object(sljson_writer_t *writer, FILE *file);

// UTF-8 (or ASCII)
int sljson_write_z_name(sljson_writer_t *writer, FILE *file, const char *name);
int sljson_write_name(sljson_writer_t *writer, FILE *file, const char *name, size_t length);

int sljson_write_latin1_z_name(sljson_writer_t *writer, FILE *file, const unsigned char *name);
int sljson_write_latin1_name(sljson_writer_t *writer, FILE *file, const unsigned char *name, size_t length);
int sljson_write_bmp_z_name(sljson_writer_t *writer, FILE *file, const char16_t *name);
int sljson_write_bmp_name(sljson_writer_t *writer, FILE *file, const char16_t *name, size_t length);
int sljson_write_unicode_z_name(sljson_writer_t *writer, FILE *file, const char32_t *name);
int sljson_write_unicode_name(sljson_writer_t *writer, FILE *file, const char32_t *name, size_t length);

#if __STDC_VERSION__ >= 201112L
#define sljson_write_unicode_z_name(writer, file, value) _Generic(value, \
  void *: sljson_write_latin1_z_name, \
  unsigned char *: sljson_write_latin1_z_name, \
  char16_t *: sljson_write_bmp_z_name, \
  char32_t *: sljson_write_unicode_z_name)(writer, file, value)
#define sljson_write_unicode_name(writer, file, value, length) _Generic(value, \
  void *: sljson_write_latin1_name, \
  unsigned char *: sljson_write_latin1_name, \
  char16_t *: sljson_write_bmp_name, \
  char32_t *: sljson_write_unicode_name)(writer, file, value, length)
#endif

int sljson_write_end_object(sljson_writer_t *writer, FILE *file);


//
// reader
//

typedef struct sljson_reader sljson_reader_t;

enum sljson_type
{
  SLJSON_NULL,
  SLJSON_BOOLEAN,
  SLJSON_NUMBER,
  SLJSON_STRING,
  SLJSON_ARRAY,
  SLJSON_OBJECT
};

void sljson_init_reader(sljson_reader_t *reader);

void sljson_destroy_reader(sljson_reader_t *reader);

// can record/change position by copying/restoring reader state & file position
void sljson_copy_reader(sljson_reader_t *restrict dest_reader,
  const sljson_reader_t *restrict src_reader);

// does not advance file position past token
int sljson_peek_type(sljson_reader_t *reader, FILE *file, enum sljson_type *type_out);

int sljson_skip_value(sljson_reader_t *reader, FILE *file);

// do we actually need this?
int sljson_peek_null(sljson_reader_t *reader, FILE *file, bool *is_null_out);

int sljson_consume_null(sljson_reader_t *reader, FILE *file);

int sljson_read_bool(sljson_reader_t *reader, FILE *file, bool *value_out);

int sljson_read_bcd(sljson_reader_t *reader, FILE *file,
  unsigned char *restrict sig_out, size_t length, int *exp_out);

// UTF-8
ssize_t sljson_read_string(sljson_reader_t *reader, FILE *file,
  char *restrict value, size_t length, bool allow_nuls);

ssize_t sljson_read_ascii_string(sljson_reader_t *reader, FILE *file,
  char *restrict value, size_t length, bool allow_nuls);
ssize_t sljson_read_latin1_string(sljson_reader_t *reader, FILE *file,
  unsigned char *restrict value, size_t length, bool allow_nuls);
ssize_t sljson_read_bmp_string(sljson_reader_t *reader, FILE *file,
  char16_t *restrict value, size_t length, bool allow_nuls);
ssize_t sljson_read_unicode_string(sljson_reader_t *reader, FILE *file,
  char32_t *restrict value, size_t length, bool allow_nuls);

#if __STDC_VERSION__ >= 201112L
#define sljson_read_unicode_string(reader, file, value, length, allow_nuls) _Generic(value, \
  char *: sljson_read_ascii_string, \
  unsigned char *: sljson_read_latin1_string, \
  char16_t *: sljson_read_bmp_string, \
  char32_t *: sljson_read_unicode_string)(reader, file, value, length, allow_nuls)
#endif

int sljson_consume_begin_array(sljson_reader_t *reader, FILE *file);
int sljson_peek_end_array(sljson_reader_t *reader, FILE *file, bool *is_end_out);
int sljson_consume_end_array(sljson_reader_t *reader, FILE *file);

int sljson_consume_begin_object(sljson_reader_t *reader, FILE *file);

int sljson_skip_name_value(sljson_reader_t *reader, FILE *file);

// UTF-8
ssize_t sljson_read_name(sljson_reader_t *reader, FILE *file,
  char *restrict name, size_t length, bool allow_nuls);

ssize_t sljson_read_ascii_name(sljson_reader_t *reader, FILE *file,
  char *restrict name, size_t length, bool allow_nuls);
ssize_t sljson_read_latin1_name(sljson_reader_t *reader, FILE *file,
  unsigned char *restrict name, size_t length, bool allow_nuls);
ssize_t sljson_read_bmp_name(sljson_reader_t *reader, FILE *file,
  char16_t *restrict name, size_t length, bool allow_nuls);
ssize_t sljson_read_unicode_name(sljson_reader_t *reader, FILE *file,
  char32_t *restrict name, size_t length, bool allow_nuls);

#if __STDC_VERSION__ >= 201112L
#define sljson_read_unicode_name(reader, file, name, length, allow_nuls) _Generic(name, \
  char *: sljson_read_ascii_name, \
  unsigned char *: sljson_read_latin1_name, \
  char16_t *: sljson_read_bmp_name, \
  char32_t *: sljson_read_unicode_name)(reader, file, name, length, allow_nuls)
#endif

int sljson_peek_end_object(sljson_reader_t *reader, FILE *file, bool *is_end_out);
int sljson_consume_end_object(sljson_reader_t *reader, FILE *file);


// higher-level object interface
// requires seekable stream

typedef struct sljson_object sljson_object_t;

int sljson_read_object(sljson_reader_t *reader, FILE *file, sljson_object_t *object_out);
//int sljson_seek_value(sljson_reader_t *reader, FILE *file, const sljson_object_t *object, XXX name);


// private

#define SLJSON_MAX_DEPTH 256

struct sljson_writer
{
  bool ascii_only, bmp_only, printable_only;
  bool start_of_compound;
  unsigned int depth;
};

struct sljson_reader
{
};

#define SLJSON_OBJECT_MAX_NAMES 1024

struct sljson_object
{
  unsigned int num_names;
  struct { sljson_reader_t reader; long pos; } names[SLJSON_OBJECT_MAX_NAMES];
};

#endif
