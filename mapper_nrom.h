#ifndef MAPPER_NROM_H
#define MAPPER_NROM_H

#include "cart.h"
#include "cpu.h"
#include "ppu.h"

struct nrom_config
{
  enum mirroring mirroring;
};

struct nrom_memory
{
  void *prg_bank[2];  // 16 KiB
  void *chr_bank;  // 8 KiB
};

struct nrom
{
  struct nrom_config config;
  struct nrom_memory memory;
};


int nrom_config(struct nrom *, const struct cart *)
  __attribute__ ((cold));

void nrom_init(struct nrom *, struct cpu *, struct ppu *)
  __attribute__ ((cold));

#endif
