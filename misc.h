#ifndef MISC_H
#define MISC_H

#define is_power_of_2(x) \
  ({ __typeof__ (x) _x = (x); _x > 0 && ((_x - 1) & _x) == 0; })

#define max(x, y) ({ \
  __typeof__ (x) _x = (x); __typeof__ (y) _y = (y); \
  _x < _y ? _y : _x; })

#define min(x, y) ({ \
  __typeof__ (x) _x = (x); __typeof__ (y) _y = (y); \
  _x > _y ? _y : _x; })

#ifdef __clang__
typedef float v2f __attribute__ ((ext_vector_type (2)));
typedef float v4f __attribute__ ((ext_vector_type (4)));
#else
typedef float v2f __attribute__ ((vector_size (8)));
typedef float v4f __attribute__ ((vector_size (16)));
#endif

#include <stdint.h>

#define explode_bits(x) \
  ({ int _x = (x); \
     (((_x & 0xFE) * UINT64_C(0x02040810204080)) | _x) & UINT64_C(0x0101010101010101); })

#define explode_bits_rev(x) \
  ({ int _x = (x); \
     ((_x * UINT64_C(0x8040201008040201)) >> 7) & UINT64_C(0x0101010101010101); })

#define PROMISE(c) if (!(c)) __builtin_unreachable()

#define LIKELY(c) __builtin_expect(c, 1)
#define UNLIKELY(c) __builtin_expect(c, 0)

#include <stddef.h>

// clang/OS X lies about C11 support
void *aligned_alloc(size_t, size_t)
  __attribute__ ((weak));

#endif
