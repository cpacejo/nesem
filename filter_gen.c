#include <assert.h>
#include <math.h>
#include <stdio.h>
#include "filter_gen.h"

/* What's going on here?
 *
 * We model an NES pixel as a horizontal line.
 * That is, unit rectangle in the horizontal direction,
 * and unit impulse in the vertical direction.
 *
 * We want to render the image using square pixels,
 * either 640x480 (NTSC standard; RAR of 1:1), or
 * 320x240 (more typical RAR of 2:1, and eliminates
 * scanline artifacts).  (An RAR of 2:1 gives a look
 * you would see on the 13" Daewoo in your bedroom;
 * an RAR of 1:1, your rich uncle's Trinitron.)
 * (See http://www.repairfaq.org/sam/crtfaq.htm#crtdir
 * for definition of RAR.)
 *
 * Technically we should go to 704x480 or 352x240,
 * but since these don't form nice ratios with 280,
 * we'd need more complicated filters.  The RAR
 * filtering is an approximation anyway.
 *
 * The vertical upsampling is trivial;
 * because the NES pixel is an impulse in that direction,
 * the target vertical dimension is an integer multiple
 * of the number of NES scanlines, and sinc is 0
 * at integer multiples, we can just insert black lines,
 * and accordingly scale the brightness of the non-black lines.
 * Of course from x240 to x240 it is a no-op.
 * (Additionally, this means we can render scanline-by-scanline.)
 *
 * For the horizontal dimension, we are going from
 * 7 NES pixels to either 8 or 16 output pixels,
 * *and* the NES pixel is a unit rectangle in that direction.
 * So here, we convolve a Lanczos-3 filter of the *output*
 * bandwidth with the (NES) unit rectangle to produce a
 * filter we can use at runtime for upsampling.
 *
 * Finally, we do provide for rendering an RAR of 1:1
 * upsampled by 1.5x, *offset* by 1/2 vertical output pixel.
 * Since we're no longer sampling the sinc peak,
 * we gain (most of) the headroom necessary to
 * double the brightness.  (This offset has the convenient
 * side effect of retaining one black output line.)
 * Here we use only Lanczos-2 for the vertical upsampling
 * in order to simplify the computation a bit.
 * (Lanczos-3 would require interpolating 3 adjancent
 * input scanlines instead of 2.)  The limitation of
 * this method is that any further scaling is apt to
 * induce clipping.
 *
 * Note that in the RAR 2:1 case, we actually output a 640x240
 * image; 320x240 is "theoretically" lossless, but there is too
 * much important content near the Nyquist frequencey
 * (e.g. character eyeballs that are single pixels).  (This is
 * not a problem in the vertical dimension since the input and
 * output grids are exactly aligned.)
 *
 * These outputs are therefore non-lossy with respect to
 * the image they are meant to contain.  They can be
 * arbitrary upscaled later in the rendering pipeline
 * (preferably with Lanczos) to whatever size is desired.
 * 
 * The result is an "idealized" NES rendering model,
 * which is nonetheless rather visually pleasing.
 */

#define INTEG_STEPS 1023  // must be multiple of 3

#if 0
static double lanczos2(const double x)
{
  if (x == 0.0) return 1.0;
  else if (fabs(x) < 2.0)
    return 2.0 * sin(M_PI * x) * sin((M_PI * x) / 2.0)
      / ((M_PI * x) * (M_PI * x));
  else return 0.0;
}
#endif

static double lanczos3(const double x)
{
  if (x == 0.0) return 1.0;
  else if (fabs(x) < 3.0)
    return 3.0 * sin(M_PI * x) * sin((M_PI * x) / 3.0)
      / ((M_PI * x) * (M_PI * x));
  else return 0.0;
}

static double lanczos3_integ(const double lower, const double upper)
{
  // Simpson's 3/8 rule
  const double h = (upper - lower) * (1.0 / INTEG_STEPS);
  double threes = lanczos3(lower + h) + lanczos3(upper - h), twos = 0.0;
  for (int i = 2; i + 2 < INTEG_STEPS; i += 3)
  {
    threes += lanczos3(lower + (double) i * h);
    twos += lanczos3(lower + (double) (i + 1) * h);
    threes += lanczos3(lower + (double) (i + 2) * h);
  }
  const double sum = lanczos3(lower) + lanczos3(upper) + 3.0 * threes + 2.0 * twos;
  return sum * (upper - lower) * (3.0 / (8.0 * INTEG_STEPS));
}

// Surprisingly, the integral of a Lanczos filter is not quite 1.
// NOTE: Not a constructor because then we have to ensure we're linked
// in the right order w/r/t render.o.
static double lanczos3_scale(void)
{
  return 1.0 / lanczos3_integ(-3.0, 3.0);
}

static double lanczos3_conv_rect(const double l3f, const double x)
{
  return lanczos3_integ(l3f * (x - 0.5), l3f * (x + 0.5)) * lanczos3_scale();
}

static double lanczos3_conv_step(const double x)
{
  return lanczos3_integ(-3.0, x) * lanczos3_scale();
}

int rar_2_1_min_ix(const int ox)
{
  return (int) ceil((0.5 * (double) ox - (3.0 + (4.0/7.0))) * (7.0/8.0) - (9.0/32.0));
}

int rar_1_1_min_ix(const int ox)
{
  return (int) ceil(((double) ox - (3.0 + (8.0/7.0))) * (7.0/16.0) - (9.0/32.0));
}

double rar_2_1_coeff(const int ox, const int ix)
{
  return lanczos3_conv_rect(8.0/7.0,
    (double) ox * (7.0/16.0) - ((double) ix + (9.0/32.0)));
}

double rar_1_1_coeff(const int ox, const int ix)
{
  return lanczos3_conv_rect(16.0/7.0,
    (double) ox * (7.0/16.0) - ((double) ix + (9.0/32.0)));
}

double step_coeff(const int x, const double phase)
{
  // account for rendering, which adds 1
  // for all x strictly greatly than phase
  return lanczos3_conv_step((double) x - phase) -
    (double) ((double) x > phase);
}

void lowpass_iir(const double rel_f,
  double *const a1, double *const b0, double *const b1)
{
  const double c = cos(M_PI * rel_f);
  const double s = sin(M_PI * rel_f);

  *b0 = s / (s + c);
  *b1 = s / (s + c);
  *a1 = (s - c) / (s + c);
}

void highpass_iir(const double rel_f,
  double *const a1, double *const b0, double *const b1)
{
  const double c = cos(M_PI * rel_f);
  const double s = sin(M_PI * rel_f);

  *b0 = c / (s + c);
  *b1 = -c / (s + c);
  *a1 = (s - c) / (s + c);
}

void butterworth_lowpass_iir(const double rel_f, const int order,
  const int stage, double *const a1, double *const a2,
  double *const b0, double *const b1, double *const b2)
{
  assert(order > 0 && order % 2 == 0);
  assert(stage >= 0 && stage < order / 2);

  const double k =
    -2.0 * cos(M_PI * (2.0 * (double) stage + (double) order + 1.0) /
      (2.0 * order));

  const double c = cos(M_PI * rel_f);
  const double s = sin(M_PI * rel_f);

  const double d = 1.0 + k * c * s;
  *b0 = (s * s) / d;
  *b1 = (2.0 * s * s) / d;
  *b2 = (s * s) / d;
  *a1 = (2.0 * (s * s - c * c)) / d;
  *a2 = (1.0 - k * c * s) / d;
}

int __attribute__ ((weak, cold))
main(int argc, char *argv[])
{
  printf("RAR 1:1\n");

  printf("       ");
  for (int ix = -3; ix < 10; ix++)
    printf("  i[%2i] ", ix);
  printf("\n");

  for (int ox = 0; ox < 16; ox++)
  {
    printf("o[%2i] =", ox);
    for (int ix = -3; ix < 10; ix++)
    {
      const double c = rar_1_1_coeff(ox, ix);

      if (ix < rar_1_1_min_ix(ox) || ix >= rar_1_1_min_ix(ox) + RAR_1_1_FILTER_WIDTH)
      {
        assert(c == 0.0);
        printf("        ");
      }
      else
        printf(" % 2.4f", c);
    }
    printf("\n");
  }


#if 0
  printf("\nRAR 1:1 -> 1.5\n");

  printf("       ");
  for (int ix = -3; ix < 10; ix++)
    printf("  i[%2i] ", ix);
  printf("\n");

  for (int ox = 0; ox < 24; ox++)
  {
    printf("o[%2i] =", ox);
    for (int ix = -3; ix < 10; ix++)
    {
      const double c =
        lanczos3_conv_rect(16.0/7.0,
          (double) ox * (7.0/24.0) - ((double) ix + (17.0/48.0)));

      if (fabs((double) ox/1.5 - ((double) ix + (17.0/48.0)) * (16.0/7.0)) > (3.0 + (8.0/7.0)))
      {
          assert(c == 0.0);
        printf("        ");
      }
      else
        printf(" % 2.4f", c);
    }
    printf("\n");
  }

  const double scale = lanczos2(1.0/3.0) + lanczos2(5.0/3.0);
  printf("\nvert: % 2.4f, % 2.4f  (scale: % 2.4f)\n",
    lanczos2(1.0/3.0) / scale, lanczos2(5.0/3.0) / scale, scale);
#endif


  printf("\nRAR 2:1\n");

  printf("       ");
  for (int ix = -3; ix < 10; ix++)
    printf("  i[%2i] ", ix);
  printf("\n");

  for (int ox = 0; ox < RAR_2_1_OUTPUT_BLOCK_SIZE; ox++)
  {
    printf("o[%2i] =", ox);
    for (int ix = -3; ix < 10; ix++)
    {
      const double c = rar_2_1_coeff(ox, ix);

      if (ix < rar_2_1_min_ix(ox) || ix >= rar_2_1_min_ix(ox) + RAR_2_1_FILTER_WIDTH)
      {
        assert(c == 0.0);
        printf("        ");
      }
      else
        printf(" % 2.4f", c);
    }
    printf("\n");
  }


  printf("\nstep\n");

  printf("            ");
  for (int x = STEP_FILTER_MIN_X; x <= STEP_FILTER_MAX_X; x++)
    printf("  i[%2i] ", x);
  printf("\n");

  for (int phase = 0; phase < 8; phase++)
  {
    printf("phase % 2i/ 8:", phase);
    for (int x = STEP_FILTER_MIN_X; x <= STEP_FILTER_MAX_X; x++)
      printf(" % 2.4f", step_coeff(x, (double) phase / 8.0));
    printf("\n");
  }

  return 0;
}
