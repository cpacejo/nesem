#include <assert.h>
#include <stdint.h>
#include "cart.h"
#include "cpu.h"
#include "ppu.h"
#include "mapper_nrom.h"

int nrom_config(struct nrom *const nrom, const struct cart *const cart)
{
  assert(cart->mapper_type == MAPPER_TYPE_NROM);

  if (cart->prg_rom_size != 16 << 10 && cart->prg_rom_size != 32 << 10)
    return -1;

  if (cart->chr_rom_size != 8 << 10)
    return -1;

  if (cart->prg_ram_size != 0)
    return -1;

  if (cart->chr_ram_size != 0)
    return -1;

  nrom->config.mirroring = cart->mirroring;

  for (int i = 0; i < 2; i++)
    nrom->memory.prg_bank[i] =
      (char *) cart->prg_rom + ((i << 14) & (cart->prg_rom_size - 1));

  nrom->memory.chr_bank = cart->chr_rom;

  return 0;
}

void nrom_init(struct nrom *const nrom,
  struct cpu *const cpu, struct ppu *const ppu)
{
  ppu->memory.name_attribute_table[0] =
    &ppu->ram->name_attribute_table[0];

  ppu->memory.name_attribute_table[1] =
    &ppu->ram->name_attribute_table[(int) (nrom->config.mirroring == MIRRORING_VERTICAL)];

  ppu->memory.name_attribute_table[2] =
    &ppu->ram->name_attribute_table[(int) (nrom->config.mirroring == MIRRORING_HORIZONTAL)];

  ppu->memory.name_attribute_table[3] =
    &ppu->ram->name_attribute_table[1];

  for (int i = 0; i < 128; i++)
    cpu->memory.pages[128 + i] =
      (uint8_t *) nrom->memory.prg_bank[i >> 6] + ((i & 0x3F) << 8);

  for (int i = 0; i < 8; i++)
    ppu->memory.pattern_table_1k[i] =
      (struct pattern_table_1k *) nrom->memory.chr_bank + i;
}
