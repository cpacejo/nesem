#version 410 core

uniform ivec2 dims;

in vec2 position;
out vec2 coord;

void main()
{
  gl_Position = vec4(position, 0.0, 1.0);
  coord = vec2(dims) * vec2(0.5, -0.5) * (position + vec2(1.0, -1.0)) - vec2(0.5, 0.5);
}
