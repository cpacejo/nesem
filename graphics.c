#include <stdio.h>
#include <stdlib.h>
#define GL_GLEXT_PROTOTYPES
#define GLFW_INCLUDE_GLCOREARB
#include <GLFW/glfw3.h>
#include "graphics.h"

#define POSITION_ATTRIB 0
#define SCALING_TEXTURE GL_TEXTURE0

static GLfloat vertices[] = { -1.0f, -1.0f, 1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f };

extern const GLchar vertex_shader_source[];
extern const GLchar scaling_fragment_shader_source[];

static void check_error(void)
{
  switch (glGetError())
  {
  case GL_NO_ERROR: return;
  case GL_INVALID_ENUM: fprintf(stderr, "invalid enum\n"); fflush(stderr); break;
  case GL_INVALID_VALUE: fprintf(stderr, "invalid value\n"); fflush(stderr); break;
  case GL_INVALID_OPERATION: fprintf(stderr, "invalid operation\n"); fflush(stderr); break;
  case GL_INVALID_FRAMEBUFFER_OPERATION: fprintf(stderr, "invalid framebuffer operation\n"); fflush(stderr); break;
  }

  abort();
}

void graphics_init(struct graphics *const g)
{
  // allocate stuff
  g->scaling_program = glCreateProgram();
  glGenBuffers(1, &g->vertex_buffer);
  glGenVertexArrays(1, &g->vertex_array);
  glGenTextures(1, &g->texture);

  // global setup
  glEnable(GL_FRAMEBUFFER_SRGB);

  // compile shaders
  const GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
  const GLuint scaling_fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(vertex_shader, 1, (const char *[]) { vertex_shader_source }, NULL);
  glShaderSource(scaling_fragment_shader, 1, (const char *[]) { scaling_fragment_shader_source }, NULL);
  glCompileShader(vertex_shader);
  glCompileShader(scaling_fragment_shader);
  char log[65536];
  glGetShaderInfoLog(scaling_fragment_shader, sizeof log, NULL, log);
  fprintf(stderr, "%s\n", log); fflush(stdout);
  glAttachShader(g->scaling_program, vertex_shader);
  glAttachShader(g->scaling_program, scaling_fragment_shader);
  glBindAttribLocation(g->scaling_program, POSITION_ATTRIB, "position");
  glBindFragDataLocation(g->scaling_program, 0, "color");
  glLinkProgram(g->scaling_program);
  glDetachShader(g->scaling_program, vertex_shader);
  glDetachShader(g->scaling_program, scaling_fragment_shader);
  glDeleteShader(vertex_shader);
  glDeleteShader(scaling_fragment_shader);
  glReleaseShaderCompiler();
  glUseProgram(g->scaling_program);

  // set up vertex buffer
  glBindBuffer(GL_ARRAY_BUFFER, g->vertex_buffer);
  glBufferData(GL_ARRAY_BUFFER, sizeof vertices, vertices, GL_STATIC_DRAW);
  glBindVertexArray(g->vertex_array);
  glVertexAttribPointer(POSITION_ATTRIB, 2, GL_FLOAT, GL_FALSE, 0, NULL);

  // set up texture
  glActiveTexture(SCALING_TEXTURE);
  glBindTexture(GL_TEXTURE_RECTANGLE, g->texture);
  glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
  glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
  const GLint tex_uniform = glGetUniformLocation(g->scaling_program, "tex");
  glUniform1i(tex_uniform, 0);

  // config
  const GLint dims_uniform = glGetUniformLocation(g->scaling_program, "dims");
  glUniform2i(dims_uniform, 640, 240);

  check_error();
}

void graphics_draw(struct graphics *const g, const float framebuffer[240][640][3])
{
  glActiveTexture(SCALING_TEXTURE);
  glTexImage2D(GL_TEXTURE_RECTANGLE, 0, GL_RGB32F,
    640, 240, 0, GL_RGB, GL_FLOAT, framebuffer);

  glEnableVertexAttribArray(POSITION_ATTRIB);
  glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
}

void graphics_destroy(struct graphics *const g)
{
  glDeleteTextures(1, &g->texture);
  glDeleteVertexArrays(1, &g->vertex_array);
  glDeleteBuffers(1, &g->vertex_buffer);
  glDeleteProgram(g->scaling_program);
}
