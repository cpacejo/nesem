#ifndef SYSTEM_H
#define SYSTEM_H

#include <stdint.h>
#include "apu.h"
#include "cart.h"
#include "controller.h"
#include "cpu.h"
#include "mapper.h"
#include "ppu.h"

struct system
{
  long long clock_us;
  struct cart *cart;
  struct mapper mapper;
  struct cpu cpu;
  struct apu apu;
  struct ppu ppu;
  struct controller controller;
  uint8_t *ram;
  struct frame (*frame)[2];
  int valid_frame;
};

int system_config(struct system *, struct cart *)
  __attribute__ ((cold));

void system_init(struct system *)
  __attribute__ ((cold));

void system_run_for_us(struct system *, long long us);

const struct frame *system_current_frame(const struct system *);

void system_set_controller_state(struct system *,
  int which, const bool buttons[8]);

void system_destroy(struct system *)
  __attribute__ ((cold));

#endif
