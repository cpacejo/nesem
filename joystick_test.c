#include <stdbool.h>
#include <stdio.h>
#include <unistd.h>
#include <GLFW/glfw3.h>

int main(int argc, char *argv[])
{
  glfwInit();

  for (;;)
  {
    bool found = false;

    for (int i = GLFW_JOYSTICK_1; i <= GLFW_JOYSTICK_LAST; i++)
    {
      if (glfwJoystickPresent(i) == GLFW_TRUE)
      {
        printf("[% 2i] \"%s\":\n", (i - GLFW_JOYSTICK_1) + 1, glfwGetJoystickName(i));

        int axis_count;
        const float *const axes = glfwGetJoystickAxes(i, &axis_count);

        int button_count;
        const unsigned char *const buttons = glfwGetJoystickButtons(i, &button_count);

        printf("  Axes:");
        for (int j = 0; j < axis_count; j++)
          printf(" % 1.2f", (double) axes[j]);

        printf("  Buttons:");
        for (int j = 0; j < button_count; j++)
          printf(" %c", buttons[j] == GLFW_PRESS ? 'X' : '_');

        printf("\n");

        found = true;
      }
    }

    if (!found) printf("(none)\n");

    sleep(1);
  }

  glfwTerminate();
  return 0;
}
