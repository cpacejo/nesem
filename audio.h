#ifndef AUDIO_H
#define AUDIO_H

#include <stddef.h>

#define DOWNSAMPLE_FILTER_ORDER 6  // must be even

__attribute__ ((cold, const))
int downsample_pre_in_samples(float in_per_out);

struct biquad
{
  float a1, a2, b0, b1, b2;
};

__attribute__ ((cold))
void compute_downsample_filter_coeffs(float in_per_out,
  struct biquad filter_coeffs[restrict DOWNSAMPLE_FILTER_ORDER / 2]);

// NOTE: In gets modified (downsampled);
// it *should* be used when copying its end to its start.
void downsample(
  size_t samples_in, size_t samples_out,
  float in[restrict], float out[restrict],
  const struct biquad filter_coeffs[restrict DOWNSAMPLE_FILTER_ORDER / 2],
  float filter_state[restrict DOWNSAMPLE_FILTER_ORDER]);

#endif
