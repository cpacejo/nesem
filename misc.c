#include <stddef.h>
#include <stdlib.h>
#include "misc.h"

void *aligned_alloc(const size_t alignment, const size_t size)
{
  return malloc(size);
}
