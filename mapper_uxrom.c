#include <assert.h>
#include <stdint.h>
#include "cpu.h"
#include "misc.h"
#include "ppu.h"
#include "mapper_uxrom.h"

int uxrom_config(struct uxrom *const uxrom, const struct cart *const cart)
{
  assert(cart->mapper_type == MAPPER_TYPE_UXROM);

  if (!is_power_of_2(cart->prg_rom_size) ||
    cart->prg_rom_size < 16 << 10 || cart->prg_rom_size > 256 << 10)
    return -1;

  if (cart->chr_rom_size != 0)
    return -1;

  if (cart->prg_ram_size != 0)
    return -1;

  if (cart->chr_ram_size != 8192)
    return -1;

  uxrom->config.mirroring = cart->mirroring;

  for (int i = 0; i < 16; i++)
    uxrom->memory.prg_bank[i] =
      (char *) cart->prg_rom + ((i << 14) & (cart->prg_rom_size - 1));

  uxrom->memory.chr_bank = cart->chr_ram;

  return 0;
}

static void update_prg_16k_bank_helper(struct uxrom_memory *const memory,
  struct cpu *const cpu, const int which, const int bank)
{
  for (int i = 0; i < 64; i++)
    cpu->memory.pages[i + ((which + 2) << 6)] =
      (uint8_t *) memory->prg_bank[bank] + (i << 8);
}

void uxrom_init(struct uxrom *const uxrom,
  struct cpu *const cpu, struct ppu *const ppu)
{
  ppu->memory.name_attribute_table[0] =
    &ppu->ram->name_attribute_table[0];

  ppu->memory.name_attribute_table[1] =
    &ppu->ram->name_attribute_table[(int) (uxrom->config.mirroring == MIRRORING_VERTICAL)];

  ppu->memory.name_attribute_table[2] =
    &ppu->ram->name_attribute_table[(int) (uxrom->config.mirroring == MIRRORING_HORIZONTAL)];

  ppu->memory.name_attribute_table[3] =
    &ppu->ram->name_attribute_table[1];

  update_prg_16k_bank_helper(&uxrom->memory, cpu, 0, 0);
  update_prg_16k_bank_helper(&uxrom->memory, cpu, 1, 15);

  for (int i = 0; i < 8; i++)
    ppu->memory.pattern_table_1k[i] =
      (struct pattern_table_1k *) uxrom->memory.chr_bank + i;
}

static void write_bank_select(struct uxrom *const uxrom,
  struct cpu *const cpu, const int bank)
{
  update_prg_16k_bank_helper(&uxrom->memory, cpu, 0, bank & 15);
}

void uxrom_write(struct uxrom *const uxrom,
  struct cpu *const cpu, struct ppu *const ppu,
  const int addr, const int value)
{
  write_bank_select(uxrom, cpu, value);
}
