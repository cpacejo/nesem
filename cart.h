#ifndef CART_H
#define CART_H

#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>

enum mapper_type
{
  MAPPER_TYPE_NROM,
  MAPPER_TYPE_MMC1,
  MAPPER_TYPE_UXROM,
  MAPPER_TYPE_CNROM,
  MAPPER_TYPE_MMC3,
  MAPPER_TYPE_MMC5
};

enum mirroring
{
  MIRRORING_VERTICAL,
  MIRRORING_HORIZONTAL
};

struct cart
{
  enum mapper_type mapper_type;
  enum mirroring mirroring;
  bool persistent_prg_ram;
  void *prg_rom; size_t prg_rom_size;
  void *chr_rom; size_t chr_rom_size;
  void *prg_ram; size_t prg_ram_size;
  void *chr_ram; size_t chr_ram_size;
};

enum cart_error
{
  CART_ERROR_BAD_HEADER,
  CART_ERROR_UNSUPPORTED_FEATURE,
  CART_ERROR_UNSUPPORTED_FORMAT,
  CART_ERROR_UNKNOWN_MAPPER,
  CART_ERROR_BAD_DATA,
  CART_ERROR_CANT_CREATE_SAVE_FILE
};

struct cart *load_cart(FILE *, const char *name, enum cart_error *)
  __attribute__ ((cold));

void cart_sync_prg_ram(struct cart *)
  __attribute__ ((cold));

void free_cart(struct cart *)
  __attribute__ ((cold));

#endif
