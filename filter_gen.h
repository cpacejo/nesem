#ifndef FILTER_GEN_H
#define FILTER_GEN_H

#define RAR_2_1_FILTER_WIDTH 7
#define RAR_1_1_FILTER_WIDTH 4

#define RAR_2_1_INPUT_BLOCK_SIZE 7
#define RAR_2_1_OUTPUT_BLOCK_SIZE 16
#define RAR_1_1_INPUT_BLOCK_SIZE 7
#define RAR_1_1_OUTPUT_BLOCK_SIZE 16

int rar_2_1_min_ix(int ox)
  __attribute__ ((cold, const));
int rar_1_1_min_ix(int ox)
  __attribute__ ((cold, const));
double rar_2_1_coeff(int ox, int ix)
  __attribute__ ((cold, const));
double rar_1_1_coeff(int ox, int ix)
  __attribute__ ((cold, const));

#define STEP_FILTER_WIDTH 6
#define STEP_FILTER_MIN_X (-2)
#define STEP_FILTER_MAX_X (STEP_FILTER_MIN_X + STEP_FILTER_WIDTH - 1)

// Coefficients for a filtered step response.
// `phase` is really just subtracted from `x`
// but the intent is to generate the coefficients
// for each of several fractional phases.
// Does NOT include the step response itself,
// which is 1.0 wherever x >= phase.
double step_coeff(int x, double phase)
  __attribute__ ((cold, const));

void lowpass_iir(double rel_f,
  double *a1, double *b0, double *b1)
  __attribute__ ((cold));

void highpass_iir(double rel_f,
  double *a1, double *b0, double *b1)
  __attribute__ ((cold));

void butterworth_lowpass_iir(double rel_f, int order,
  int stage, double *a1, double *a2,
  double *b0, double *b1, double *b2)
  __attribute__ ((cold));

#endif
