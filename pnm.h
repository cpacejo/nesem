#ifndef PNM_H
#define PNM_H

#include <stdio.h>
#include "ppu.h"

int export_frame_pnm_rar_1_1(const struct frame *, FILE *)
  __attribute__ ((cold));
int export_frame_pnm_rar_2_1(const struct frame *, FILE *)
  __attribute__ ((cold));
int export_pattern_tables_pnm(struct pattern_table_1k *[8], FILE *)
  __attribute__ ((cold));

#endif
