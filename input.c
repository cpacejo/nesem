#include <stdbool.h>
#include <GLFW/glfw3.h>
#include "input.h"

#define AXIS_THRESH 0.5f

static int controller_map[2]  // -1 -> not present
  = { GLFW_JOYSTICK_1, -1 };
static int button_map[2][8]
  = { { 3, 9, 2, 1, 4, 4, 3, 3 } };
static enum {
  NOT_MAPPED, BUTTON, POS_AXIS, NEG_AXIS
} button_type[2][8]
  = { { BUTTON, BUTTON, BUTTON, BUTTON, NEG_AXIS, POS_AXIS, NEG_AXIS, POS_AXIS } };

void input_read_controller(const int which, bool pressed[8])
{
  if (controller_map[which] < 0)
  {
    for (int i = 0; i < 8; i++)
      pressed[i] = false;
    return;
  }

  int axis_count;
  const float *const axes = glfwGetJoystickAxes(controller_map[which], &axis_count);

  int button_count;
  const unsigned char *const buttons = glfwGetJoystickButtons(controller_map[which], &button_count);

  for (int i = 0; i < 8; i++)
  {
    const int j = button_map[which][i];

    switch (button_type[which][i])
    {
    case NOT_MAPPED:
      pressed[i] = false;

    case BUTTON:
      pressed[i] = j < button_count && buttons[j] == GLFW_PRESS;
      break;

    case POS_AXIS:
      pressed[i] = j < axis_count && axes[j] >= AXIS_THRESH;
      break;

    case NEG_AXIS:
      pressed[i] = j < axis_count && axes[j] <= -AXIS_THRESH;
      break;
    }
  }
}
