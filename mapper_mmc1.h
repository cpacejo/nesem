#ifndef MAPPER_MMC1_H
#define MAPPER_MMC1_H

#include <stdbool.h>
#include "cart.h"
#include "cpu.h"
#include "ppu.h"

enum mmc1_prg_bank_mode
{
  MMC1_PRG_BANK_32_KB,
  MMC1_PRG_BANK_16_KB_UPPER,
  MMC1_PRG_BANK_16_KB_LOWER
};

enum mmc1_chr_bank_mode
{
  MMC1_CHR_BANK_8_KB,
  MMC1_CHR_BANK_4_KB
};

struct mmc1_regs
{
  enum mmc1_prg_bank_mode prg_bank_mode;
  enum mmc1_chr_bank_mode chr_bank_mode;
  int prg_bank, chr_bank_0, chr_bank_1;
};

struct mmc1_memory
{
  void *prg_bank[16];  // 16 KiB
  void *chr_bank[32];  // 4 KiB
  bool has_prg_ram;
  void *prg_ram;  // 8 KiB
};

struct mmc1
{
  int shift;
  struct mmc1_regs regs;
  struct mmc1_memory memory;
};


int mmc1_config(struct mmc1 *, const struct cart *)
  __attribute__ ((cold));

void mmc1_init(struct mmc1 *, struct cpu *, struct ppu *)
  __attribute__ ((cold));

void mmc1_write(struct mmc1 *, struct cpu *, struct ppu *,
  int addr, int value);

#endif
