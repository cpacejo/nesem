#ifndef MAPPER_CNROM_H
#define MAPPER_CNROM_H

#include "cart.h"
#include "cpu.h"
#include "ppu.h"

struct cnrom_config
{
  enum mirroring mirroring;
};

struct cnrom_memory
{
  void *prg_bank[2];  // 16 KiB
  void *chr_bank[4];  // 8 KiB
};

struct cnrom
{
  struct cnrom_config config;
  struct cnrom_memory memory;
};


int cnrom_config(struct cnrom *, const struct cart *)
  __attribute__ ((cold));

void cnrom_init(struct cnrom *, struct cpu *, struct ppu *)
  __attribute__ ((cold));

void cnrom_write(struct cnrom *, struct cpu *, struct ppu *,
  int addr, int value);

#endif
