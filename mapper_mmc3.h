#ifndef MAPPER_MMC3_H
#define MAPPER_MMC3_H

#include <stdbool.h>
#include "cart.h"
#include "cpu.h"
#include "ppu.h"

enum mmc3_prg_bank_mode
{
  // values are important!
  MMC3_PRG_BANK_1_FIXED = 0,
  MMC3_PRG_BANK_0_FIXED = 1
};

enum mmc3_chr_bank_mode
{
  // values are important!
  MMC3_CHR_BANK_2_KB_1_KB = 0,
  MMC3_CHR_BANK_1_KB_2_KB = 1
};

struct mmc3_regs
{
  int chr_bank[6], prg_bank[2];
  enum mmc3_prg_bank_mode prg_bank_mode;
  enum mmc3_chr_bank_mode chr_bank_mode;
  int irq_latch, irq_counter;
  bool irq_enable, irq_reload, irq;
};

struct mmc3_memory
{
  void *prg_bank[64];  // 8 KiB
  void *chr_bank[256];  // 1 KiB
  bool has_prg_ram;
  void *prg_ram;  // 8 KiB
};

struct mmc3
{
  int reg;
  bool need_prg_ram_sync;
  struct mmc3_regs regs;
  struct mmc3_memory memory;
};


int mmc3_config(struct mmc3 *, const struct cart *)
  __attribute__ ((cold));

void mmc3_init(struct mmc3 *, struct cpu *, struct ppu *)
  __attribute__ ((cold));

void mmc3_write(struct mmc3 *, struct cpu *, struct ppu *,
  int addr, int value);

void mmc3_scanline(struct mmc3 *);

inline bool mmc3_poll_irq(const struct mmc3 *const mmc3)
{
  return mmc3->regs.irq;
}

#endif
