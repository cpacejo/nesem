#ifndef INPUT_H
#define INPUT_H

#include <stdbool.h>

enum button
{
  BUTTON_A = 0,
  BUTTON_B = 1,
  BUTTON_SELECT = 2,
  BUTTON_START = 3,
  BUTTON_UP = 4,
  BUTTON_DOWN = 5,
  BUTTON_LEFT = 6,
  BUTTON_RIGHT = 7
};

void input_read_controller(int which, bool pressed[8]);

#endif
