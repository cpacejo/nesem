#include <assert.h>
#include <pthread.h>
#include <stdbool.h>
#include <stddef.h>
#include <GLFW/glfw3.h>
#include "graphics.h"
#include "render.h"
#include "system.h"
#include "graphics_thread.h"

void graphics_thread_control_init(struct graphics_thread_control *const control)
{
  pthread_mutex_init(&control->mutex);
  pthread_cond_init(&control->cond);
  control->stop = false;
  control->dirty = false;
}

void graphics_thread_control_destroy(struct graphics_thread_control *const control)
{
  pthread_cond_destroy(&control->cond);
  pthread_mutex_destroy(&control->mutex);
}

void graphics_thread_notify_stop(struct graphics_thread_control *const control)
{
  pthread_mutex_lock(&control->mutex);
  control->stop = true;
  pthread_cond_signal(&control->cond);
  pthread_mutex_unlock(&control->mutex);
}

void graphics_thread_notify_dirty(struct graphics_thread_control *const control)
{
  pthread_mutex_lock(&control->mutex);
  control->dirty = true;
  pthread_cond_signal(&control->cond);
  pthread_mutex_unlock(&control->mutex);
}

void *graphics_thread_f(void *const arg)
{
  const struct graphics_thread_context *const context = arg;

  glfwMakeContextCurrent(context->window);
  glfwSwapInterval(1);

  pthread_mutex_lock(&context->control->mutex);

  for (;;)
  {
    while (!context->control->dirty && !context->control->stop)
      pthread_cond_wait(&context->control->cond, &context->control->mutex);
    if (context->control->stop) break;
    assert(context->control->dirty);
    pthread_mutex_unlock(&context->control->mutex);

    pthread_mutex_lock(context->current_frame_mutex);
    render_ntsc_frame_rar_2_1_linear(system_current_frame(context->system), *context->framebuffer);
    pthread_mutex_lock(&context->control->mutex);
    context->control->dirty = false;
    pthread_mutex_unlock(&context->control->mutex);
    pthread_mutex_unlock(context->current_frame_mutex);

    graphics_draw(context->graphics, *context->framebuffer);
    glfwSwapBuffers(context->window);

    pthread_mutex_lock(&context->control->mutex);
  }

  pthread_mutex_unlock(&context->control->mutex)

  return NULL;
}
