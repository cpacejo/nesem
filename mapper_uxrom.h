#ifndef MAPPER_UXROM_H
#define MAPPER_UXROM_H

#include "cart.h"
#include "cpu.h"
#include "ppu.h"

struct uxrom_config
{
  enum mirroring mirroring;
};

struct uxrom_memory
{
  void *prg_bank[16];  // 16 KiB
  void *chr_bank;  // 8 KiB
};

struct uxrom
{
  struct uxrom_config config;
  struct uxrom_memory memory;
};


int uxrom_config(struct uxrom *, const struct cart *)
  __attribute__ ((cold));

void uxrom_init(struct uxrom *, struct cpu *, struct ppu *)
  __attribute__ ((cold));

void uxrom_write(struct uxrom *, struct cpu *, struct ppu *,
  int addr, int value);

#endif
