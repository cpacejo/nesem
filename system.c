#include <stdint.h>
#include <stdlib.h>
#include "cart.h"
#include "controller.h"
#include "apu.h"
#include "cpu.h"
#include "mapper.h"
#include "ppu.h"
#include "system.h"

static void io_write_f(void *const ctx, const int addr, const int val)
{
  struct system *const system = ctx;

  if (addr >= 0x8000)
    mapper_write(&system->mapper, &system->cpu, &system->ppu, addr, val);
  else if (addr < 0x4000)
  {
    switch (addr & 7)
    {
    case 0: ppu_write_control(&system->ppu, val); break;
    case 1: ppu_write_mask(&system->ppu, val); break;
    case 3: ppu_write_oam_addr(&system->ppu, val); break;
    case 4: ppu_write_oam_data(&system->ppu, val); break;
    case 5: ppu_write_scroll(&system->ppu, val); break;
    case 6: ppu_write_addr(&system->ppu, val); break;
    case 7: ppu_write_data(&system->ppu, val); break;
    default: break;
    }
  }
  else if (addr < 0x4020)
  {
    switch (addr & 0x1F)
    {
    case 0x00: apu_write_pulse_1_envelope(&system->apu, val); break;
    case 0x01: apu_write_pulse_1_sweep(&system->apu, val); break;
    case 0x02: apu_write_pulse_1_timer_low(&system->apu, val); break;
    case 0x03: apu_write_pulse_1_timer_high(&system->apu, val); break;
    case 0x04: apu_write_pulse_2_envelope(&system->apu, val); break;
    case 0x05: apu_write_pulse_2_sweep(&system->apu, val); break;
    case 0x06: apu_write_pulse_2_timer_low(&system->apu, val); break;
    case 0x07: apu_write_pulse_2_timer_high(&system->apu, val); break;
    case 0x08: apu_write_triangle_linear(&system->apu, val); break;
    case 0x0A: apu_write_triangle_timer_low(&system->apu, val); break;
    case 0x0B: apu_write_triangle_timer_high(&system->apu, val); break;
    case 0x0C: apu_write_noise_envelope(&system->apu, val); break;
    case 0x0E: apu_write_noise_timer(&system->apu, val); break;
    case 0x0F: apu_write_noise_length(&system->apu, val); break;
    case 0x10: apu_write_dmc_rate(&system->apu, val); break;
    case 0x11: apu_write_dmc_direct(&system->apu, val); break;
    case 0x12: apu_write_dmc_address(&system->apu, val); break;
    case 0x13: apu_write_dmc_length(&system->apu, val); break;
    case 0x14: ppu_write_oam_dma(&system->ppu, &system->cpu, val); break;
    case 0x15: apu_write_control(&system->apu, val); break;
    case 0x16: controller_write_strobe(&system->controller, val); break;
    case 0x17: apu_write_frame_counter(&system->apu, val); break;
    default: break;
    }
  }
}

static int io_read_f(void *const ctx, const int addr)
{
  struct system *const system = ctx;

  if (addr < 0x4000)
  {
    switch (addr & 7)
    {
    case 2: return ppu_read_status(&system->ppu);
    case 7: return ppu_read_data(&system->ppu);
    default: return 0;
    }
  }
  else if (addr < 0x4020)
  {
    switch (addr & 0x1F)
    {
    case 0x15: return apu_read_status(&system->apu);
    case 0x16: return controller_read_0(&system->controller);
    case 0x17: return controller_read_1(&system->controller);
    default: return 0;
    }
  }
  else
    return 0;
}

int system_config(struct system *const system, struct cart *const cart)
{
  system->cart = cart;
  return mapper_config(&system->mapper, cart);
}

void system_init(struct system *const system)
{
  system->clock_us = 0;

  system->ram = calloc(0x800, 1);
  system->frame = calloc(2, sizeof (*system->frame)[0]);
  system->valid_frame = 0;

  system->cpu = (struct cpu) {
    .memory = {
      .io_ctx = system,
      .io_write_f = io_write_f,
      .io_read_f = io_read_f,
      .io_write = { UINT64_C(0xFFFFFFFF00000000), ~UINT64_C(0),
        ~UINT64_C(0), ~UINT64_C(0) },
      .io_read = { UINT64_C(0xFFFFFFFF00000000), ~UINT64_C(0),
        0, 0 }
    }
  };
  for (int i = 0; i < 0x20; i++)
    system->cpu.memory.pages[i] = system->ram + ((i & 7) << 8);

  system->apu = (struct apu) { };

  system->ppu = (struct ppu) {
    .ram = calloc(1, sizeof (struct ppu_ram))
  };

  system->controller = (struct controller) { };

  mapper_init(&system->mapper, &system->cpu, &system->ppu);
  apu_init(&system->apu);
  cpu_reset(&system->cpu);
}

void system_run_for_us(struct system *const system, const long long us)
{
  const long long target_us = system->clock_us + us;
  const long long target_cpu_clock = (target_us * 179ll) / 100ll;
  const long long target_ppu_clock = target_cpu_clock * 3ll;

  while (system->cpu.clock < target_cpu_clock ||
         system->ppu.clock < target_ppu_clock)
  {
    if (system->cpu.clock * 3ll < system->ppu.clock)
    {
      const int old_pc = system->cpu.regs.pc;
      cpu_step(&system->cpu);
      // Some games wait for interrupts in busy loops.
      // Detect this as an unchanging PC and just skip the clock ahead.
      // (Technically we could be at a JSR or RTI/RTS but
      // that would be really weird, so assume it's a JMP or Bxx
      // in order to keep this check very cheap.)
      if (system->cpu.regs.pc == old_pc)
        system->cpu.clock = (system->ppu.clock + 2ll) / 3ll;
    }
    else
    {
      const int scanline = system->ppu.state.scanline;
      ppu_step(&system->ppu, *system->frame, &system->valid_frame);
      if (system->ppu.regs.mask.background || system->ppu.regs.mask.sprites)
        for (int i = scanline; i < system->ppu.state.scanline; i++)
          mapper_scanline(&system->mapper);
    }

    system->cpu.nmi = ppu_poll_nmi(&system->ppu);
    system->cpu.irq =
      mapper_poll_irq(&system->mapper) ||
      apu_poll_irq(&system->apu);

    if (mapper_need_prg_ram_sync(&system->mapper))
    {
      cart_sync_prg_ram(system->cart);
      mapper_notify_prg_ram_sync(&system->mapper);
    }
  }

  system->clock_us += us;
}

const struct frame *system_current_frame(const struct system *const system)
{
  return &(*system->frame)[system->valid_frame];
}

void system_set_controller_state(struct system *const system,
  const int which, const bool buttons[8])
{
  controller_set_state(&system->controller, which, buttons);
}

void system_destroy(struct system *const system)
{
  free(system->ppu.ram);
  free(system->ram);
  free(system->frame);
}
