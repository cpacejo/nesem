#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "cpu.h"
#include "ppu.h"


//
// Helpers
//

static struct pattern *pattern_table(struct ppu_memory *const memory,
  const int bank, const int index)
{
  return &memory->pattern_table_1k[(bank << 2) | (index >> 6)]
    ->pattern[index & 0x3F];
}

static int flip8(const int x)
{
  const int a = ((x & 0xAA) >> 1) | ((x & 0x55) << 1);
  const int b = ((a & 0xCC) >> 2) | ((a & 0x33) << 2);
  return ((b & 0xF0) >> 4) | ((b & 0x0F) << 4);
}


//
// I/O
//

void ppu_write_control(struct ppu *const ppu, const int val)
{
  ppu->regs.scroll.t = (ppu->regs.scroll.t & 0x73FF) | ((val & 3) << 10);
  ppu->regs.control.data_vert_incr = (val >> 2) & 1;
  ppu->regs.control.sprite_pattern_table = (val >> 3) & 1;
  ppu->regs.control.background_pattern_table = (val >> 4) & 1;
  ppu->regs.control.large_sprites = (val >> 5) & 1;
  ppu->regs.control.vblank_nmi = (val >> 7) & 1;
}

void ppu_write_mask(struct ppu *const ppu, const int val)
{
  ppu->regs.mask.greyscale = val & 1;
  ppu->regs.mask.background_leftmost = (val >> 1) & 1;
  ppu->regs.mask.sprites_leftmost = (val >> 2) & 1;
  ppu->regs.mask.background = (val >> 3) & 1;
  ppu->regs.mask.sprites = (val >> 4) & 1;
  ppu->regs.mask.emph_r = (val >> 5) & 1;
  ppu->regs.mask.emph_g = (val >> 6) & 1;
  ppu->regs.mask.emph_b = (val >> 7) & 1;
}

int ppu_read_status(struct ppu *const ppu)
{
  const int ret =
    ((int) ppu->regs.status.sprite_0_hit << 6) |
    ((int) ppu->regs.status.vblank << 7);

  ppu->regs.status.vblank = false;
  ppu->regs.scroll.w = false;

  return ret;
}

void ppu_write_oam_addr(struct ppu *const ppu, const int val)
{
  ppu->regs.oam_addr = val;
}

void ppu_write_oam_data(struct ppu *const ppu, const int val)
{
  ((uint8_t *) &ppu->memory.oam)[ppu->regs.oam_addr] = val;
  ppu->regs.oam_addr = (ppu->regs.oam_addr + 1) & 0xFF;
}

void ppu_write_scroll(struct ppu *const ppu, const int val)
{
  if (ppu->regs.scroll.w)
    ppu->regs.scroll.t = (ppu->regs.scroll.t & 0xC1F) |
      ((val & 7) << 12) | ((val & 0xF8) << 2);
  else
  {
    ppu->regs.scroll.t = (ppu->regs.scroll.t & 0x7FE0) | (val >> 3);
    ppu->regs.scroll.x = val & 7;
  }

  ppu->regs.scroll.w = !ppu->regs.scroll.w;
}

void ppu_write_addr(struct ppu *const ppu, const int val)
{
  if (ppu->regs.scroll.w)
  {
    ppu->regs.scroll.t = (ppu->regs.scroll.t & 0x7F00) | val;
    ppu->regs.scroll.v = ppu->regs.scroll.t;
  }
  else
    ppu->regs.scroll.t = (ppu->regs.scroll.t & 0xFF) | ((val & 0x3F) << 8);

  ppu->regs.scroll.w = !ppu->regs.scroll.w;
}

static void ppu_incr_v(struct ppu *const ppu)
{
  ppu->regs.scroll.v = (ppu->regs.scroll.v +
    (ppu->regs.control.data_vert_incr ? 32 : 1)) & 0xFFFF;
}

void ppu_write_data(struct ppu *const ppu, const int val)
{
  const int addr = ppu->regs.scroll.v;

  if (((addr >> 13) & 1) == 0)
    ((uint8_t *) ppu->memory.pattern_table_1k[(addr >> 10) & 7])[addr & 0x3FF] = val;
  else if (((addr >> 8) & 0x3F) < 0x3F)
    ((uint8_t *) ppu->memory.name_attribute_table[(addr >> 10) & 3])[addr & 0x3FF] = val;
  else if ((addr & 0x10) == 0 || (addr & 3) == 0)
    ((uint8_t *) &ppu->memory.background_palette)[addr & 0xF] = val;
  else
    ((uint8_t *) &ppu->memory.sprite_palette)[addr & 0xF] = val;

  ppu_incr_v(ppu);
}

int ppu_read_data(struct ppu *const ppu)
{
  const int addr = ppu->regs.scroll.v;
  int ret = ppu->regs.data;

  if (((addr >> 13) & 1) == 0)
    ppu->regs.data = ((uint8_t *) ppu->memory.pattern_table_1k[(addr >> 10) & 7])[addr & 0x3FF];
  else
  {
    ppu->regs.data = ((uint8_t *) ppu->memory.name_attribute_table[(addr >> 10) & 3])[addr & 0x3FF];
    if (((addr >> 8) & 0x3F) >= 0x3F)
    {
      if ((addr & 0x10) == 0 || (addr & 3) == 0)
        ret = ((uint8_t *) &ppu->memory.background_palette)[addr & 0xF];
      else
        ret = ((uint8_t *) &ppu->memory.sprite_palette)[addr & 0xF];
    }
  }

  ppu_incr_v(ppu);
  return ret;
}

void ppu_write_oam_dma(struct ppu *const ppu, struct cpu *const cpu, const int val)
{
  const int addr = ppu->regs.oam_addr;
  const uint8_t *const data = cpu->memory.pages[val];
  memcpy(&((uint8_t *) &ppu->memory.oam)[addr], data, 0x100 - addr);
  memcpy(&ppu->memory.oam, data, addr);
  cpu->clock += 513 + (cpu->clock & 1);
}

bool ppu_poll_nmi(const struct ppu *);


//
// Rendering
//

static int get_prio_mask(const uint8_t prio[33], const int x)
{
  return ((prio[x >> 3] << (x & 7)) & 0xFF) |
    (prio[(x >> 3) + 1] >> (8 - (x & 7)));
}

static void set_prio_mask(uint8_t prio[33], const int x, const int mask)
{
  prio[x >> 3] |= mask >> (x & 7);
  prio[(x >> 3) + 1] |= mask << (8 - (x & 7));
}

static void get_sprite_pattern_bits(struct ppu *const ppu,
  const struct sprite *const sprite, const int y,
  int *const restrict pattern_bits_low_out, int *const restrict pattern_bits_high_out)
{
  const int sprite_y =
    (sprite->attr & 0x80) == 0 ? y - (sprite->y + 1) :
    16 - (y - sprite->y);

  const int name = ppu->regs.control.large_sprites ?
    ((sprite->index >> 1) << 1) | (sprite_y >> 3) : sprite->index;
  const int bank = ppu->regs.control.large_sprites ?
    sprite->index & 1 : ppu->regs.control.sprite_pattern_table;

  int pattern_bits_low =
    pattern_table(&ppu->memory, bank, name)->bits[0][sprite_y & 7];
  int pattern_bits_high =
    pattern_table(&ppu->memory, bank, name)->bits[1][sprite_y & 7];

  if ((sprite->attr & 0x40) != 0)
  {
    pattern_bits_low = flip8(pattern_bits_low);
    pattern_bits_high = flip8(pattern_bits_high);
  }

  *pattern_bits_low_out = pattern_bits_low;
  *pattern_bits_high_out = pattern_bits_high;
}

static void get_background_pattern_bits(struct ppu *const ppu,
  const int map, const int xi, const int yi, const int y_fine_offset,
  int *const restrict pattern_bits_low_out, int *const restrict pattern_bits_high_out)
{
  const int name = ppu->memory.name_attribute_table[map]
    ->name_table.tile[yi][xi];

  *pattern_bits_low_out =
    pattern_table(&ppu->memory, ppu->regs.control.background_pattern_table, name)
      ->bits[0][y_fine_offset];

  *pattern_bits_high_out =
    pattern_table(&ppu->memory, ppu->regs.control.background_pattern_table, name)
      ->bits[1][y_fine_offset];
}

static void render_sprite(struct scanline *const scanline,
  const int pattern_bits_low, const int pattern_bits_high,
  const int mask, const int attr,
  const int x, const struct palette *const palette)
{
  // this unfortunately is rather slow
  for (int j = 0; j < 8; j++)
  {
    const int pattern_bit_high = ((pattern_bits_high & ~mask) << j) & 0x80;
    const int pattern_bit_low = ((pattern_bits_low & ~mask) << j) & 0x80;
    if ((pattern_bit_high | pattern_bit_low) != 0)
      scanline->pixel[12 + x + j] =
        palette->color[attr][(pattern_bit_high >> 6) + (pattern_bit_low >> 7)];
  }
}

static bool sprite_0_hit(struct ppu *const ppu, const int y,
  struct sprite *const sprite)
{
  int sprite_pattern_bits_low, sprite_pattern_bits_high;
  get_sprite_pattern_bits(ppu, sprite, y,
    &sprite_pattern_bits_low, &sprite_pattern_bits_high);
  const int sprite_opaque =
    sprite_pattern_bits_low | sprite_pattern_bits_high;

  const int v = ppu->regs.scroll.v;
  const int x = sprite->x + ((v & 0x1F) << 3) + ppu->regs.scroll.x;
  const int y_fine_offset = (v >> 12) & 7;
  const int yi = (v >> 5) & 0x1F;
  const int base_map = (v >> 10) & 3;
  int bg0_pattern_bits_low, bg0_pattern_bits_high;
  int bg1_pattern_bits_low, bg1_pattern_bits_high;
  get_background_pattern_bits(ppu,
    base_map ^ (x >> 8), (x >> 3) & 0x1F, yi, y_fine_offset,
    &bg0_pattern_bits_low, &bg0_pattern_bits_high);
  get_background_pattern_bits(ppu,
    base_map ^ ((x + 8) >> 8), ((x + 8) >> 3) & 0x1F, yi, y_fine_offset,
    &bg1_pattern_bits_low, &bg1_pattern_bits_high);
  const int bg_opaque =
    ((bg0_pattern_bits_low | bg0_pattern_bits_high) << 8) |
    bg1_pattern_bits_low | bg1_pattern_bits_high;

  return ((bg_opaque >> (x & 7)) & sprite_opaque) != 0;
}

static void render_active_sprite(
  struct ppu *const ppu, struct scanline *const scanline,
  const int y, const int i,
  uint8_t prio[33], const bool background)
{
  const struct sprite *const sprite = &ppu->memory.oam.sprite[i];

  if ((bool) (sprite->attr & 0x20) != background)
    return;

  int pattern_bits_low, pattern_bits_high;
  get_sprite_pattern_bits(ppu, sprite, y,
    &pattern_bits_low, &pattern_bits_high);

  const int mask = get_prio_mask(prio, sprite->x);
  const int opaque = pattern_bits_low | pattern_bits_high;

  set_prio_mask(prio, sprite->x, opaque);
  render_sprite(scanline, pattern_bits_low, pattern_bits_high, mask,
    sprite->attr & 3, sprite->x, &ppu->memory.sprite_palette);
}

static void render_background_sprites(
  struct ppu *const ppu, struct scanline *const scanline)
{
  const int v = ppu->regs.scroll.v;
  const int x_fine_offset = ppu->regs.scroll.x;
  const int x_coarse_offset = v & 0x1F;
  const int y_fine_offset = (v >> 12) & 7;
  const int yi = (v >> 5) & 0x1F;
  const int base_map = (v >> 10) & 3;

  for (int i = (int) !ppu->regs.mask.background_leftmost; i < 33; i++)
  {
    const int map = base_map ^ ((x_coarse_offset + i) >> 5);
    const int xi = (x_coarse_offset + i) & 0x1F;

    const int attr = (ppu->memory.name_attribute_table[map]
      ->attribute_table.square[yi >> 2][xi >> 2]
      >> ((xi & 2) + ((yi & 2) << 1))) & 3;

    int pattern_bits_low, pattern_bits_high;
    get_background_pattern_bits(ppu, map, xi, yi, y_fine_offset,
      &pattern_bits_low, &pattern_bits_high);

    render_sprite(scanline, pattern_bits_low, pattern_bits_high, 0,
      attr, (i << 3) - x_fine_offset, &ppu->memory.background_palette);
  }
}

static void next_line(struct ppu *const ppu)
{
  // next line
  if (ppu->regs.mask.background || ppu->regs.mask.sprites)
  {
    int vy = 1 +
      ((ppu->regs.scroll.v >> 12) & 7) +
      ((ppu->regs.scroll.v >> 2) & 0xF8) +
      ((ppu->regs.scroll.v >> 3) & 0x100);
    if ((vy & 0xF8) >> 3 == 30)
      vy += 0x10;
    ppu->regs.scroll.v =
      (ppu->regs.scroll.t & 0x41F) |  // yes, .t
      ((vy & 7) << 12) | ((vy & 0xF8) << 2) | ((vy & 0x100) << 3);
  }
}

static void do_pre_render_scanline(struct ppu *const ppu)
{
  assert(ppu->state.state == PPU_PRE_RENDER);

  ppu->regs.status.vblank = false;
  ppu->regs.status.sprite_0_hit = false;
  ppu->clock += 340 - (int) ppu->state.odd_frame;
  ppu->state.state = PPU_VISIBLE;
  ppu->state.scanline = 0;
}

static void render_scanline(struct ppu *const ppu, struct scanline *const scanline)
{
  assert(ppu->state.state == PPU_VISIBLE);
  assert(ppu->state.scanline < 240);

  if (ppu->state.scanline == 0)
  {
    if (ppu->regs.mask.background || ppu->regs.mask.sprites)
      ppu->regs.scroll.v = ppu->regs.scroll.t;
  }
  else next_line(ppu);

  const int y = ppu->state.scanline;

  // tint
  scanline->greyscale = ppu->regs.mask.greyscale;
  scanline->emph_r = ppu->regs.mask.emph_r;
  scanline->emph_g = ppu->regs.mask.emph_g;
  scanline->emph_b = ppu->regs.mask.emph_b;

  // backdrop
  const int backdrop_color = ppu->memory.background_palette.color[0][0];
  memset(&scanline->pixel[12], backdrop_color, 268 - 12);

  // order sprites
  int active_sprites[8];
  int active_sprite_count = 0;
  if (ppu->regs.mask.sprites)
  {
    const int sprite_height = ppu->regs.control.large_sprites ? 16 : 8;
    for (int i = 0; i < 64; i++)
    {
      if (ppu->memory.oam.sprite[i].y + 1 <= y &&
          ppu->memory.oam.sprite[i].y + 1 + sprite_height > y)
      {
        active_sprites[active_sprite_count] = i;
        active_sprite_count++;
        if (active_sprite_count >= 8) break;
      }
    }
  }

  // check sprite 0 hit
  if (active_sprite_count > 0 && active_sprites[0] == 0 && ppu->regs.mask.background)
    ppu->regs.status.sprite_0_hit =
      ppu->regs.status.sprite_0_hit ||
      sprite_0_hit(ppu, y, &ppu->memory.oam.sprite[0]);

  // background sprites
  // prio[32] isn't read from, only written to
  uint8_t prio[33] = { (uint8_t) ((int) ppu->regs.mask.sprites_leftmost - 1) };
  for (int i = 0; i < active_sprite_count; i++)
    render_active_sprite(ppu, scanline, y, active_sprites[i], prio, true);

  // background tiles
  if (ppu->regs.mask.background)
    render_background_sprites(ppu, scanline);

  // foreground sprites
  for (int i = 0; i < active_sprite_count; i++)
    render_active_sprite(ppu, scanline, y, active_sprites[i], prio, false);

  // border
  memset(&scanline->pixel[0], backdrop_color, 12 + ((int) !ppu->regs.mask.background_leftmost << 3));
  memset(&scanline->pixel[268], backdrop_color, 280 - 268);

  ppu->clock += 340;
  if (ppu->state.scanline == 239)
    ppu->state.state = PPU_POST_RENDER;
  ppu->state.scanline++;
}

static void do_post_render_scanline(struct ppu *const ppu)
{
  assert(ppu->state.state == PPU_POST_RENDER);

  next_line(ppu);

  ppu->clock += 340;
  ppu->state.state = PPU_VBLANK;
}

static void do_vblank(struct ppu *const ppu)
{
  assert(ppu->state.state == PPU_VBLANK);

  ppu->regs.status.vblank = true;

  // NOTE: this clock indicates when the *next* thing is to occur
  ppu->clock += 340 * 20;
  ppu->state.state = PPU_PRE_RENDER;
}

void ppu_step(struct ppu *const ppu, struct frame frame[2], int *const valid_frame)
{
  const int cur_frame = 1 - *valid_frame;

  switch (ppu->state.state)
  {
  case PPU_PRE_RENDER:
    do_pre_render_scanline(ppu);
    break;

  case PPU_VISIBLE:
    render_scanline(ppu, &frame[cur_frame].scanline[ppu->state.scanline]);
    if (ppu->state.state == PPU_POST_RENDER)
      *valid_frame = cur_frame;
    break;

  case PPU_POST_RENDER:
    do_post_render_scanline(ppu);
    break;

  case PPU_VBLANK:
    do_vblank(ppu);
    break;
  }
}
