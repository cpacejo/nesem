#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include "cart.h"
#include "cpu.h"
#include "misc.h"
#include "ppu.h"
#include "mapper_mmc3.h"


int mmc3_config(struct mmc3 *const mmc3, const struct cart *const cart)
{
  assert(cart->mapper_type == MAPPER_TYPE_MMC3);

  if (!is_power_of_2(cart->prg_rom_size) ||
    cart->prg_rom_size < 8 << 10 || cart->prg_rom_size > 512 << 10)
    return -1;

  if (cart->chr_rom_size != 0 && (!is_power_of_2(cart->chr_rom_size) ||
    cart->chr_rom_size < 1 << 10 || cart->chr_rom_size > 256 << 10))
    return -1;

  if (cart->prg_ram_size != 0 && cart->prg_ram_size != 8192)
    return -1;

  if (cart->chr_ram_size != 0 && cart->chr_ram_size != 8192)
    return -1;

  if (cart->chr_rom_size != 0 && cart->chr_ram_size != 0)
    return -1;

  for (int i = 0; i < 64; i++)
    mmc3->memory.prg_bank[i] =
      (char *) cart->prg_rom + ((i << 13) & (cart->prg_rom_size - 1));

  if (cart->chr_rom_size == 0)
  {
    for (int i = 0; i < 256; i++)
      mmc3->memory.chr_bank[i] =
        (char *) cart->chr_ram + ((i & 7) << 10);
  }
  else
  {
    for (int i = 0; i < 256; i++)
      mmc3->memory.chr_bank[i] =
        (char *) cart->chr_rom + ((i << 10) & (cart->chr_rom_size - 1));
  }

  mmc3->memory.has_prg_ram = cart->prg_ram_size != 0;
  mmc3->memory.prg_ram = cart->prg_ram;

  return 0;
}


//
// Helpers
//

static void update_mirroring(struct ppu *const ppu, const int mode)
{
  ppu->memory.name_attribute_table[0] =
    &ppu->ram->name_attribute_table[0];

  ppu->memory.name_attribute_table[1] =
    &ppu->ram->name_attribute_table[1 - mode];

  ppu->memory.name_attribute_table[2] =
    &ppu->ram->name_attribute_table[mode];

  ppu->memory.name_attribute_table[3] =
    &ppu->ram->name_attribute_table[1];
}

static void update_prg_8k_bank_helper(struct mmc3_memory *const memory,
  struct cpu *const cpu, const int which, const int bank)
{
  for (int i = 0; i < 32; i++)
    cpu->memory.pages[i + ((which + 4) << 5)] =
      (uint8_t *) memory->prg_bank[bank] + (i << 8);
}

static void update_prg_bank(struct mmc3 *const mmc3,
  struct cpu *const cpu, const int which)
{
  update_prg_8k_bank_helper(&mmc3->memory, cpu,
    which == 0 ? ((int) mmc3->regs.prg_bank_mode << 1) : 1,
    mmc3->regs.prg_bank[which]);
}

static void update_prg_bank_62(struct mmc3 *const mmc3, struct cpu *const cpu)
{
  update_prg_8k_bank_helper(&mmc3->memory, cpu,
    (1 - (int) mmc3->regs.prg_bank_mode) << 1, 62);
}

static void update_prg_bank_63(struct mmc3 *const mmc3, struct cpu *const cpu)
{
  update_prg_8k_bank_helper(&mmc3->memory, cpu, 3, 63);
}

static void update_chr_bank(struct mmc3 *const mmc3,
  struct ppu *const ppu, const int which)
{
  if (which < 2)
  {
    ppu->memory.pattern_table_1k[((int) mmc3->regs.chr_bank_mode << 2) + (which << 1)] =
      mmc3->memory.chr_bank[mmc3->regs.chr_bank[which] & ~1];
    ppu->memory.pattern_table_1k[((int) mmc3->regs.chr_bank_mode << 2) + (which << 1) + 1] =
      mmc3->memory.chr_bank[mmc3->regs.chr_bank[which] | 1];
  }
  else
  {
    ppu->memory.pattern_table_1k[((1 - (int) mmc3->regs.chr_bank_mode) << 2) + (which - 2)] =
      mmc3->memory.chr_bank[mmc3->regs.chr_bank[which]];
  }
}


//
// I/O
//

void mmc3_init(struct mmc3 *const mmc3,
  struct cpu *const cpu, struct ppu *const ppu)
{
  mmc3->reg = 0;
  mmc3->regs.prg_bank_mode = MMC3_PRG_BANK_1_FIXED;
  mmc3->regs.chr_bank_mode = MMC3_CHR_BANK_2_KB_1_KB;
  mmc3->regs.irq_latch = 0;
  mmc3->regs.irq_counter = 0;
  mmc3->regs.irq_reload = false;
  mmc3->regs.irq_enable = false;
  mmc3->regs.irq = false;

  update_mirroring(ppu, 0);

  for (int i = 0; i < 6; i++)
  {
    if (i < 2) mmc3->regs.chr_bank[i] = i << 1;
    else mmc3->regs.chr_bank[i] = i + 2;
    update_chr_bank(mmc3, ppu, i);
  }

  for (int i = 0; i < 2; i++)
  {
    mmc3->regs.prg_bank[i] = i;
    update_prg_bank(mmc3, cpu, i);
  }

  update_prg_bank_62(mmc3, cpu);
  update_prg_bank_63(mmc3, cpu);

  if (mmc3->memory.has_prg_ram)
  {
    cpu->memory.io_write[1] &= UINT64_C(0x00000000FFFFFFFF);
    cpu->memory.io_read[1] &= UINT64_C(0x00000000FFFFFFFF);
    for (int i = 0; i < 32; i++)
      cpu->memory.pages[0x60 + i] =
        (uint8_t *) mmc3->memory.prg_ram + (i << 8);
  }
}

static void write_control(struct mmc3 *const mmc3,
  struct cpu *const cpu, struct ppu *const ppu,
  const int value)
{
  const enum mmc3_prg_bank_mode prg_bank_mode =
    (enum mmc3_prg_bank_mode) (value >> 6) & 1;
  const enum mmc3_chr_bank_mode chr_bank_mode =
    (enum mmc3_chr_bank_mode) (value >> 7) & 1;

  if (mmc3->regs.chr_bank_mode != chr_bank_mode)
  {
    mmc3->regs.chr_bank_mode = chr_bank_mode;
    for (int i = 0; i < 6; i++)
      update_chr_bank(mmc3, ppu, i);
  }

  if (mmc3->regs.prg_bank_mode != prg_bank_mode)
  {
    mmc3->regs.prg_bank_mode = prg_bank_mode;
    update_prg_bank(mmc3, cpu, 0);
    update_prg_bank_62(mmc3, cpu);
  }
}

static void write_chr_bank(struct mmc3 *const mmc3,
  struct ppu *const ppu, const int which, const int bank)
{
  if (mmc3->regs.chr_bank[which] != bank)
  {
    mmc3->regs.chr_bank[which] = bank;
    update_chr_bank(mmc3, ppu, which);
  }
}

static void write_prg_bank(struct mmc3 *const mmc3,
  struct cpu *const cpu, const int which, const int bank)
{
  if (mmc3->regs.prg_bank[which] != (bank & 0x3F))
  {
    mmc3->regs.prg_bank[which] = bank & 0x3F;
    update_prg_bank(mmc3, cpu, which);
  }
}

static void write_bank(struct mmc3 *const mmc3,
  struct cpu *const cpu, struct ppu *const ppu,
  const int which, const int bank)
{
  if (which < 6) write_chr_bank(mmc3, ppu, which, bank);
  else write_prg_bank(mmc3, cpu, which - 6, bank);
}

static void write_mirroring(struct mmc3 *const mmc3,
  struct ppu *const ppu, const int value)
{
  update_mirroring(ppu, value & 1);
}

static void write_prg_ram_protect(struct mmc3 *const mmc3, const int value)
{
  if ((value & 0x40) != 0)
    mmc3->need_prg_ram_sync = true;
}

static void write_irq_latch(struct mmc3 *const mmc3, const int value)
{
  mmc3->regs.irq_latch = value;
}

static void write_irq_reload(struct mmc3 *const mmc3)
{
  mmc3->regs.irq_reload = true;
}

static void write_irq_disable(struct mmc3 *const mmc3)
{
  mmc3->regs.irq = false;
  mmc3->regs.irq_enable = false;
}

static void write_irq_enable(struct mmc3 *const mmc3)
{
  mmc3->regs.irq_enable = true;
}


void mmc3_write(struct mmc3 *const mmc3,
  struct cpu *const cpu, struct ppu *const ppu,
  const int addr, const int value)
{
  switch (((addr >> 12) & 6) | (addr & 1))
  {
  case 0:
    mmc3->reg = value & 7;
    write_control(mmc3, cpu, ppu, value);
    break;

  case 1:
    write_bank(mmc3, cpu, ppu, mmc3->reg, value);
    break;

  case 2:
    write_mirroring(mmc3, ppu, value);
    break;

  case 3:
    write_prg_ram_protect(mmc3, value);
    break;

  case 4:
    write_irq_latch(mmc3, value);
    break;

  case 5:
    write_irq_reload(mmc3);
    break;

  case 6:
    write_irq_disable(mmc3);
    break;

  case 7:
    write_irq_enable(mmc3);
    break;
  }
}

void mmc3_scanline(struct mmc3 *const mmc3)
{
  const int irq_counter = mmc3->regs.irq_counter;

  if (irq_counter == 0 || mmc3->regs.irq_reload)
  {
    mmc3->regs.irq_counter = mmc3->regs.irq_latch;
    mmc3->regs.irq_reload = false;
    mmc3->regs.irq = false;
  }
  else
    mmc3->regs.irq_counter--;

  if (mmc3->regs.irq_counter == 0 && irq_counter != 0)
    mmc3->regs.irq = mmc3->regs.irq_enable;
}

bool mmc3_poll_irq(const struct mmc3 *);
