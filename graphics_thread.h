#ifndef GRAPHICS_THREAD
#define GRAPHICS_THREAD

#include <pthread.h>
#include <stdbool.h>
#include <GLFW/glfw3.h>
#include "ppu.h"
#include "graphics.h"
#include "system.h"

struct graphics_thread_control
{
  pthread_mutex_t mutex;
  pthread_cond_t cond;
  bool stop, dirty;
};

struct graphics_thread_context
{
  struct graphics_thread_control *control;
  GLFWwindow *window;
  struct graphics *graphics;
  struct system *system;
  pthread_mutex_t *current_frame_mutex;
  float (*framebuffer)[240][640][3];
};

void graphics_thread_control_init(struct graphics_thread_control *)
  __attribute__ ((cold));
void graphics_thread_control_destroy(struct graphics_thread_control *)
  __attribute__ ((cold));

void graphics_thread_notify_stop(struct graphics_thread_control *)
  __attribute__ ((cold));
void graphics_thread_notify_dirty(struct graphics_thread_control *);

void *graphics_thread_f(void *);

#endif
