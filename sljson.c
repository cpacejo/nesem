#include <assert.h>
#include <ctype.h>
#include <limits.h>
#include <math.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#if __STDC_VERSION__ >= 201112L && !defined(__clang__)
// clang lies about C11 support
#include <uchar.h>
#define u(c) u#c
#define U(c) U#c
#else
typedef uint_fast16_t char16_t;
typedef uint_fast32_t char32_t;
#define u(c) ((char16_t) (c))
#define U(c) ((char32_t) (c))
#endif
#include <sys/types.h>
#include "sljson.h"

#define CHECK(f) do { const int ret = (f); if (ret < 0) return ret; } while (0)


//
// writer
//

#undef sljson_write_unicode_z_string
#undef sljson_write_unicode_string
#undef sljson_write_unicode_z_name
#undef sljson_write_unicode_name

static int maybe_comma(sljson_writer_t *const writer, FILE *const file)
{
  if (writer->start_of_compound)
  {
    writer->start_of_compound = false;
    return 0;
  }
  else if (writer->depth > 0)
    return putc(',', file);
  else
    return putc(' ', file);
}

void sljson_init_writer(sljson_writer_t *const writer)
{
  writer->ascii_only = true;
  writer->bmp_only = true;
  writer->printable_only = true;

  writer->start_of_compound = true;
  writer->depth = 0;
}

void sljson_set_writer_output_style(sljson_writer_t *const writer,
  const enum sljson_encoding encoding, const bool printable_only)
{
  switch (encoding)
  {
  case SLJSON_ASCII:
    writer->ascii_only = true;
    writer->bmp_only = true;
    break;

  case SLJSON_BMP_UTF8:
    writer->ascii_only = false;
    writer->bmp_only = true;
    break;

  case SLJSON_UNICODE_UTF8:
    writer->ascii_only = false;
    writer->bmp_only = false;
    break;
  }

  writer->printable_only = printable_only;
}

void sljson_destroy_writer(sljson_writer_t *const writer)
{
}

int sljson_write_null(sljson_writer_t *const writer, FILE *const file)
{
  CHECK(maybe_comma(writer, file));
  return fputs("null", file);
}

int sljson_write_bool(sljson_writer_t *const writer, FILE *const file, const bool value)
{
  CHECK(maybe_comma(writer, file));
  return fputs(value ? "true" : "false", file);
}

static int write_ascii(sljson_writer_t *const writer, FILE *const file, const char value)
{
  switch (value)
  {
  case 0x22: return fputs("\\\"", file);
  case 0x5C: return fputs("\\\\", file);
  case 0x08: return fputs("\\b", file);
  case 0x0C: return fputs("\\f", file);
  case 0x0A: return fputs("\\n", file);
  case 0x0D: return fputs("\\r", file);
  case 0x09: return fputs("\\t", file);

  case 0x00 ... 0x07:
  case 0x0B:
  case 0x0E ... 0x1F:
    return fprintf(file, "\\u%04X", value);

  case 0x20 ... 0x21:
  case 0x23 ... 0x5B:
  case 0x5D ... 0x7E:
    return putc(value, file);

  case 0x7F:
    if (writer->printable_only)
      return fprintf(file, "\\u%04X", value);
    return putc(value, file);

  default:
    return -1;
  }
}

static int write_bmp(sljson_writer_t *const writer, FILE *const file, const char16_t value)
{
  if (value < UINT16_C(0x80))
    return write_ascii(writer, file, value);
  else if (value < UINT16_C(0x800) && !writer->ascii_only)
  {
    if (writer->printable_only && (value == UINT16_C(0x6DD) || value == UINT16_C(0x70F)))
      return fprintf(file, "\\u%04X", value);

    CHECK(putc(0xC0 | (value >> 6), file));
    return putc(0x80 | (value & 0x3F), file);
  }
  else
  {
    if (value >= UINT16_C(0xD800) && value < UINT16_C(0xE000))
      return -1;

    if (writer->ascii_only ||
      (writer->printable_only &&
        ((value >= UINT16_C(0x180B) && value <= UINT16_C(0x180D)) ||
         (value >= UINT16_C(0x200C) && value <= UINT16_C(0x200F)) ||
         (value >= UINT16_C(0x202A) && value <= UINT16_C(0x202E)) ||
         (value >= UINT16_C(0x2060) && value <= UINT16_C(0x206F)) ||
         (value >= UINT16_C(0xFE00) && value <= UINT16_C(0xFE0F)) ||
         (value >= UINT16_C(0xFEFF) && value <= UINT16_C(0xFFFB)))))
      return fprintf(file, "\\u%04X", value);

    CHECK(putc(0xE0 | (value >> 12), file));
    CHECK(putc(0x80 | ((value >> 6) & 0x3F), file));
    return putc(0x80 | (value & 0x3F), file);
  }
}

static int write_unicode(sljson_writer_t *const writer, FILE *const file, const char32_t value)
{
  if (value < UINT32_C(0x10000))
    return write_bmp(writer, file, value);
  else
  {
    if (value >= UINT32_C(0x110000)) return -1;

    if (writer->ascii_only || writer->bmp_only ||
      (writer->printable_only &&
        ((value >= UINT32_C(0x1D173) && value <= UINT32_C(0x1D17A)) ||
         (value >= UINT32_C(0xE0000) && value <= UINT32_C(0xE0FFF)))))
    {
      return fprintf(file, "\\u%04X\\u%04X",
        0xD800u + (unsigned int) ((value - UINT32_C(0x10000)) >> 10),
        0xDC00u + (unsigned int) (value & UINT32_C(0x3FF)));
    }

    CHECK(putc(0xF0 | (value >> 18), file));
    CHECK(putc(0x80 | ((value >> 12) & 0x3F), file));
    CHECK(putc(0x80 | ((value >> 6) & 0x3F), file));
    return putc(0x80 | (value & 0x3F), file);
  }
}

static char32_t decode_utf8(const char *const value, ptrdiff_t *const i, const size_t length)
{
  char32_t c;

  if ((unsigned char) value[*i] < 0x80)
    c = (char32_t) (unsigned char) value[*i];
  else if (((unsigned char) value[*i] & 0xE0) == 0xC0 && *i + 1 < length &&
      ((unsigned char) value[*i + 1] & 0xC0) == 0x80)
  {
    c = ((char32_t) ((unsigned char) value[*i] & 0x1F) << 6) |
      (char32_t) ((unsigned char) value[*i + 1] & 0x3F);
    *i += 1;
  }
  else if (((unsigned char) value[*i] & 0xF0) == 0xE0 && *i + 2 < length &&
      ((unsigned char) value[*i + 1] & 0xC0) == 0x80 &&
      ((unsigned char) value[*i + 2] & 0xC0) == 0x80)
  {
    c = ((char32_t) ((unsigned char) value[*i] & 0x0F) << 12) |
      ((char32_t) ((unsigned char) value[*i + 1] & 0x3F) << 6) |
      (char32_t) ((unsigned char) value[*i + 2] & 0x3F);
    *i += 2;
  }
  else if (((unsigned char) value[*i] & 0xF8) == 0xF0 && *i + 3 < length &&
      ((unsigned char) value[*i + 1] & 0xC0) == 0x80 &&
      ((unsigned char) value[*i + 2] & 0xC0) == 0x80 &&
      ((unsigned char) value[*i + 3] & 0xC0) == 0x80)
  {
    c = ((char32_t) ((unsigned char) value[*i] & 0x07) << 18) |
      ((char32_t) ((unsigned char) value[*i + 1] & 0x3F) << 12) |
      ((char32_t) ((unsigned char) value[*i + 2] & 0x3F) << 6) |
      (char32_t) ((unsigned char) value[*i + 3] & 0x3F);
    if (c >= UINT32_C(0x110000))
      c = UINT32_C(0xFFFD);
    *i += 3;
  }
  else
    c = UINT32_C(0xFFFD);

  return c;
}

int sljson_write_z_string(sljson_writer_t *const writer, FILE *const file, const char *const value)
{
  CHECK(maybe_comma(writer, file));
  CHECK(putc('"', file));

  for (ptrdiff_t i = 0; value[i] != '\0'; i++)
    CHECK(write_unicode(writer, file, decode_utf8(value, &i, i + 4)));

  return putc('"', file);
}

int sljson_write_string(sljson_writer_t *const writer, FILE *const file,
  const char *const value, const size_t length)
{
  CHECK(maybe_comma(writer, file));
  CHECK(putc('"', file));

  for (ptrdiff_t i = 0; i < length; i++)
    CHECK(write_unicode(writer, file, decode_utf8(value, &i, length)));

  return putc('"', file);
}

int sljson_write_latin1_z_string(sljson_writer_t *const writer, FILE *const file,
  const unsigned char *const value)
{
  CHECK(maybe_comma(writer, file));
  CHECK(putc('"', file));

  for (ptrdiff_t i = 0; value[i] != '\0'; i++)
    CHECK(write_bmp(writer, file, value[i]));

  return putc('"', file);
}

int sljson_write_latin1_string(sljson_writer_t *const writer, FILE *const file,
  const unsigned char *const value, const size_t length)
{
  CHECK(maybe_comma(writer, file));
  CHECK(putc('"', file));

  for (ptrdiff_t i = 0; i < length; i++)
    CHECK(write_bmp(writer, file, value[i]));

  return putc('"', file);
}

int sljson_write_bmp_z_string(sljson_writer_t *const writer, FILE *const file, const char16_t *const value)
{
  CHECK(maybe_comma(writer, file));
  CHECK(putc('"', file));

  for (ptrdiff_t i = 0; value[i] != '\0'; i++)
    CHECK(write_bmp(writer, file, value[i]));

  return putc('"', file);
}

int sljson_write_bmp_string(sljson_writer_t *const writer, FILE *const file,
  const char16_t *const value, const size_t length)
{
  CHECK(maybe_comma(writer, file));
  CHECK(putc('"', file));

  for (ptrdiff_t i = 0; i < length; i++)
    CHECK(write_bmp(writer, file, value[i]));

  return putc('"', file);
}

int sljson_write_unicode_z_string(sljson_writer_t *const writer, FILE *const file,
  const char32_t *const value)
{
  CHECK(maybe_comma(writer, file));
  CHECK(putc('"', file));

  for (ptrdiff_t i = 0; value[i] != '\0'; i++)
    CHECK(write_unicode(writer, file, value[i]));

  return putc('"', file);
}

int sljson_write_unicode_string(sljson_writer_t *const writer, FILE *const file,
  const char32_t *const value, const size_t length)
{
  CHECK(maybe_comma(writer, file));
  CHECK(putc('"', file));

  for (ptrdiff_t i = 0; i < length; i++)
    CHECK(write_unicode(writer, file, value[i]));

  return putc('"', file);
}

int sljson_write_integer(sljson_writer_t *const writer, FILE *const file, const intmax_t value)
{
  CHECK(maybe_comma(writer, file));
  return fprintf(file, "%ji", value);
}

int sljson_write_u_integer(sljson_writer_t *const writer, FILE *const file, const uintmax_t value)
{
  CHECK(maybe_comma(writer, file));
  return fprintf(file, "%ju", value);
}

int sljson_write_float(sljson_writer_t *const writer, FILE *const file, const float value)
{
  if (!isfinite(value))
    return -1;

  CHECK(maybe_comma(writer, file));

  const float abs_value = fabsf(value);

  char buf[2+8+2+2+1];
  sprintf(buf, "%.8E", abs_value);
  buf[1] = '.';  // ugh locales aren't thread-local
  const char *const exp_str = &buf[2 + 8];


  // binary search to find shortest representation
  // O(n*log(n)) ain't too bad
  // by lucky coincidence we have exactly 16 digits to play with
  // this algorithm also leaves at least one digit (which is good)

  int sig_digits = 4;
  for (int d = 4; d > 0; d /= 2)
  {
    char buf2[2+8+2+2+1];
    memcpy(&buf2[0], &buf[0], 2 + sig_digits);
    strcpy(&buf2[2 + sig_digits], exp_str);

    // hop by 2, then 1, then 1 if bad, 0 if good
    if (strtof(buf2, NULL) == abs_value) sig_digits -= d / 2;
    else sig_digits += (d + 1) / 2;
  }


  // it's possible we can get a shorter representation
  // by truncating the next representable float

  const float next_abs_value = nextafterf(abs_value, INFINITY);
  if (next_abs_value != HUGE_VALF)
  {
    char next_buf[2+8+2+2+1];
    sprintf(next_buf, "%.8E", next_abs_value);
    next_buf[1] = '.';
    int d;
    // yes we skip the integer but that's OK because
    // the 1st decimal will always be different if that's different
    for (d = 1; d < sig_digits; d++)
      if (next_buf[2 + d - 1] != buf[2 + d - 1])
        break;

    if (d < sig_digits)
    {
      char buf2[2+8+2+2+1];
      memcpy(&buf2[0], &next_buf[0], 2 + d);
      strcpy(&buf2[2 + d], &next_buf[2 + 8]);

      if (strtof(buf2, NULL) == abs_value)
      {
        sig_digits = d;
        strcpy(buf, next_buf);
      }
    }
  }


  // actually print

  if (signbit(value) != 0)
    CHECK(putc('-', file));

  CHECK(fprintf(file, "%.*s", 2 + sig_digits, buf));

  const long exp = strtol(&exp_str[1], NULL, 10);
  if (exp != 0) return fprintf(file, "E%li", exp);
  else return 0;
}

int sljson_write_double(sljson_writer_t *const writer, FILE *const file, const double value)
{
  if (!isfinite(value))
    return -1;

  CHECK(maybe_comma(writer, file));

  const double abs_value = fabs(value);

  char buf[2+16+2+3+1];
  sprintf(buf, "%.16E", abs_value);
  buf[1] = '.';  // ugh locales aren't thread-local
  const char *const exp_str = &buf[2 + 16];


  // binary search to find shortest representation
  // O(n*log(n)) ain't too bad
  // by lucky coincidence we have exactly 16 digits to play with
  // this algorithm also leaves at least one digit (which is good)
  int sig_digits = 8;
  for (int d = 8; d > 0; d /= 2)
  {
    char buf2[2+16+2+3+1];
    memcpy(&buf2[0], &buf[0], 2 + sig_digits);
    strcpy(&buf2[2 + sig_digits], exp_str);

    // hop by 4, then 2, then 1, then 1 if bad, 0 if good
    if (strtod(buf2, NULL) == abs_value) sig_digits -= d / 2;
    else sig_digits += (d + 1) / 2;
  }


  // it's possible we can get a shorter representation
  // by truncating the next representable float

  const double next_abs_value = nextafter(abs_value, INFINITY);
  if (next_abs_value != HUGE_VAL)
  {
    char next_buf[2+16+2+3+1];
    sprintf(next_buf, "%.16E", next_abs_value);
    next_buf[1] = '.';
    int d;
    // yes we skip the integer but that's OK because
    // the 1st decimal will always be different if that's different
    for (d = 1; d < sig_digits; d++)
      if (next_buf[2 + d - 1] != buf[2 + d - 1])
        break;

    if (d < sig_digits)
    {
      char buf2[2+16+2+3+1];
      memcpy(&buf2[0], &next_buf[0], 2 + d);
      strcpy(&buf2[2 + d], &next_buf[2 + 16]);

      if (strtof(buf2, NULL) == abs_value)
      {
        sig_digits = d;
        strcpy(buf, next_buf);
      }
    }
  }


  // actually print

  if (signbit(value) != 0)
    CHECK(putc('-', file));

  CHECK(fprintf(file, "%.*s", 2 + sig_digits, buf));

  const long exp = strtol(&exp_str[1], NULL, 10);
  if (exp != 0) return fprintf(file, "E%li", exp);
  else return 0;
}

int sljson_write_decimal(sljson_writer_t *const writer, FILE *const file,
  const intmax_t sig, const int exp)
{
  CHECK(maybe_comma(writer, file));
  CHECK(fprintf(file, "%ji", sig));
  if (exp != 0) return fprintf(file, "E%i", exp);
  else return 0;
}

int sljson_write_decimal2(sljson_writer_t *const writer, FILE *const file,
  const int sign, const uintmax_t sig, const uintmax_t frac,
  const int frac_digits, const int exp)
{
  if (frac_digits < 0)
    return -1;

  CHECK(maybe_comma(writer, file));

  if (sign < 0)
    CHECK(putc('-', file));

  CHECK(fprintf(file, "%ju", sig));

  if (frac_digits > 0)
    CHECK(fprintf(file, ".%0*ju", frac_digits, frac));

  if (exp != 0) return fprintf(file, "E%i", exp);
  else return 0;
}

static int write_digit(FILE *const file, const int digit)
{
  if (digit >= 0 && digit < 10) return putc('0' + digit, file);
  else return -1;
}

int sljson_write_bcd(sljson_writer_t *const writer, FILE *const file,
  const unsigned char *const sig, const size_t length, const int exp)
{
  CHECK(maybe_comma(writer, file));

  const int sign_nibble =
    length > 0 ? sig[length - 1] & 0x0F : 0;

  if (sign_nibble == 0x0D || sign_nibble == 0x0B)
    CHECK(putc('-', file));

  const size_t digits = length * 2 - (sign_nibble < 0x0A ? 0 : 1);

  bool found_nonzero = false;

  for (ptrdiff_t i = 0; i < digits; i++)
  {
    const int digit0 = sig[i / 2] >> 4;
    if (digit0 > 0) found_nonzero = true;
    if (found_nonzero)
      CHECK(write_digit(file, digit0));

    i++;
    if (!(i < digits)) break;

    const int digit1 = sig[i / 2] & 0x0F;
    if (digit1 > 0) found_nonzero = true;
    if (found_nonzero)
      CHECK(write_digit(file, digit1));
  }

  if (!found_nonzero)
    CHECK(putc('0', file));

  if (exp != 0) return fprintf(file, "E%i", exp);
  else return 0;
}

int sljson_write_begin_array(sljson_writer_t *const writer, FILE *const file)
{
  CHECK(maybe_comma(writer, file));

  writer->start_of_compound = true;
  if (writer->depth == UINT_MAX) return -1;
  writer->depth++;

  return putc('[', file);
}

int sljson_write_end_array(sljson_writer_t *const writer, FILE *const file)
{
  writer->start_of_compound = false;
  writer->depth--;

  return putc(']', file);
}

int sljson_write_begin_object(sljson_writer_t *const writer, FILE *const file)
{
  CHECK(maybe_comma(writer, file));

  writer->start_of_compound = true;
  if (writer->depth == UINT_MAX) return -1;
  writer->depth++;

  return putc('{', file);
}

int sljson_write_z_name(sljson_writer_t *const writer, FILE *const file, const char *const name)
{
  CHECK(sljson_write_z_string(writer, file, name));

  writer->start_of_compound = true;
  return putc(':', file);
}

int sljson_write_name(sljson_writer_t *const writer, FILE *const file,
  const char *const name, const size_t length)
{
  CHECK(sljson_write_string(writer, file, name, length));

  writer->start_of_compound = true;
  return putc(':', file);
}

int sljson_write_latin1_z_name(sljson_writer_t *const writer, FILE *const file,
  const unsigned char *const name)
{
  CHECK(sljson_write_latin1_z_string(writer, file, name));

  writer->start_of_compound = true;
  return putc(':', file);
}

int sljson_write_latin1_name(sljson_writer_t *const writer, FILE *const file,
  const unsigned char *const name, const size_t length)
{
  CHECK(sljson_write_latin1_string(writer, file, name, length));

  writer->start_of_compound = true;
  return putc(':', file);
}

int sljson_write_bmp_z_name(sljson_writer_t *const writer, FILE *const file, const char16_t *const name)
{
  CHECK(sljson_write_bmp_z_string(writer, file, name));

  writer->start_of_compound = true;
  return putc(':', file);
}

int sljson_write_bmp_name(sljson_writer_t *const writer, FILE *const file,
  const char16_t *const name, const size_t length)
{
  CHECK(sljson_write_bmp_string(writer, file, name, length));

  writer->start_of_compound = true;
  return putc(':', file);
}

int sljson_write_unicode_z_name(sljson_writer_t *const writer,
  FILE *const file, const char32_t *const name)
{
  CHECK(sljson_write_unicode_z_string(writer, file, name));

  writer->start_of_compound = true;
  return putc(':', file);
}

int sljson_write_unicode_name(sljson_writer_t *const writer, FILE *const file,
  const char32_t *const name, const size_t length)
{
  CHECK(sljson_write_unicode_string(writer, file, name, length));

  writer->start_of_compound = true;
  return putc(':', file);
}

int sljson_write_end_object(sljson_writer_t *const writer, FILE *const file)
{
  writer->start_of_compound = false;
  writer->depth--;

  return putc('}', file);
}


//
// reader
//

#undef sljson_read_unicode_string
#undef sljson_read_unicode_name

void sljson_init_reader(sljson_reader_t *const reader)
{
}

void sljson_destroy_reader(sljson_reader_t *const reader)
{
}

void sljson_copy_reader(sljson_reader_t *const restrict dest_reader,
  const sljson_reader_t *const restrict src_reader)
{
  *dest_reader = *src_reader;
}

static bool is_ws(const char c)
{
  return c == ' ' || c == '\n' || c == '\t' || c == '\r';
}

static bool is_ws_comma(const char c)
{
  return c == ',' || is_ws(c);
}

static int skip_ws_comma(sljson_reader_t *const reader, FILE *const file)
{
  return fscanf(file, "%*[\t\n\r ,]");
}

static int skip_ws(sljson_reader_t *const reader, FILE *const file)
{
  return fscanf(file, "%*[\t\n\r ]");
}

int sljson_peek_type(sljson_reader_t *const reader, FILE *const file,
  enum sljson_type *const type_out)
{
  CHECK(skip_ws_comma(reader, file));

  const int c = getc(file);
  if (c < 0) return c;

  CHECK(ungetc(c, file));

  switch (c)
  {
  case 'n':
    *type_out = SLJSON_NULL;
    break;
  case 't':
  case 'f':
    *type_out = SLJSON_BOOLEAN;
    break;
  case '-':
  case '0' ... '9':
    *type_out = SLJSON_NUMBER;
    break;
  case '"':
    *type_out = SLJSON_STRING;
    break;
  case '[':
    *type_out = SLJSON_ARRAY;
    break;
  case '{':
    *type_out = SLJSON_OBJECT;
    break;
  default:
    return -1;
  }

  return 0;
}

static int skip_rest_of_string(sljson_reader_t *const reader, FILE *const file)
{
  for (;;)
  {
    const int c = getc(file);
    if (c < 0) return c;
    if (c == '"') break;
    if (c == '\\')
      CHECK(getc(file));
  }

  return 0;
}

int sljson_skip_value(sljson_reader_t *const reader, FILE *const file)
{
  CHECK(skip_ws_comma(reader, file));

  const int c = getc(file);
  if (c < 0) return c;

  switch (c)
  {
  case 'n':
    return fscanf(file, "ull");
  case 't':
    return fscanf(file, "rue");
  case 'f':
    return fscanf(file, "alse");

  case '-':
  case '0' ... '9':
    return fscanf(file, "%*[0-9.Ee+-]");

  case '"':
    return skip_rest_of_string(reader, file);

  case '[':
  case '{':
    for (int depth = 1; depth > 0; )
    {
      const int d = getc(file);

      switch (d)
      {
      case '[':
      case '{':
        if (depth == INT_MAX) return -1;
        depth++;
        break;

      case ']':
      case '}':
        depth--;
        break;

      case '"':
        CHECK(skip_rest_of_string(reader, file));
        break;

      default:
        break;
      }
    }
    return 0;

  default:
    return -1;
  }
}

int sljson_peek_null(sljson_reader_t *const reader, FILE *const file,
  bool *const is_null_out)
{
  CHECK(skip_ws_comma(reader, file));

  const int c = getc(file);
  if (c < 0) return c;

  CHECK(ungetc(c, file));

  *is_null_out = c == 'n';

  return 0;
}

int sljson_consume_null(sljson_reader_t *const reader, FILE *const file)
{
  CHECK(skip_ws_comma(reader, file));

  return fscanf(file, "null");
}

int sljson_read_bool(sljson_reader_t *const reader, FILE *const file,
  bool *const value_out)
{
  CHECK(skip_ws_comma(reader, file));

  const int c = getc(file);
  if (c < 0) return c;

  switch (c)
  {
  case 't':
    CHECK(fscanf(file, "rue"));
    *value_out = true;
    return 0;

  case 'f':
    CHECK(fscanf(file, "alse"));
    *value_out = true;
    return 0;

  default:
    return -1;
  }
}

static int read_digits(sljson_reader_t *const reader, FILE *const file,
  int *const sign_out, signed char *const restrict digits, const int max_digits,
  int *const num_digits_out, int *const exp_out)
{
  CHECK(skip_ws_comma(reader, file));

  int digit = 0, sigs, exp = 0;
  int sign = 1;

  int c = getc(file);
  if (c < 0) return c;

  if (c == '-')
  {
    sign = -1;
    c = getc(file);
    if (c < 0) return c;
  }

  if (c == '0')
  {
    c = getc(file);
    if (c < 0 && !feof(file)) return c;
  }
  else
  {
    if (c <= '0' || c >= '9') return -1;
    do
    {
      if (digit >= max_digits) return -1;
      digits[digit] = c - '0';
      digit++;
      c = getc(file);
      if (c < 0 && !feof(file)) return c;
    } while (c >= '0' && c <= '9');
  }

  if (c == '.')
  {
    sigs = digit;
    c = getc(file);
    if (c < 0) return c;
    if (c <= '0' || c >= '9') return -1;

    do
    {
      if (digit >= max_digits) return -1;
      digits[digit] = c - '0';
      digit++;
      c = getc(file);
      if (c < 0 && !feof(file)) return c;
    } while (c >= '0' && c <= '9');
  }

  if (c == 'e' || c == 'E')
  {
    int exp = 0;
    int exp_sign = 1;

    c = getc(file);
    if (c < 0) return c;

    if (c == '-') exp_sign = -1;
    if (c == '-' || c == '+')
    {
      c = getc(file);
      if (c < 0) return c;
    }
    if (c <= '0' || c >= '9') return -1;

    do
    {
      const int d = c - '0';
      if ((exp_sign < 0 && exp < (INT_MIN + d) / 10) ||
        (exp_sign > 0 && exp > (INT_MAX - d) / 10))
        return -1;
      exp = (exp * 10) + exp_sign * d;
      c = getc(file);
      if (c < 0 && !feof(file)) return c;
    } while (c >= '0' && c <= '9');
  }

  if (c >= 0 && !is_ws_comma(c))
    return -1;

  if (c >= 0)
    CHECK(ungetc(c, file));
  else
  {
    assert(feof(file));
    clearerr(file);
  }

  *sign_out = sign;
  *num_digits_out = digit;
  *exp_out = exp - digit + sigs;
  return 0;
}

int sljson_read_bcd(sljson_reader_t *const reader, FILE *const file,
  unsigned char *const sig_out, const size_t length, int *const exp_out)
{
  if (length < 1 || length > INT_MAX / 2)
    return -1;

  const int max_digits = length * 2 - 1;
  signed char digits[max_digits];
  int sign, num_digits;

  CHECK(read_digits(reader, file, &sign, digits, max_digits, &num_digits, exp_out));

  const int offset = max_digits - num_digits;

  if ((offset & 1) != 0)
    sig_out[offset / 2] = digits[0];

  for (int i = offset / 2; i < length - 1; i++)
    sig_out[i] = (digits[2 * i - offset] << 4) | digits[2 * i - offset + 1];

  sig_out[length - 1] =
    (digits[num_digits - 1] << 4) | (sign < 0 ? 0xD : 0xC);

  return 0;
}

static int read_unicode(sljson_reader_t *const reader, FILE *const file, char32_t *const out)
{
  const int c0 = getc(file);
  if (c0 < 0) return -1;

  if (c0 < 0x80)
  {
    *out = (char32_t) c0;
    return 0;
  }

  const int c1 = getc(file);
  if (c1 < 0) return -1;

  if ((c0 & 0xE0) == 0xC0 && (c1 & 0xC0) == 0x80)
  {
    const char32_t c = ((char32_t) (c0 & 0x1F) << 6) | (char32_t) (c1 & 0x3F);
    if (c >= UINT32_C(0x80)) *out = c;
    else *out = UINT32_C(0xFFFD);
    return 0;
  }

  const int c2 = getc(file);
  if (c2 < 0) return -1;

  if ((c0 & 0xF0) == 0xE0 && (c1 & 0xC0) == 0x80 && (c2 & 0xC0) == 0x80)
  {
    const char32_t c = ((char32_t) (c0 & 0x0F) << 12) |
      ((char32_t) (c1 & 0x3F) << 6) | (char32_t) (c2 & 0x3F);
    if (c >= UINT32_C(0x800) && (c < UINT32_C(0xD800) || c >= UINT32_C(0xE000))) *out = c;
    else *out = UINT32_C(0xFFFD);
    return 0;
  }

  const int c3 = getc(file);
  if (c3 < 0) return -1;

  if ((c0 & 0xF8) == 0xF0 && (c1 & 0xC0) == 0x80 &&
      (c2 & 0xC0) == 0x80 && (c3 & 0xC0) == 0x80)
  {
    const char32_t c = ((char32_t) (c0 & 0x07) << 18) | ((char32_t) (c1 & 0x3F) << 12) |
      ((char32_t) (c2 & 0x3F) << 6) | (char32_t) (c3 & 0x3F);
    if (c >= UINT32_C(0x10000) && c < UINT32_C(0x110000)) *out = c;
    else *out = UINT32_C(0xFFFD);
    return 0;
  }

  *out = UINT32_C(0xFFFD);
  return 0;
}

static int read_hex_quad(sljson_reader_t *const reader, FILE *const file, char16_t *const out)
{
  const int c0 = getc(file);
  if (!isxdigit(c0)) return -1;

  const int c1 = getc(file);
  if (!isxdigit(c1)) return -1;

  const int c2 = getc(file);
  if (!isxdigit(c2)) return -1;

  const int c3 = getc(file);
  if (!isxdigit(c3)) return -1;

  *out = (char16_t) strtol((const char []) { c0, c1, c2, c3, '\0' }, NULL, 0x10);
  return 0;
}

static int read_string_unicode(sljson_reader_t *const reader, FILE *const file,
  char32_t *const out, bool *const end_of_string)
{
  char32_t c;
  CHECK(read_unicode(reader, file, &c));

  if (c == U('"'))
  {
    *end_of_string = true;
    return 0;
  }

  *end_of_string = false;

  if (c == U('\\'))
  {
    const int d = getc(file);
    if (d < 0) return -1;

    switch (d)
    {
    case '"':
    case '\\':
    case '/':
      *out = (char32_t) c;
      return 0;

    case 'b':
      *out = U('\b');
      return 0;

    case 'f':
      *out = U('\f');
      return 0;

    case 'n':
      *out = U('\n');
      return 0;

    case 'r':
      *out = U('\r');
      return 0;

    case 't':
      *out = U('\t');
      return 0;

    case 'u': {
      char16_t u0;
      CHECK(read_hex_quad(reader, file, &u0));

      if (u0 < UINT16_C(0xD800) || u0 >= UINT16_C(0xE000))
      {
        *out = (char32_t) u0;
        return 0;
      }

      if (u0 >= UINT16_C(0xDC00))
        return -1;

      CHECK(fscanf(file, "\\u"));

      char16_t u1;
      CHECK(read_hex_quad(reader, file, &u1));

      if (u1 < UINT16_C(0xDC00) || u1 >= UINT16_C(0xE000))
        return -1;

      *out = UINT32_C(0x10000) +
        ((char32_t) (u0 & UINT16_C(0x3FF)) << 10) +
        ((char32_t) (u1 & UINT16_C(0x3FF)));
      return 0;
    }

    default:
      return -1;
    }
  }

  if (c < UINT32_C(0x20)) return -1;

  *out = c;
  return 0;
}

ssize_t sljson_read_string(sljson_reader_t *const reader, FILE *const file,
  char *const restrict value, size_t length, const bool allow_nuls)
{
  if (length > INT_MAX - 3) length = INT_MAX - 3;

  CHECK(skip_ws_comma(reader, file));
  CHECK(fscanf(file, "\""));

  for (int i = 0; i < length;)
  {
    char32_t c;
    bool end_of_string;
    CHECK(read_string_unicode(reader, file, &c, &end_of_string));
    if (end_of_string)
    {
      value[i] = '\0';
      return i;
    }

    if (c < UINT32_C(0x80))
    {
      value[i] = (char) c;
      i++;
    }
    else if (c < UINT32_C(0x800))
    {
      if (i + 1 >= length) break;
      value[i] = (char) (0xC0 | (c >> 6));
      value[i + 1] = (char) (c & 0x3F);
      i += 2;
    }
    else if (c < UINT32_C(0x10000))
    {
      if (i + 2 >= length) break;
      value[i] = (char) (0xE0 | (c >> 12));
      value[i + 1] = (char) (0x80 | ((c >> 6) & 0x3F));
      value[i + 2] = (char) (c & 0x3F);
      i += 3;
    }
    else
    {
      assert(c < UINT32_C(0x110000));
      if (i + 3 >= length) break;
      value[i] = (char) (0xF0 | (c >> 18));
      value[i + 1] = (char) (0x80 | ((c >> 12) & 0x3F));
      value[i + 2] = (char) (0x80 | ((c >> 6) & 0x3F));
      value[i + 3] = (char) (c & 0x3F);
      i += 4;
    }
  }

  return -1;
}

ssize_t sljson_read_ascii_string(sljson_reader_t *const reader, FILE *const file,
  char *const restrict value, size_t length, const bool allow_nuls)
{
  if (length > INT_MAX) length = INT_MAX;

  CHECK(skip_ws_comma(reader, file));
  CHECK(fscanf(file, "\""));

  for (int i = 0; i < length; i++)
  {
    char32_t c;
    bool end_of_string;
    CHECK(read_string_unicode(reader, file, &c, &end_of_string));
    if (end_of_string)
    {
      value[i] = '\0';
      return i;
    }

    if (c < UINT32_C(0x80) && (c != UINT32_C(0) || allow_nuls))
      value[i] = (char) c;
    else value[i] = '\x1A';
  }

  return -1;
}

ssize_t sljson_read_latin1_string(sljson_reader_t *const reader, FILE *const file,
  unsigned char *const restrict value, size_t length, const bool allow_nuls)
{
  if (length > INT_MAX) length = INT_MAX;

  CHECK(skip_ws_comma(reader, file));
  CHECK(fscanf(file, "\""));

  for (int i = 0; i < length; i++)
  {
    char32_t c;
    bool end_of_string;
    CHECK(read_string_unicode(reader, file, &c, &end_of_string));
    if (end_of_string)
    {
      value[i] = 0;
      return i;
    }

    if (c < UINT32_C(0x100) && (c != UINT32_C(0) || allow_nuls))
      value[i] = (unsigned char) c;
    else value[i] = 0x1A;
  }

  return -1;
}

ssize_t sljson_read_bmp_string(sljson_reader_t *const reader, FILE *const file,
  char16_t *const restrict value, size_t length, const bool allow_nuls)
{
  if (length > INT_MAX) length = INT_MAX;

  CHECK(skip_ws_comma(reader, file));
  CHECK(fscanf(file, "\""));

  for (int i = 0; i < length; i++)
  {
    char32_t c;
    bool end_of_string;
    CHECK(read_string_unicode(reader, file, &c, &end_of_string));
    if (end_of_string)
    {
      value[i] = UINT16_C(0);
      return i;
    }

    if (c < UINT32_C(0x10000) && (c != UINT32_C(0) || allow_nuls))
      value[i] = (char16_t) c;
    else value[i] = UINT16_C(0xFFFD);
  }

  return -1;
}

ssize_t sljson_read_unicode_string(sljson_reader_t *const reader, FILE *const file,
  char32_t *const restrict value, size_t length, const bool allow_nuls)
{
  if (length > INT_MAX) length = INT_MAX;

  CHECK(skip_ws_comma(reader, file));
  CHECK(fscanf(file, "\""));

  for (int i = 0; i < length; i++)
  {
    char32_t c;
    bool end_of_string;
    CHECK(read_string_unicode(reader, file, &c, &end_of_string));
    if (end_of_string)
    {
      value[i] = UINT32_C(0);
      return i;
    }

    if (allow_nuls && c != UINT32_C(0)) value[i] = c;
    else value[i] = UINT32_C(0xFFFD);
  }

  return -1;
}

int sljson_consume_begin_array(sljson_reader_t *const reader, FILE *const file)
{
  CHECK(skip_ws_comma(reader, file));
  return fscanf(file, "[");
}

int sljson_peek_end_array(sljson_reader_t *const reader, FILE *const file,
  bool *const is_end_out)
{
  CHECK(skip_ws(reader, file));

  const int c = getc(file);
  if (c < 0) return c;

  CHECK(ungetc(c, file));

  *is_end_out = c == ']';
  return 0;
}

int sljson_consume_end_array(sljson_reader_t *const reader, FILE *const file)
{
  CHECK(skip_ws(reader, file));
  return fscanf(file, "[");
}

int sljson_consume_begin_object(sljson_reader_t *const reader, FILE *const file)
{
  CHECK(skip_ws_comma(reader, file));
  return fscanf(file, "{");
}

int sljson_skip_name_value(sljson_reader_t *const reader, FILE *const file)
{
  CHECK(skip_ws_comma(reader, file));
  CHECK(fscanf(file, "\""));
  CHECK(skip_rest_of_string(reader, file));
  CHECK(skip_ws(reader, file));
  CHECK(fscanf(file, ":"));
  return sljson_skip_value(reader, file);
}

ssize_t sljson_read_name(sljson_reader_t *const reader, FILE *const file,
  char *const restrict name, const size_t length, const bool allow_nuls)
{
  CHECK(sljson_read_string(reader, file, name, length, allow_nuls));
  CHECK(skip_ws(reader, file));
  return fscanf(file, ":");
}

ssize_t sljson_read_ascii_name(sljson_reader_t *const reader, FILE *const file,
  char *const restrict name, const size_t length, const bool allow_nuls)
{
  CHECK(sljson_read_ascii_string(reader, file, name, length, allow_nuls));
  CHECK(skip_ws(reader, file));
  return fscanf(file, ":");
}

ssize_t sljson_read_latin1_name(sljson_reader_t *const reader, FILE *const file,
  unsigned char *const restrict name, const size_t length, const bool allow_nuls)
{
  CHECK(sljson_read_latin1_string(reader, file, name, length, allow_nuls));
  CHECK(skip_ws(reader, file));
  return fscanf(file, ":");
}

ssize_t sljson_read_bmp_name(sljson_reader_t *const reader, FILE *const file,
  char16_t *const restrict name, const size_t length, const bool allow_nuls)
{
  CHECK(sljson_read_bmp_string(reader, file, name, length, allow_nuls));
  CHECK(skip_ws(reader, file));
  return fscanf(file, ":");
}

ssize_t sljson_read_unicode_name(sljson_reader_t *reader, FILE *file,
  char32_t *restrict name, size_t length, bool allow_nuls)
{
  CHECK(sljson_read_unicode_string(reader, file, name, length, allow_nuls));
  CHECK(skip_ws(reader, file));
  return fscanf(file, ":");
}

int sljson_peek_end_object(sljson_reader_t *const reader, FILE *const file,
  bool *const is_end_out)
{
  CHECK(skip_ws(reader, file));

  const int c = getc(file);
  if (c < 0) return c;

  CHECK(ungetc(c, file));

  *is_end_out = c == '}';
  return 0;
}

int sljson_consume_end_object(sljson_reader_t *const reader, FILE *const file)
{
  CHECK(skip_ws(reader, file));
  return fscanf(file, "}");
}


// higher-level obejct interface

int sljson_read_object(sljson_reader_t *const reader, FILE *const file, sljson_object_t *const object_out)
{
  CHECK(sljson_consume_begin_object(reader, file));

  int num_names = 0;

  for (;;)
  {
    bool is_end;
    CHECK(sljson_peek_end_object(reader, file, &is_end));
    if (is_end) break;

    const long pos = ftell(file);
    if (pos < 0) return -1;

    sljson_copy_reader(&object_out->names[num_names].reader, reader);
    object_out->names[num_names].pos = pos;

    CHECK(sljson_skip_name_value(reader, file));

    num_names++;
  }

  object_out->num_names = num_names;

  return sljson_consume_end_object(reader, file);
}
