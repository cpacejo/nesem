#ifndef APU_H
#define APU_H

#include <stdbool.h>
#include <stddef.h>
#include "cpu.h"
#include "filter_gen.h"

struct apu;

void apu_write_pulse_1_envelope(struct apu *, int val);
void apu_write_pulse_1_sweep(struct apu *, int val);
void apu_write_pulse_1_timer_low(struct apu *, int val);
void apu_write_pulse_1_timer_high(struct apu *, int val);
void apu_write_pulse_2_envelope(struct apu *, int val);
void apu_write_pulse_2_sweep(struct apu *, int val);
void apu_write_pulse_2_timer_low(struct apu *, int val);
void apu_write_pulse_2_timer_high(struct apu *, int val);
void apu_write_triangle_linear(struct apu *, int val);
void apu_write_triangle_timer_low(struct apu *, int val);
void apu_write_triangle_timer_high(struct apu *, int val);
void apu_write_noise_envelope(struct apu *, int val);
void apu_write_noise_timer(struct apu *, int val);
void apu_write_noise_length(struct apu *, int val);
void apu_write_dmc_rate(struct apu *, int val);
void apu_write_dmc_direct(struct apu *, int val);
void apu_write_dmc_address(struct apu *, int val);
void apu_write_dmc_length(struct apu *, int val);
void apu_write_control(struct apu *, int val);
int apu_read_status(struct apu *);
void apu_write_frame_counter(struct apu *, int val);
inline bool apu_poll_irq(const struct apu *);

#define SAMPLE_BUFFER_SIZE 466  // approx. CPU clock / 16
#define SAMPLE_RATE (SAMPLE_BUFFER_SIZE*240.0)
#define DELAY_SAMPLES (-STEP_FILTER_MIN_X)
#define OVERLAP_SAMPLES (STEP_FILTER_WIDTH - 1 - DELAY_SAMPLES)
// The extra samples are used as working space, to avoid a memcpy.
#define RAW_SAMPLE_BUFFER_SIZE (SAMPLE_BUFFER_SIZE + DELAY_SAMPLES + OVERLAP_SAMPLES)

__attribute__ ((cold))
void apu_init(struct apu *);

// Samples out may be less than SAMPLE_BUFFER_SIZE if the
// game resyncs by writing the high bit of the frame counter reg.
// (Not yet implemented!)
void apu_frame(struct apu *, const struct cpu_memory *,
  float buffer[restrict RAW_SAMPLE_BUFFER_SIZE],
  size_t *samples_out);


// private

enum apu_step
{
  APU_STEP_1, APU_STEP_2, APU_STEP_3, APU_STEP_4, APU_STEP_STUTTER
};

enum apu_channel
{
  CHANNEL_PULSE_1 = 0,
  CHANNEL_PULSE_2 = 1,
  CHANNEL_TRIANGLE = 2,
  CHANNEL_NOISE = 3,
  CHANNEL_DMC = 4
};

enum pulse_duty
{
  PULSE_DUTY_1_8 = 0,
  PULSE_DUTY_1_4 = 1,
  PULSE_DUTY_1_2 = 2,
  PULSE_DUTY_3_4 = 3
};

struct apu_regs
{
  struct apu_pulse
  {
    enum pulse_duty duty;
    // phase_low counts down from timer to 0 (inclusive)
    // phase_high counts up from 0 to 7 (inclusive)
    int timer, phase_high, phase_low;
    struct apu_sweep
    {
      bool enabled, negate, reload;
      int period, shift, divider;
    } sweep;
  } pulse[2];

  struct apu_triangle
  {
    int linear_counter, linear_counter_reload_value;
    // phase_low counts down from timer to 0 (inclusive)
    // phase_high counts up from 0 to 31 (inclusive)
    int timer, phase_high, phase_low;
    bool linear_counter_reload;
  } triangle;

  struct apu_noise
  {
    bool fast;
    // phase_low counts down from timer to 0 (inclusive)
    int timer, lfsr, phase_low;
  } noise;

  struct apu_dmc
  {
    bool irq_enable, loop, irq;
    // phase_low counts down from timer to 0 (inclusive)
    int timer, address, length, phase_low;
    int level, remaining;
    bool silent;
  } dmc;

  struct apu_envelope
  {
    int length_counter, divider, decay_level;
    bool halt, start;
    bool constant_volume;
    int volume;
  } envelope[4];  // [enum apu_channel], but not CHANNEL_DMC

  struct
  {
    bool enable[5];
  } control;

  struct
  {
    bool stutter_mode, irq_inhibit, irq;
  } frame_counter;
};

struct apu_state
{
  enum apu_step step;
  float delay_overlap_buffer[DELAY_SAMPLES + OVERLAP_SAMPLES];
  float highpass_90_carry, highpass_440_carry, lowpass_14000_carry;
};

struct apu
{
  struct apu_state state;
  struct apu_regs regs;
};

inline bool apu_poll_irq(const struct apu *const apu)
{
  return apu->regs.frame_counter.irq || apu->regs.dmc.irq;
}

#endif
