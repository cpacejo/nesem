#include <assert.h>
#include <stdint.h>
#include "cpu.h"
#include "misc.h"
#include "ppu.h"
#include "mapper_cnrom.h"


int cnrom_config(struct cnrom *const cnrom, const struct cart *const cart)
{
  assert(cart->mapper_type == MAPPER_TYPE_CNROM);

  if (cart->prg_rom_size != 16 << 10 && cart->prg_rom_size != 32 << 10)
    return -1;

  if (!is_power_of_2(cart->chr_rom_size) ||
    cart->chr_rom_size < 8 << 10 || cart->chr_rom_size > 32 << 10)
    return -1;

  if (cart->prg_ram_size != 0)
    return -1;

  if (cart->chr_ram_size != 0)
    return -1;

  cnrom->config.mirroring = cart->mirroring;

  for (int i = 0; i < 2; i++)
    cnrom->memory.prg_bank[i] =
      (char *) cart->prg_rom + ((i << 14) & (cart->prg_rom_size - 1));

  for (int i = 0; i < 4; i++)
    cnrom->memory.chr_bank[i] =
      (char *) cart->chr_rom + ((i << 13) & (cart->chr_rom_size - 1));

  return 0;
}


static void update_chr_8k_bank_helper(struct cnrom_memory *const memory,
  struct ppu *const ppu, const int bank)
{
  for (int i = 0; i < 8; i++)
    ppu->memory.pattern_table_1k[i] =
      (struct pattern_table_1k *) memory->chr_bank[bank] + i;
}


void cnrom_init(struct cnrom *const cnrom,
  struct cpu *const cpu, struct ppu *const ppu)
{
  ppu->memory.name_attribute_table[0] =
    &ppu->ram->name_attribute_table[0];

  ppu->memory.name_attribute_table[1] =
    &ppu->ram->name_attribute_table[(int) (cnrom->config.mirroring == MIRRORING_VERTICAL)];

  ppu->memory.name_attribute_table[2] =
    &ppu->ram->name_attribute_table[(int) (cnrom->config.mirroring == MIRRORING_HORIZONTAL)];

  ppu->memory.name_attribute_table[3] =
    &ppu->ram->name_attribute_table[1];

  for (int i = 0; i < 128; i++)
    cpu->memory.pages[128 + i] =
      (uint8_t *) cnrom->memory.prg_bank[i >> 6] + ((i & 0x3F) << 8);

  update_chr_8k_bank_helper(&cnrom->memory, ppu, 0);
}

static void write_bank_select(struct cnrom *const cnrom,
  struct ppu *const ppu, const int bank)
{
  update_chr_8k_bank_helper(&cnrom->memory, ppu, bank & 3);
}

void cnrom_write(struct cnrom *const cnrom,
  struct cpu *const cpu, struct ppu *const ppu,
  const int addr, const int value)
{
  write_bank_select(cnrom, ppu, value);
}
