#include <stdbool.h>
#include <string.h>
#include "input.h"
#include "controller.h"

void controller_write_strobe(struct controller *const c, const int value)
{
  if (c->strobe)
    memcpy(c->latched, c->pressed, sizeof c->latched);

  c->shift[0] = 0;
  c->shift[1] = 0;
  c->strobe = (bool) (value & 1);
}

static bool controller_read(struct controller *const c, const int which)
{
  if (c->strobe) return c->pressed[which][0];
  if (c->shift[which] >= 8) return true;
  const int ret = c->latched[which][c->shift[which]];
  c->shift[which]++;
  return ret;
}

int controller_read_0(struct controller *const c)
{
  return 0x40 | (int) controller_read(c, 0);
}

int controller_read_1(struct controller *const c)
{
  return 0x40 | (int) controller_read(c, 1);
}

void controller_set_state(struct controller *const c, const int which, const bool pressed[8])
{
  memcpy(c->pressed[which], pressed, sizeof c->pressed[0]);
}
